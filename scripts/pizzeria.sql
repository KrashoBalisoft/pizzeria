CREATE DATABASE  IF NOT EXISTS `pizzeria` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pizzeria`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: localhost    Database: pizzeria
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cat_mensaje_error`
--

DROP TABLE IF EXISTS `cat_mensaje_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_mensaje_error` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla',
  `descripcion` varchar(100) DEFAULT NULL COMMENT 'Descripción',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_mensaje_error`
--

LOCK TABLES `cat_mensaje_error` WRITE;
/*!40000 ALTER TABLE `cat_mensaje_error` DISABLE KEYS */;
INSERT INTO `cat_mensaje_error` VALUES (1,'Usuario y Contraseña Incorrectos.'),(2,'Parámetros Insuficientes.'),(3,'Error al procesar la solicitud.'),(4,'Error en los datos enviados.'),(5,'No se encontró el registro con la clave proporcionada');
/*!40000 ALTER TABLE `cat_mensaje_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_perfil`
--

DROP TABLE IF EXISTS `cat_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_perfil` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'Id de la tabla',
  `descripcion` varchar(45) DEFAULT NULL COMMENT 'Descripción',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Catálogo de perfiles de los usuarios del sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_perfil`
--

LOCK TABLES `cat_perfil` WRITE;
/*!40000 ALTER TABLE `cat_perfil` DISABLE KEYS */;
INSERT INTO `cat_perfil` VALUES (0000000001,'Administrador');
/*!40000 ALTER TABLE `cat_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_tipo_telefono`
--

DROP TABLE IF EXISTS `cat_tipo_telefono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_tipo_telefono` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la tabla',
  `descripcion` varchar(100) DEFAULT NULL COMMENT 'Descripción',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Esta tabla es un catálogo de tipos telefónicos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_tipo_telefono`
--

LOCK TABLES `cat_tipo_telefono` WRITE;
/*!40000 ALTER TABLE `cat_tipo_telefono` DISABLE KEYS */;
/*!40000 ALTER TABLE `cat_tipo_telefono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_usuarios`
--

DROP TABLE IF EXISTS `cat_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID de la tabla',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre del usuario',
  `username` varchar(45) DEFAULT NULL COMMENT 'Login del usuario',
  `pass` varchar(255) DEFAULT NULL COMMENT 'Password del usuario',
  `estatus` char(1) DEFAULT 'A' COMMENT 'Bandera que indica la situación del usuario (A=Activo, I=Inactivo)',
  `id_perfil` int(10) unsigned DEFAULT NULL COMMENT 'Id del perfil al que pertenece',
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_id_perfil_idx` (`id_perfil`),
  CONSTRAINT `fk_cat_usuarios_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `cat_perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla que guarda los usuarios del sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_usuarios`
--

LOCK TABLES `cat_usuarios` WRITE;
/*!40000 ALTER TABLE `cat_usuarios` DISABLE KEYS */;
INSERT INTO `cat_usuarios` VALUES (2,'Jose Luis','pepe','926e27eecdbc7a18858b3798ba99bddd','A',1);
/*!40000 ALTER TABLE `cat_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del cliente',
  `nombre` varchar(45) DEFAULT NULL COMMENT 'Nombre del cliente',
  `rfc` varchar(13) DEFAULT NULL COMMENT 'RFC del cliente',
  `domicilio` varchar(100) DEFAULT NULL COMMENT 'Domicilio del cliente',
  `cp` varchar(20) DEFAULT NULL COMMENT 'Código postal del cliente',
  `correo` varchar(45) DEFAULT NULL COMMENT 'Correo del cliente',
  `colonia` varchar(100) DEFAULT NULL COMMENT 'Colonia del cliente',
  `telefono` varchar(20) DEFAULT NULL COMMENT 'Número telefónico del cliente',
  `id_tipo_telefono` tinyint(3) unsigned DEFAULT NULL COMMENT 'Id que identifica el tipo de teléfono',
  `id_sucursal` int(10) unsigned DEFAULT NULL COMMENT 'Id de la sucursal más cercana',
  PRIMARY KEY (`id`),
  UNIQUE KEY `telefono_UNIQUE` (`telefono`),
  KEY `fk_clientes_id_tipo_telefono_idx` (`id_tipo_telefono`),
  KEY `fk_clientes_id_sucursal_idx` (`id_sucursal`),
  CONSTRAINT `fk_clientes_id_sucursal` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_id_tipo_telefono` FOREIGN KEY (`id_tipo_telefono`) REFERENCES `cat_tipo_telefono` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Esta tabla guarda a los clientes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'JOSE LUIS','GASL811214GP1',NULL,NULL,NULL,NULL,'9831340351',NULL,NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID de la tabla',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre de la sucursal',
  `direccion` varchar(100) DEFAULT NULL COMMENT 'Dirección de la sucursal',
  `telefono` varchar(20) DEFAULT NULL COMMENT 'Teléfono de la sucursal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_sucursales`
--

DROP TABLE IF EXISTS `usuarios_sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_sucursales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID de la tabla',
  `id_usuario` int(10) unsigned DEFAULT NULL COMMENT 'Id del usuario',
  `id_sucursal` int(10) unsigned DEFAULT NULL COMMENT 'Id de la sucursal que puede ver el usuario',
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_sucursales_id_usuario_idx` (`id_usuario`),
  KEY `fk_usuarios_sucursales_id_sucursal_idx` (`id_sucursal`),
  CONSTRAINT `fk_usuarios_sucursales_id_sucursal` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_sucursales_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `cat_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Esta tabla guarda las sucursales que puede ver un usuario del sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_sucursales`
--

LOCK TABLES `usuarios_sucursales` WRITE;
/*!40000 ALTER TABLE `usuarios_sucursales` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_sucursales` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-04  8:19:38
