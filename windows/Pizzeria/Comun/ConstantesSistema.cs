﻿using System;
using System.ComponentModel;

namespace Comun
{
    public class ConstantesSistemas
    {
        public const int IdBebidas = 1, IdEspecialidades = 2, IdEntremeses = 3, IdPastas = 4, IdPersonalizadas = 5;
        public const int IdEstadoInicialPedido = 1, IdEstadoEnviadoCocina = 2, IdEstadoTerminado = 5, IdEnReparto = 6;
        public static int IdUsuario = 0;

        public static readonly int[] IdSucursal = new int[] {3};
        public static int IdOrigen = 2, IdOrigenCallCenter = 1, IdCliente = 0;
        public const string CiudadSucursal = "Playa del Carmen";

        public const int IdEstadoCajaAbierta = 1, IdRetiroEfectivo = 2, IdCierreCaja = 3;
        public const int IdGrande = 1, IdMediano = 2, IdPequeno = 3;

        public const int NumeroCopias = 2;  
        public const string DirectorioImagenesCallCenter = "/Imagenes/";

        public static TimeSpan Tiempo = new TimeSpan(0, 0, 15);
	}
}