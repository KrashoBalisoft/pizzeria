﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosPedidos
    {
        private readonly IRepositorioPedidos _repositorioPedidos;

        public ServiciosPedidos(string usuario, string password)
        {
            _repositorioPedidos = new RepositorioPedidosWS(usuario, password);
        }

	    public async Task<bool> AgregarPedido(Pedido pedido)
	    {
		    return await _repositorioPedidos.AgregarPedido(pedido);
	    }

        public async Task<Pedido> AgregarPedidoSucursal(Pedido pedido)
        {
            return await _repositorioPedidos.AgregarPedidoSucursal(pedido);
        }

        public async Task<bool> CobrarPedido(int id)
        {
            return await _repositorioPedidos.CobrarPedido(id);
        }

        public async Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos)
        {
            return await _repositorioPedidos.BuscarPedidosCocina(listaSucursales, estadoPedidos);
        }

        public async Task<Pedido> BuscarPedido(int id)
        {
            return await _repositorioPedidos.BuscarPedido(id);
        }

		public async Task<IList<Pedido>> ObtenerTodos()
		{
			return await _repositorioPedidos.ObtenerTodos();
		}

        public async Task<IList<Pedido>> ObtenerPedidosPorSucursal(int[] listaSucursales)
        {
            return await _repositorioPedidos.ObtenerPedidosPorSucursal(listaSucursales);
        }

        public async Task<IList<Pedido>> ObtenerPedidosPorSucursalyFecha(int[] listaSucursales, string fechaInicial, string fechaFinal)
        {
            return await _repositorioPedidos.ObtenerPedidosPorSucursalyFecha(listaSucursales, fechaInicial, fechaFinal);
        }

        public async Task<bool> EnviarPedidoAPreparacion(int id)
        {
            return await _repositorioPedidos.EnviarPedidoAPreparacion(id);
        }

        public async Task<bool> TerminarPedido(int id)
        {
            return await _repositorioPedidos.TerminarPedido(id);
        }

        public async Task<bool> TransferirPedido(int IdPedido, int IdSucursal)
        {
            return await _repositorioPedidos.TransferirPedido(IdPedido, IdSucursal);
        }

        public async Task<bool> CancelarPedido(int id)
        {
            return await _repositorioPedidos.CancelarPedido(id);
        }

        public async Task<bool> EnviarPedidoALaCocina(int id)
        {
            return await _repositorioPedidos.EnviarPedidoALaCocina(id);
        }

        public async Task<bool> AsignarPedido(int IdPedido, int IdEmpleado)
        {
            return await _repositorioPedidos.AsignarPedido(IdPedido, IdEmpleado);
        }

        public async Task<bool> ConfirmarPedido(int IdPedido, int Entregado, string Observaciones)
        {
            return await _repositorioPedidos.ConfirmarPedido(IdPedido, Entregado, Observaciones);
        }

        public async Task<bool> MandarPedidoUrgente(int id)
        {
            return await _repositorioPedidos.MandarPedidoUrgente(id);
        }
    }
}