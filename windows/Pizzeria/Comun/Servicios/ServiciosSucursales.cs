﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
	public class ServiciosSucursales
	{
		private readonly IRepositorioSucursales _repositorioSucursales;

		public ServiciosSucursales(string usuario, string password)
		{
            _repositorioSucursales = new RepositorioSucursalesWS(usuario, password);
		}

		public async Task<Sucursal> ObtenerDatosSucursal(int id)
		{
            return await _repositorioSucursales.ObtenerDatosSucursal(id);
		} 

        public async Task<List<Sucursal>> ObtenerListaSucursales()
        {
            return await _repositorioSucursales.ObtenerListaSucursales();
        }

        public async Task<string> Insertar(string nombre, string direccion, string telefono, string horario)
        {
            return await _repositorioSucursales.Insertar(nombre, direccion, telefono, horario);
        }

        public async Task<string> Actualizar(int id, string nombre, string direccion, string telefono, string horario)
        {
            return await _repositorioSucursales.Actualizar(id, nombre, direccion, telefono, horario);
        }

        public async Task<string> Eliminar(int id)
        {
            return await _repositorioSucursales.Eliminar(id);
        } 

	}
}