﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
	public class ServiciosClientes
	{
		private readonly IRepositorioClientes _repositorioClientes;

		public ServiciosClientes(string usuario, string password)
		{
			_repositorioClientes = new RepositorioClientesWS(usuario, password);
		}

		public async Task<Cliente> BuscarClientePorTelefono(string telefono)
		{
			return await _repositorioClientes.BuscarClientePorTelefono(telefono);
		}

		public async Task<Cliente> AgregarCliente(Cliente cliente)
		{
			return await _repositorioClientes.AgregarCliente(cliente);
		}

		public async Task<bool> ModificarCliente(Cliente cliente)
		{
			return await _repositorioClientes.ModificarCliente(cliente);
		}

        public async Task<List<Cliente>> ObtenerListaClientes()
        {
            return await _repositorioClientes.ObtenerListaClientes();
        }

        public async Task<string> Insertar(Cliente cliente)
        {
            return await _repositorioClientes.Insertar(cliente);
        }


       public async Task<string> Eliminar(int id)
        {
            return await _repositorioClientes.Eliminar(id);
        } 
 

	}
}