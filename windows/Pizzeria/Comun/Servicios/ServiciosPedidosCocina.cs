﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosPedidosCocina
    {
        private readonly IRepositorioPedidosCocina _repositorioPedidos;

        public ServiciosPedidosCocina(string usuario, string password)
        {
            _repositorioPedidos = new RepositorioPedidosCocinaWS(usuario, password);
        }

        public async Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos)
        {
            return await _repositorioPedidos.BuscarPedidosCocina(listaSucursales, estadoPedidos);
        }

        public async Task<bool> EnviarPedidoAPreparacion(int Id)
        {
            return await _repositorioPedidos.EnviarPedidoAPreparacion(Id);
        }

        public async Task<bool> TerminarPedido(int Id)
        {
            return await _repositorioPedidos.TerminarPedido(Id);
        }
    }
}