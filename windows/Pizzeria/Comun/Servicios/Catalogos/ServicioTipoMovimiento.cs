﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Comun.Repositorios;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosTipoMovimiento
    {
        private readonly IRepositorioTipoMovimiento _repositorio;

        public ServiciosTipoMovimiento(string usuario, string password)
        {
            _repositorio = new RepositorioTipoMovimientoWS(usuario, password);
        }

        public async Task<List<TipoMovimiento>> ObtenerListaTipoMovimiento()
        {
            return await _repositorio.ObtenerListaTipoMovimiento();
        }
         
    }
}
