﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosEstadosPedido
    {
        private readonly IRepositorioEstadosPedido _repositorioEstadosPedido;

        public ServiciosEstadosPedido(string usuario, string password)
		{
            _repositorioEstadosPedido = new RepositorioEstadosPedidoWS(usuario, password);
		}
        public async Task<List<EstadoPedido>> ObtenerListaEstadosPedido()
        {
            return await _repositorioEstadosPedido.ObtenerListaEstadoPedido();
        }
        
    }
}
