﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Comun.Repositorios;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosTipoEmpleados
    {
        private readonly IRepositorioTipoEmpleados _repositorio;

        public ServiciosTipoEmpleados(string usuario, string password)
        {
            _repositorio = new IRepositorioTipoEmpleadosWS(usuario, password);
        }

        public async Task<List<TipoEmpleado>> ObtenerLista()
        {
            return await _repositorio.ObtenerLista();
        }

        public async Task<string> Insertar(string descripcion)
        {
            return await _repositorio.Insertar(descripcion);
        }

        public async Task<string> Actualizar(int id, string descripcion)
        {
            return await _repositorio.Actualizar(id, descripcion);
        }
        public async Task<string> Eliminar(int id)
        {
            return await _repositorio.Eliminar(id);
        } 


    }
}
