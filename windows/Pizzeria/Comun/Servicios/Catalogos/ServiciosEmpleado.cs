﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Comun.Repositorios;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosEmpleado
    {
        private readonly IRepositorioEmpleado _repositorio;

        public ServiciosEmpleado(string usuario, string password)
        {
            _repositorio = new IRepositorioEmpleadoWS(usuario, password);
        }

        public async Task<List<Empleado>> ObtenerLista()
        {
            return await _repositorio.ObtenerLista();
        }

        public async Task<string> Insertar(string nombre, int IdTipoEmpleado)
        {
            return await _repositorio.Insertar(nombre, IdTipoEmpleado);
        }

        public async Task<string> Actualizar(int id, string nombre, int IdTipoEmpleado)
        {
            return await _repositorio.Actualizar(id, nombre, IdTipoEmpleado);
        }
        public async Task<string> Eliminar(int id)
        {
            return await _repositorio.Eliminar(id);
        } 


    }
}
