﻿﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades.Catalogos;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosUsuario
    {
        private readonly IRepositorioUsuarios _repositorioUsuarios;

        public ServiciosUsuario(string usuario, string password)
		{
            _repositorioUsuarios = new IRepositorioUsuarioWS(usuario, password);
		}
        public async Task<List<Usuario>> ObtenerLista()
        {
            return await _repositorioUsuarios.ObtenerLista();
        }

        public async Task<Usuario> ValidarUsuario()
        {
            return await _repositorioUsuarios.ValidarUsuario();
        }

        public async Task<string> Insertar(string username, string pass, int id_perfil, string estatus, int id_empleado)
        {
            return await _repositorioUsuarios.Insertar(username,pass,id_perfil,estatus,id_empleado);
        }


        public async Task<string> Actualizar(int id, string username, string pass, int id_perfil, string estatus, int id_empleado)
        {
            return await _repositorioUsuarios.Actualizar(id, username, pass, id_perfil, estatus, id_empleado);
        }
        public async Task<string> Eliminar(int id)
        {
            return await _repositorioUsuarios.Eliminar(id);
        }
         
    }
}
