﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Comun.Repositorios;
using Comun.Repositorios.Catalogos;

namespace Comun.Servicios.Catalogos
{
    public class ServiciosTipoEntrega
    {
        private readonly IRepositorioTipoEntrega _repositorioTipoEntrega;

        public ServiciosTipoEntrega(string usuario, string password)
		{
            _repositorioTipoEntrega = new IRepositorioTipoEntregaWS(usuario, password);
		}
        public async Task<List<TipoEntrega>> ObtenerListaTipoEntrega()
        {
            return await _repositorioTipoEntrega.ObtenerListaTipoEntrega();
        }
        public async Task<bool> Insertar(string descripcion)
        {
            return await _repositorioTipoEntrega.Insertar(descripcion);
        }

        public async Task<bool> Actualizar(int id, string descripcion)
        {
            return await _repositorioTipoEntrega.Actualizar(id, descripcion);
        }
        public async Task<bool> Eliminar(int id)
        {
            return await _repositorioTipoEntrega.Eliminar(id);
        } 
    }
}
