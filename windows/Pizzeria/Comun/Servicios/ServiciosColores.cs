﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosColores
    {
        private readonly IRepositorioColores _repositorioColores;

        public ServiciosColores(string usuario, string password)
		{
            _repositorioColores = new RepositorioColoresWS(usuario, password);
		}

        public async Task<List<ColorSemaforo>> ObtenerListaColores()
        {
            return await _repositorioColores.ObtenerListaColores();
        }

        public async Task<string> Insertar(string nombre, string codigo, string tolerancia)
        {
            return await _repositorioColores.Insertar(nombre, codigo, tolerancia);
        }

        public async Task<string> Actualizar(int id, string nombre, string codigo, string tolerancia)
        {
            return await _repositorioColores.Actualizar(id, nombre, codigo, tolerancia);
        }

        public async Task<string> Eliminar(int id)
        {
            return await _repositorioColores.Eliminar(id);
        } 
        
    }
}
