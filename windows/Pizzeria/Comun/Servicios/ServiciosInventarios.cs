﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
	public class ServiciosInventarios
	{
		private readonly IRepositorioInventarios _repositorioInventarios;

		public ServiciosInventarios(string usuario, string password)
		{
			_repositorioInventarios = new RepositorioInventariosWS(usuario, password);
		}

		public async Task<List<Inventario>> ObtenerListado()
		{
            return await _repositorioInventarios.ObtenerListado();
		}
        public async Task<Inventario> Insertar(Inventario inventario)
        {
            return await _repositorioInventarios.Insertar(inventario);
        }

        public async Task<bool> Eliminar(int id)
        {
            return await _repositorioInventarios.Eliminar(id);
        } 
	}
}