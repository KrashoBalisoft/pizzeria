﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosEstadosPedido
    {
        private readonly IRepositorioEstadosPedido _repositorioEstadosPedido;

        public ServiciosEstadosPedido (string usuario, string password)
        {
            _repositorioEstadosPedido = new RepositorioEstadosPedidoWS(usuario, password);
        }

        public async Task<List<EstadoPedido>> ObtenerListaEstadoPedido()
	    {
		    return await _repositorioEstadosPedido.ObtenerListaEstadoPedido();
	    }
    }
}