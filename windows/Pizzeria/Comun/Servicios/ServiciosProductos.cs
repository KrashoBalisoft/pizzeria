﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
	public class ServiciosProductos
	{
		private readonly IRepositorioProductos _repositorioProductos;

		public ServiciosProductos(string usuario, string password)
		{
			_repositorioProductos = new RepositorioProductosWS(usuario, password);
		}

		public async Task<List<Producto>> ObtenerTodos()
		{
			return await _repositorioProductos.ObtenerProductos();
		}

        public async Task<List<Producto>> ListadoProductos()
		{
			return await _repositorioProductos.ListadoProductos();
		}

        public async Task<List<Producto>> ObtenerIngredientes()
        {
            return await _repositorioProductos.ObtenerIngredientes();
        }
        public async Task<string> Eliminar(int Id)
        {
            return await _repositorioProductos.Eliminar(Id);
        }

        public async Task<bool> Agregar(Producto producto)
        {
            return await _repositorioProductos.Agregar(producto);
        }

        public async Task<bool> Actualizar(Producto producto)
        {
            return await _repositorioProductos.Actualizar(producto);
        }
	}
}