﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosCajas
    {
        private readonly IRepositorioCajas _repositorioCajas;

        public ServiciosCajas(string usuario, string password)
		{
            _repositorioCajas = new RepositorioCajasWS(usuario, password);
		}

        public async Task<List<Caja>> ObtenerListaMovimientos(int[] IdSucursal)
        {
            return await _repositorioCajas.ObtenerListaMovimientos(IdSucursal);
        }

        public async Task<List<OperacionesCaja>> ObtenerListaOperaciones()
        {
            return await _repositorioCajas.ObtenerListaOperaciones();
        }

        public async Task<List<Denominacion>> ObtenerListaDenominaciones()
        {
            return await _repositorioCajas.ObtenerListaDenominaciones();
        }

        public async Task<bool> GuardarCaja(Caja caja)
        {
            return await _repositorioCajas.GuardarCaja(caja);
        }

        public async Task<bool> CajaAbierta(int[] IdSucursal)
        {
            return await _repositorioCajas.CajaAbierta(IdSucursal);
        }

        public async Task<bool> CancelarMovimientoCaja(int IdMovimiento, string Motivo)
        {
            return await _repositorioCajas.CancelarMovimientoCaja(IdMovimiento, Motivo);
        }
         
    }
}
