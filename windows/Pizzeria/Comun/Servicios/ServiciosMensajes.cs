﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Repositorios;

namespace Comun.Servicios
{
    public class ServiciosMensajes
    {
        private readonly IRepositorioMensajes _repositorioMensajes;

        public ServiciosMensajes(string usuario, string password)
		{
            _repositorioMensajes = new RepositorioMensajesWS(usuario, password);
		}

        public async Task<List<Mensaje>> ObtenerListaMensajes()
        {
            return await _repositorioMensajes.ObtenerListaMensaje();
        }

        public async Task<String> ActualizarMensaje(int id, string descripcion)
        {
            return await _repositorioMensajes.ActualizarMensaje(id, descripcion);
        }
    }
}
