﻿using System;
using System.Drawing.Printing;
using System.Drawing;
using Comun.Entidades;

namespace TicketingSystem
{
    public class Ticket
    {
        PrintDocument pdoc;
        public Pedido _pedido;
        public Sucursal _sucursal;

        public void print(int numeroCopias)
        {
            //var pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
             
            PaperSize psize = new PaperSize("Custom", 100, 200);
            ps.DefaultPageSettings.PaperSize = psize;

            //pd.Document = pdoc;
            //pd.Document.DefaultPageSettings.PaperSize = psize;
            pdoc.DefaultPageSettings.PaperSize.Height = 320;
            pdoc.DefaultPageSettings.PaperSize.Height = 820;

            pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += pdoc_PrintPage;

            for (var _numeroImpresiones = 0; _numeroImpresiones < numeroCopias; _numeroImpresiones++)
            {
                //pdoc.Print(); 
            } 
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            int caracteresXLinea = 20;

            Graphics graphics = e.Graphics;
            int startX = 5;
            int startY = 0;
            int Offset = 5;
            int Inline = 15;
            String underLine = "------------------------------------------";
            
            graphics.DrawString("NINO's PIZZA", new Font("Courier New", 14), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString("RFC: ", new Font("Courier New", 12), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString(_sucursal.Nombre, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString(_sucursal.Direccion, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString(_sucursal.Telefono, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString(_sucursal.Horario, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString(underLine, new Font("Courier New", 9), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString(string.Format("Pedido: {0}", _pedido.NumeroPedido), new Font("Courier New", 13), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            graphics.DrawString(string.Format("Fecha: {0} {1}", _pedido.FechaPedido.ToString().Substring(1, 10), _pedido.Hora), new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString("Nombre: " + _pedido.NombreCliente, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            graphics.DrawString(string.Format("Dirección: {0} {1}", _pedido.Calle, _pedido.NumeroExterior), new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            graphics.DrawString(string.Format("Colonia: {0}", _pedido.Colonia), new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            graphics.DrawString(string.Format("{0}", _pedido.Referencia), new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
           
            graphics.DrawString("Teléfono: " + this._pedido.TelefonoCliente, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
           
            graphics.DrawString(_pedido.Colonia, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString(underLine, new Font("Courier New", 9), new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            graphics.DrawString("Producto       Precio", new Font("Courier New", 10), new SolidBrush(Color.Black), startX, startY + Offset);
               
            //Si trae lista de productos
            if (_pedido.Productos != null)
            {
                int separador;

                foreach (var _producto in _pedido.Productos)
                {
                    _producto.Tamanio = _producto.Tamanio ?? "";

                    separador = caracteresXLinea - ( _producto.Tamanio.Length +  _producto.Precio.ToString().Length) ;

                    separador = separador < 0 ? 0 : separador;

                    Offset = Offset + Inline;
                    graphics.DrawString(string.Format("{0}", _producto.Producto), new Font("Courier New", 9), new SolidBrush(Color.Black), startX, startY + Offset);

                    Offset = Offset + Inline;
                    graphics.DrawString(string.Format("  {0}{1}${2}", _producto.Tamanio, new string(' ', separador), _producto.Precio), new Font("Courier New", 9), new SolidBrush(Color.Black), startX, startY + Offset);
           
                    //Si hay desglose de ingredientes 
                    if (_producto.Ingredientes != null)
                    {
                        foreach (var ingrediente in _producto.Ingredientes)
                        {
                            separador = caracteresXLinea - (ingrediente.Descripcion.Length + ingrediente.PrecioVenta.ToString().Length);
                            separador = separador < 0 ? 0 : separador; 
                          
                            Offset = Offset + Inline;
                            graphics.DrawString(string.Format("    {0}{1}$ {2}", ingrediente.Descripcion, new string(' ', separador), ingrediente.PrecioVenta), new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
                        }
                    } 
                }
            }  
            
            Offset = Offset + 20;

            graphics.DrawString("TOTAL: " + _pedido.Subtotal, new Font("Courier New", 9), new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + Inline;
            underLine = "------------------------------------------";
            graphics.DrawString(underLine, new Font("Courier New", 10), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + Inline;

            //Si el pedido será enviado a domicilio
            Offset = Offset + Inline; 
            graphics.DrawString("¡GRACIAS POR SU PREFERENCIA!", new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + Inline;
            graphics.DrawString("¡ESTE NO ES UN COMPROBANTE FISCAL!", new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + Inline;
            graphics.DrawString(_pedido.Mensaje, new Font("Courier New", 10), new SolidBrush(Color.Black), startX, startY + Offset);
  
        }
    }
}