﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun
{
     [JsonObject]
    public class EstadoPedido
    {
        [JsonProperty("id")]
        public int Id { get; set; }
         
        [JsonProperty("descripcion")]
        public string Descripcion { get; set; }
    }
}
