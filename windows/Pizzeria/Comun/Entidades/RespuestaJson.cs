﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Comun.Entidades
{
	[JsonObject]
	public class RespuestaJson
	{
		[JsonProperty("error")]
		public string Error { get; set; }
		
		[JsonProperty("msg")]
		public string Mensaje { get; set; }
	
		[JsonProperty("msgTecnico")]
        public string[] MensajeTecnico { get; set; }

		[JsonExtensionData]
		public IDictionary<string, JToken> Datos { get; set; }
	}

}