﻿﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comun.Entidades
{
	public class Pedido
    {
        [JsonProperty("id")]
		public int Id { get; set; }

        [JsonProperty("numero")]
	    public string NumeroPedido { get; set; }

		[JsonProperty("telefono")]
		public string TelefonoCliente { get; set; }

	    [JsonProperty("id_cliente")]
		public int? IdCliente { get; set; }

		[JsonProperty("cliente")]
		public string NombreCliente { get; set; }

		[JsonProperty("id_sucursal")]
		public int? IdSucursal { get; set; }

		[JsonProperty("sucursal")]
		public string NombreSucursal { get; set; }

		[JsonProperty("id_tipo_entrega")]
		public int? IdTipoEntrega { get; set; }

        [JsonProperty("id_metodo_pago")]
        public int? IdMetodoPago { get; set; }

        [JsonProperty("id_estatus")]
        public int? IdEstatus { get; set; }


        [JsonProperty("id_origen")]
        public int? IdOrigen { get; set; }

        [JsonProperty("origen")]
        public string Origen { get; set; } 


		[JsonProperty("estatus")]
		public string Estatus { get; set; }

		[JsonProperty("fecha_pedido")]
		public DateTime? FechaPedido { get; set; } 
 
        public String HoraCocina { get; set;}

        [JsonProperty("enviar_domicilio")]
        public bool EnviarPedido { get; set; } 

		[JsonProperty("domicilio")]
		public string Domicilio { get; set; }

        [JsonProperty("colonia")]
        public string Colonia { get; set; }

		[JsonProperty("hora")]
		public String Hora { get; set; }

		[JsonProperty("subtotal")]
		public decimal? Subtotal { get; set; }

		[JsonProperty("descuento")]
		public decimal? Descuento { get; set; }

        [JsonProperty("productos")]
		public List<PedidoProducto> Productos { get; set; } 
         
        [JsonProperty("cocina")]
        public List<PedidoEstatus> Cocina { get; set; } 

        [JsonProperty("pagado")]
        public bool Pagado { get; set; }


        [JsonProperty("urgente")]
        public int Urgente { get; set; }

        [JsonProperty("entregado")]
        public string Entregado { get; set; }

	    public string TiempoTranscurrido { get; set; }
	    public string Color { get; set; }

	    public string CartelUrgente { get; set; }

		[JsonProperty("nombreEmpleado")]
		public string NombreEmpleado { get; set; }

        [JsonProperty("id_domicilio")]
        public int IdDomicilio { get; set; }

        [JsonProperty("enviarCocina")]
        public int EnviarCocina { get; set; }


        [JsonProperty("mensaje")]
        public string Mensaje { get; set; }


        [JsonProperty("calle")]
        public string Calle { get; set; }


        [JsonProperty("num_ext")]
        public string NumeroExterior { get; set; }


        [JsonProperty("referencia")]
        public string Referencia { get; set; }
    }
}