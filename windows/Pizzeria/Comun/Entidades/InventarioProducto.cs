﻿using System;
using Newtonsoft.Json;

namespace Comun.Entidades
{
     [JsonObject]
    public class InventarioProducto
    {
        public int Id { get; set; }

        [JsonProperty("id_producto")]
        public int IdProducto { get; set; }

        [JsonProperty("producto")]
        public string Producto { get; set; } 

        [JsonProperty("cantidad")]
        public double Cantidad { get; set; }

        [JsonProperty("id_unidad_medida")]
        public int? IdUnidadMedida { get; set; } 

        [JsonProperty("unidadMedida")]
        public string UnidadMedida { get; set; }

        [JsonProperty("precio_costo")]
        public Decimal? Precio { get; set; }
    }
}
