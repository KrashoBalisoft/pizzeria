﻿using Newtonsoft.Json;

namespace Comun.Entidades
{
	[JsonObject]
	public class Sucursal
	{
        [JsonProperty("id")]
		public int Id { get; set; }
		[JsonProperty("nombre")]
		public string Nombre { get; set; }
		[JsonProperty("direccion")]
		public string Direccion { get; set; }
		[JsonProperty("telefono")]
		public string Telefono { get; set; }
		[JsonProperty("id_cliente")]
		public int? IdCliente { get; set; }

        [JsonProperty("horario")]
        public string Horario { get; set; } 
	}
}