﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Comun.Entidades
{
     [JsonObject]
    public class Inventario
    { 
        public int Id { get; set; }

        [JsonProperty("id_sucursal_origen")]
        public int? IdSucursalOrigen{ get; set; }

        [JsonProperty("sucursalOrigen")]
        public string SucursalOrigen { get; set; }

        [JsonProperty("id_sucursal_destino")]
        public int? IdSucursalDestino { get; set; }

        [JsonProperty("sucursalDestino")]
        public string SucursalDestino { get; set; }
     
        [JsonProperty("id_tipo_movimiento")]
        public int? IdTipoMovimiento { get; set; }

        [JsonProperty("tipoMovimiento")]
        public string TipoMovimiento { get; set; }

        [JsonProperty("fecha")]
        public DateTime? Fecha { get; set; } 

        [JsonProperty("folio")]
        public string Folio { get; set; }

        [JsonProperty("situacion")]
        public string Situacion { get; set; }
        
        [JsonProperty("productos")]
        public ObservableCollection<InventarioProducto> _productosEnLista;

    }
}
