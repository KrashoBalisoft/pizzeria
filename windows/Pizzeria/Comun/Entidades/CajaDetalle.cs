﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class CajaDetalle
    {
        public int Id { get; set; }
        [JsonProperty("id_caja")]
        public int IdCaja { get; set; }
        [JsonProperty("id_denominacion")]
        public int IdDenominacion { get; set; }
        [JsonProperty("cantidad")]
        public int Cantidad { get; set; } 
    }
}
