﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comun.Entidades
{
	[JsonObject]
	public class Cliente
	{
		public int Id { get; set; }

		[JsonProperty("telefono")]
        public string Telefono { get; set; }
		
        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("rfc")]
		public string RFC { get; set; }
		
        [JsonProperty("domicilio")]
		public string Domicilio { get; set; }
		
        [JsonProperty("cp")]
		public string CodigoPostal { get; set; }
		
        [JsonProperty("colonia")]
		public string Colonia { get; set; }
		
        [JsonProperty("correo")]
		public string Correo { get; set; }
		
        [JsonProperty("id_sucursal")]
		public int IdSucursal { get; set; }
		
        [JsonProperty("id_tipo_telefono")]
		public int IdTipoTelefono { get; set; }

        [JsonProperty("direcciones")]
        public List<ClienteDirecciones> Direcciones;
	}
}