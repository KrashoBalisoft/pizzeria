﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades.Catalogos
{
    public class Empleado
    {
        public int Id { get; set; }
        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("tipoEmpleado")]
        public string TipoEmpleado { get; set; }

        [JsonProperty("id_tipo_empleado")]
        public string IdTipoEmpleado { get; set; }


    }
}