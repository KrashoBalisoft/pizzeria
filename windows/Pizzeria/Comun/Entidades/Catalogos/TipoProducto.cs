﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comun.Entidades.Catalogos
{
    public class TipoProducto
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }


        public static List<TipoProducto> ObtenerLista()
        {
            var lista = new List<TipoProducto>() { 
                new TipoProducto { 
                    Id = "A", 
                    Descripcion = "Para Compra y Venta" 
                }, 
                new TipoProducto { 
                    Id = "V", 
                    Descripcion = "Para Venta (Pizzas)" 
                },
                 new TipoProducto { 
                    Id = "I", 
                    Descripcion = "Ingrediente" 
                } 

            };

            return lista;
        } 
    }
}
