﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comun.Entidades.Catalogos
{
    public class Estatus
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }


        public static List<Estatus> ObtenerLista()
        {
            var lista = new List<Estatus>() { 
                new Estatus { 
                    Id = "A", 
                    Descripcion = "Activo" 
                }, 
                new Estatus { 
                    Id = "I", 
                    Descripcion = "Inactivo" 
                } 
            };
            
            return lista;          
        }
    }
}
