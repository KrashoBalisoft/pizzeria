﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comun.Entidades.Catalogos
{
    public class ClasificacionProducto
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }


        public static List<ClasificacionProducto> ObtenerLista()
        {
            var lista = new List<ClasificacionProducto>() { 
                new ClasificacionProducto { 
                    Id = "1", 
                    Descripcion = "BEBIDAS" 
                }, 
                new ClasificacionProducto { 
                    Id = "2", 
                    Descripcion = "PIZZAS DE ESPECIALIDAD" 
                },
                 new ClasificacionProducto { 
                    Id = "3", 
                    Descripcion = "ENTREMESES" 
                },
                 new ClasificacionProducto { 
                    Id = "4", 
                    Descripcion = "SPAGHETTI" 
                },
                 new ClasificacionProducto { 
                    Id = "5", 
                    Descripcion = "PIZZA PERSONALIZADA" 
                }

            };

            return lista;
        } 
    }
}
