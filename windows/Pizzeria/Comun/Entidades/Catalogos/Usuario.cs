﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades.Catalogos
{
    [JsonObject]
    public class Usuario
    {
        public int Id { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("pass")]
        public string Password { get; set; }

        [JsonProperty("dPerfil")]
        public string Perfil { get; set; }

        [JsonProperty("id_perfil")]
        public string IdPerfil { get; set; }


        [JsonProperty("nombre")]
        public string Empleado { get; set; }

        [JsonProperty("id_empleado")]
        public string IdEmpleado { get; set; }


        [JsonProperty("estatus")]
        public string Estatus { get; set; }

        [JsonProperty("dEstatus")]
        public string dEstatus { get; set; }   

    }
}
