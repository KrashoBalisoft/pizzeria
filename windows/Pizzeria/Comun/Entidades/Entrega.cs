﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class Entrega
    {
        public int Id { get; set; }
        [JsonProperty("id_pedido")]
        public int IdPedido { get; set; }
        
        [JsonProperty("id_empleado")]
        public int IdEmpleado { get; set; }
        [JsonProperty("fecha_entrega_salida")]
        public DateTime FechaEntrega { get; set; }
        [JsonProperty("hora_entrega_salida")]
        public DateTime HoraEntrega { get; set; }
        [JsonProperty("fecha_entrega_retorno")]
        public DateTime FechaEntregaRetorno { get; set; }
        [JsonProperty("hora_entrega_retorno")]
        public DateTime HoraEntregaRetorno { get; set; }
        [JsonProperty("entregado")]
        public int Entregado { get; set; }

        [JsonProperty("observaciones")]
        public string Observaciones { get; set; } 
    }
}
