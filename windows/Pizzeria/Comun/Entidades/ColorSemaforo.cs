﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class ColorSemaforo
    {
        public int Id { get; set; }
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("tolerancia")]
        public int Tolerancia { get; set; }

        [JsonProperty("codigo")]
        public string Codigo { get; set; }

    }
}