﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class ClienteDirecciones
    {
        public int Id { get; set; }

        [JsonProperty("id_cliente")]
        public int IdCliente { get; set; }

        [JsonProperty("calle")]
        public string Calle { get; set; }

        [JsonProperty("numero_ext")]
        public string NumeroExterior { get; set; }

        [JsonProperty("colonia")]
        public string Colonia { get; set; }

         [JsonProperty("referencia")]
        public string Referencia { get; set; }
         [JsonProperty("domicilioConcatenado")]
         public string DomicilioConcatenado { get; set; }
    } 
}