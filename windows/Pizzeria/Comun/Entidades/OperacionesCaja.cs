﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class OperacionesCaja
    {
        public int Id { get; set; }
        [JsonProperty("descripcion")]
        public string Descripcion { get; set; } 
    }
}
