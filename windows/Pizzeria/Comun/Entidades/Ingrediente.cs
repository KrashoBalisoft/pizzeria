﻿using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
	public class Ingrediente
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("id_pedido_producto")]
        public int IdPedidoProducto { get; set; }

        [JsonProperty("descripcionIngrediente")]
        public string Descripcion { get; set; }

        [JsonProperty("id_ingrediente")]
        public int IdProducto { get; set; }
        [JsonProperty("extra")]
        public bool EsExtra { get; set; }
        [JsonProperty("precio_venta")]
        public decimal? PrecioVenta { get; set; }
	}
}