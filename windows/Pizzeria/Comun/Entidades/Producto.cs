﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;

namespace Comun.Entidades
{
	[JsonObject]
	public class Producto
	{

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("descripcion")]
        public string Nombre { get; set; }


        [JsonProperty("cantidad_ingredientes")]
        public int? CantidadIngredientes { get; set; }


        [JsonProperty("cantidad")]
        public int? Cantidad { get; set; }

        [JsonProperty("cantidad_minima")]
        public int? CantidadMinima { get; set; }

        [JsonProperty("precio_venta")]
        public decimal? PrecioVenta { get; set; }
        [JsonProperty("tipo_producto")]
        public char TipoProducto { get; set; }
        [JsonProperty("id_clasificacion_producto")]
        public int? IdClasificacion { get; set; }
        [JsonProperty("tamanios")]
        public List<ProductoTamanio> Tamanios { get; set; }

        [JsonProperty("imagen")]
        public String NombreImagen { get; set; }

        [JsonProperty("enviar_cocina")]
        public bool EnviarCocina { get; set; }

        [JsonProperty("seguir")]
        public int? Seguir { get; set; }  
	}
}