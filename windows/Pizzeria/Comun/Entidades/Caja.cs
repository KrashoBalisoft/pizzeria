﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class Caja
    {
        public int Id { get; set; }
        [JsonProperty("id_operacion_caja")]
        public int IdOperacion { get; set; } 
        [JsonProperty("operacionCaja")]
        public string DescripcionOperacion { get; set; }

        [JsonProperty("fecha")]
        public DateTime Fecha { get; set; }
        [JsonProperty("id_usuario")]
        public int IdUsuario { get; set; }
        [JsonProperty("total")]
        public Decimal Total { get; set; }
        [JsonProperty("id_sucursal")]
        public int IdSucursal { get; set; }
        [JsonProperty("observaciones")]
        public string Observaciones { get; set; }

        public string UsuarioOperacion { get; set; }

        [JsonProperty("detalle")]
        public List<CajaDetalle> Detalle { get; set; }



    }
}
