﻿using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class ProductoTamanio
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("id_producto")]
        public int IdProducto { get; set; }
        [JsonProperty("producto")]
        public string Producto { get; set; }
        [JsonProperty("id_tamanio")]
        public int? IdTamanio { get; set; }
        [JsonProperty("tamanio")]
        public string Tamanio { get; set; }
        [JsonProperty("precio_venta")] 
        public decimal? PrecioVenta { get; set; }

        [JsonProperty("estatus")]
        public string Estatus { get; set; }
        
    } 
}
