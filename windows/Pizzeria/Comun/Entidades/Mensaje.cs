﻿using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
    public class Mensaje
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("descripcion")]
        public string Descripcion { get; set; } 
    } 
}
