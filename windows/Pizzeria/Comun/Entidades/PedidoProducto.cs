﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comun.Entidades
{
    [JsonObject]
	public class PedidoProducto
	{
        [JsonProperty("id")]
		public int Id { get; set; }
       
        [JsonProperty("id_pedido")]
        public int? IdPedido { get; set; }
        
        [JsonProperty("id_producto")]
		public int? IdProducto { get; set; }
        
        [JsonProperty("producto")]
		public string Producto { get; set; }
        
        [JsonProperty("id_tamanio")]
        public int? IdTamanio { get; set; }
        
        [JsonProperty("tamanio")]
        public string Tamanio { get; set; }

        [JsonProperty("precio")]
	    public decimal? Precio { get; set; }

        [JsonProperty("total_ingredientes_extras")]
        public decimal? TotalIngredientesExtras { get; set; }
       

        [JsonProperty("cantidad")]
        public int Cantidad { get; set; }


	    [JsonProperty("observaciones")]
        public string Observaciones { get; set; }
        
        [JsonProperty("ingredientes")]
		public List<Ingrediente> Ingredientes { get; set; }

        public decimal? Total { get; set; }



        [JsonProperty("id_mitad_uno")]
        public int IdMitadUno { get; set; }

        [JsonProperty("id_mitad_dos")]
        public int IdMitadDos { get; set; }  
	}
}