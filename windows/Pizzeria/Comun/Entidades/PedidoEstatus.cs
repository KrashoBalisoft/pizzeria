﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comun.Entidades
{
	public class PedidoEstatus
	{
        [JsonProperty("id")]
		public int Id { get; set; }
       
        [JsonProperty("id_pedido")]
        public int? IdPedido { get; set; }
        
        [JsonProperty("id_estatus")]
		public int? IdEstatus { get; set; }
        
        [JsonProperty("fecha_hora_inicio")]
		public String HoraInicio { get; set; }
        
        [JsonProperty("fecha_hora_final")]
        public String HoraFinal { get; set; }
      
	}
}