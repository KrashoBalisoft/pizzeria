﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Comun
{
    public static class Crypto
    {
        public static string MD5Hash(string texto)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(Encoding.ASCII.GetBytes(texto));

            byte[] result = md5.Hash;

            var strBuilder = new StringBuilder();

            foreach (byte t in result)
            {
                strBuilder.Append(t.ToString("x2"));
            }

            return strBuilder.ToString(); 
        }
    }
}
