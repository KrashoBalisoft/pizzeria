﻿using Comun.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Comun
{
	public class MetodosWS
	{
		public static async Task<string> ObtenerCadenaJson(string url, string json)
		{
			url = ConstantesWS.URL + url;

			string cadenaJson;
			using (var cliente = new HttpClient())
			{
				var datos = new NameValueCollection();
				datos["datos"] = json;

				var pares = new List<KeyValuePair<string, string>> {new KeyValuePair<string, string>("datos", json)};
				var contenido = new FormUrlEncodedContent(pares);
				var resultado = await cliente.PostAsync(url, contenido);

				var datosDevueltos = resultado.Content.ReadAsByteArrayAsync();
				cadenaJson = Encoding.Default.GetString(datosDevueltos.Result);
			}

			return cadenaJson;
		}

        public static async Task<List<T>> ObtenerLista<T>(dynamic obj, string ObtenerListaURL)
        {
            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, ObtenerListaURL);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var lista = respuesta.Datos["datos"].ToObject<List<T>>();

            return lista;

        }


        public static async Task<T> ObtenerRegistroPorId<T>(dynamic obj, string ObtenerListaURL)
        {
            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, ObtenerListaURL);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return default(T);
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return default(T);
            }

            var lista = respuesta.Datos["datos"].First.ToObject<T>();

            return lista;

        }

        public static async Task<string> Insertar(dynamic obj, string InsertarURL)
        {
            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, InsertarURL);
            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return respuesta.Mensaje;
            }

            return "true";
        }

        public static async Task<List<T>> ObtenerPorId<T>(dynamic obj, string ObtenerPorIdURL)
        {
            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, ObtenerPorIdURL);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var metodo = respuesta.Datos["datos"].First.ToObject<T>();


            return metodo;
        }

        public static async Task<string> Actualizar(dynamic obj, string ActualizarURL)
        {

            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, ActualizarURL);
            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return respuesta.Mensaje;
            }

            return "true";
        }

        public static async Task<string> Eliminar(dynamic obj, int id, string EliminarURL)
        {

            var respuesta = await MetodosWS.ObtenerRespuesta<RespuestaJson>(obj, EliminarURL);
 
            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return respuesta.Mensaje;
            }

            return "true";
        }


        public static async Task<RespuestaJson> ObtenerRespuesta<RespuestaJson>(dynamic obj, string ObtenerListaURL)
        {
            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            return respuesta;
        }
	}
}