﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioPedidosCocinaWS : IRepositorioPedidosCocina
    {
        private readonly string _usuario;
        private readonly string _password;

        public RepositorioPedidosCocinaWS(string usuario, string password)
        {
            _usuario = usuario;
            _password = password;
        }

        //private const string BuscarPedidosParaCocinaURL = "?r=pedido/buscar";
        private const string BuscarPedidosParaCocinaURL = "?r=pedido/pendientesDeCocinar";
        private const string BuscarPedidosEnPreparacionCocinaURL = "?r=pedido/enPreparacion";
        private const string TerminarPedidoCocinaURL = "?r=pedido/terminar";
        private const string EnviarPedidoAPreparacionURL = "?r=pedido/preparar";

        public async Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        sucursales = listaSucursales
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = String.Empty;

            if (estadoPedidos == 1) //Pendientes de preparar
            {
                cadenaJson = await  MetodosWS.ObtenerCadenaJson(BuscarPedidosParaCocinaURL, json);
            }
            else //En preparacion
            {
                cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPedidosEnPreparacionCocinaURL, json);
            }


            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var pedidos = respuesta.Datos["datos"].ToObject<IList<Pedido>>();

            return CalcularTiempo(pedidos);
        } 

        private IList<Pedido> CalcularTiempo(IList<Pedido> listaPedidos)
        {
            int item = 0;
            TimeSpan tiempoTranscurrido;
            DateTime horaInicio;
            DateTime horaActual = DateTime.Now;
            string cadenaTiempo;

            foreach (var pedido in listaPedidos)
            {
                horaInicio = DateTime.Parse(string.Format("{0} {1}", listaPedidos[item].FechaPedido.ToString().Substring(0, 10), listaPedidos[item].Hora));
   
                tiempoTranscurrido = horaActual.Subtract(horaInicio);
                
                cadenaTiempo =
                    DateTime.Parse(string.Format("{0}:{1}:{2}", tiempoTranscurrido.Hours, tiempoTranscurrido.Minutes,
                        tiempoTranscurrido.Seconds)).ToString(CultureInfo.InvariantCulture).Substring(11, 8);


                listaPedidos[item].TiempoTranscurrido = cadenaTiempo;

                item++;
            } 

            return listaPedidos;
        }

        public async Task<bool> EnviarPedidoAPreparacion(int Id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id = Id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EnviarPedidoAPreparacionURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> TerminarPedido(int Id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id = Id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(TerminarPedidoCocinaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }
    }
}