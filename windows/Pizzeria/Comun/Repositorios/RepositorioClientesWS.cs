﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
	public class RepositorioClientesWS : IRepositorioClientes
	{
		private const string BuscarPorTelefonoURL = "?r=cliente/buscarPorTelefono";
		private const string AgregarClienteURL = "?r=cliente/insertar";
		private const string ModificarClienteURL = "?r=cliente/actualizar";
        private const string ObtenerListaURL = "?r=cliente/buscar";
        private const string EliminarURL = "?r=cliente/eliminar";

		private readonly string _usuario;
		private readonly string _password;

		public RepositorioClientesWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

		public async Task<Cliente> BuscarClientePorTelefono(string telefono)
		{
			var obj = new
			{
				datos = new
				{
					cliente = new
					{
						telefono
					},
					usuario = _usuario,
					password = _password
				}
			};
			var json = JsonConvert.SerializeObject(obj);
		    
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPorTelefonoURL, json);
 
			var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

			if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
			{
				return null;
			}

			if (respuesta.Datos["datos"].First == null)
			{
				return null;
			}

			var cliente = respuesta.Datos["datos"].First.ToObject<Cliente>();
			//var cliente = JsonConvert.DeserializeObject<Cliente>();
			return cliente;
		}

		public async Task<Cliente> AgregarCliente(Cliente cliente)
		{
			var obj = new
			{
				datos = new
				{
					usuario = _usuario,
					password = _password,
					cliente
				}
			};

			var json = JsonConvert.SerializeObject(obj);
			
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(AgregarClienteURL, json); 
		   	
            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);
            
			
			if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
			{
				return null;
			}

			if (respuesta.Datos["datos"].First == null)
			{
				return null;
			}
			
            var datosCliente = respuesta.Datos["datos"].ToObject<Cliente>();


            return datosCliente;
		}

		public async Task<bool> ModificarCliente(Cliente Cliente)
		{

			var obj = new
			{
				datos = new
				{
					usuario = _usuario,
					password = _password,
                    cliente = new
                    {
                        id = Cliente.Id,
                        nombre = Cliente.Nombre,
                        rfc = Cliente.RFC,
                        telefono = Cliente.Telefono,
                        cp = Cliente.CodigoPostal,
                        domicilio = Cliente.Domicilio,
                        colonia = Cliente.Colonia,
                        correo = Cliente.Correo,
                        id_tipo_telefono = Cliente.IdTipoTelefono,
                        id_sucursal = Cliente.IdSucursal, 
                        direcciones = Cliente.Direcciones
                    }
				}
			};

			var json = JsonConvert.SerializeObject(obj);
			var cadenaJson = await MetodosWS.ObtenerCadenaJson(ModificarClienteURL, json);

			var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);


			if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
			{
				return false;
			}

			return true;
		}

        public async Task<string> Insertar(Cliente Cliente)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    cliente = new
                    {
                        nombre = Cliente.Nombre,
                        rfc = Cliente.RFC,
                        telefono = Cliente.Telefono,
                        cp = Cliente.CodigoPostal,
                        domicilio = Cliente.Domicilio,
                        colonia = Cliente.Colonia,
                        correo = Cliente.Correo,
                        id_tipo_telefono = Cliente.IdTipoTelefono,
                        id_sucursal = Cliente.IdSucursal
                    }
                }
            };

            var respuesta = await MetodosWS.Insertar(obj, AgregarClienteURL);
            return respuesta;

        }

        public async Task<List<Cliente>> ObtenerListaClientes()
        {
            var obj = new
            {
                datos = new
                {
                    clientes = new
                    {

                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<Cliente>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    cliente = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }

	}


}