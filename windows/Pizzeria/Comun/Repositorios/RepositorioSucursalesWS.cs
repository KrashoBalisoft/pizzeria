﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioSucursalesWS : IRepositorioSucursales
    {
        private const string ObtenerSucursalPorIDURL = "?r=sucursal/buscarPorId";
        private const string ObtenerListaSucursalesURL = "?r=sucursal/buscar";
        private const string InsertarURL = "?r=sucursal/insertar";
        private const string ActualizarURL = "?r=sucursal/actualizar";
        private const string EliminarURL = "?r=sucursal/eliminar";

        private readonly string _usuario;
        private readonly string _password;

        public RepositorioSucursalesWS(string usuario, string password)
        {
            _usuario = usuario;
            _password = password;
        }

        public async Task<Sucursal> ObtenerDatosSucursal(int id)
        {
            var obj = new
            {
                datos = new
                {
                    sucursal = new
                    {
                        id
                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerSucursalPorIDURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            //Obtenemos sólo el primer record
            var sucursal = respuesta.Datos["datos"].First.ToObject<Sucursal>();

            return sucursal;
        }

        public async Task<List<Sucursal>> ObtenerListaSucursales()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password 
                }
            };

            var sucursales = await MetodosWS.ObtenerLista<Sucursal>(obj, ObtenerListaSucursalesURL);
            return sucursales;
        }

        public async Task<string> Insertar(string nombre, string direccion, string telefono, string horario)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    sucursal = new 
                    { 
                        nombre,
                        direccion,
                        telefono,
                        horario
                    }
                }
            };


            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string nombre, string direccion, string telefono, string horario)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    sucursal = new
                    {
                        id,
                        nombre,
                        direccion,
                        telefono,
                        horario
                    }
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    sucursal = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }

    } 
}
