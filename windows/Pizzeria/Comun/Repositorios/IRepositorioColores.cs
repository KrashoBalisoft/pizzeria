﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
    public interface IRepositorioColores
    {
        Task<List<ColorSemaforo>> ObtenerListaColores();

        Task<string> Insertar(string nombre, string codigo, string tolerancia);

        Task<string> Actualizar(int id, string nombre, string codigo, string tolerancia);

        Task<string> Eliminar(int id);

    }
}
