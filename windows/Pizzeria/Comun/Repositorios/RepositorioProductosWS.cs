﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Comun.Repositorios
{
	public class RepositorioProductosWS : IRepositorioProductos
	{
		private const string BuscarParaVenderURL = "?r=producto/buscarParaVender";
        private const string BuscarIngredientesURL = "?r=producto/buscarIngredientes";
        private const string ObtenerListaURL = "?r=producto/buscar";
        private const string EliminarProductoURL = "?r=producto/eliminar";
        private const string AgregarProductoURL = "?r=producto/insertar";
	    private const string ActualizarProductoURL = "?r=producto/actualizar";

		private readonly string _usuario;
		private readonly string _password;

		public RepositorioProductosWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

		public async Task<List<Producto>> ObtenerProductos()
		{
			var obj = new
			{
				datos = new
				{
					usuario = _usuario,
					password = _password
				}
			};

			var json = JsonConvert.SerializeObject(obj);
			var cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarParaVenderURL, json);

			var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

			var productos = respuesta.Datos["datos"].ToObject<List<Producto>>(); 

			return productos;
		}

        public async Task<List<Producto>> ObtenerIngredientes()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarIngredientesURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }
            var ingredientes = respuesta.Datos["datos"].ToObject<List<Producto>>();

            return ingredientes;
        }

        public async Task<List<Producto>> ListadoProductos()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<Producto>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    producto = new
                    {
                        id
                    }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarProductoURL);
            return respuesta;
        }

        public async Task<bool> Agregar(Producto producto)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    producto

                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(AgregarProductoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> Actualizar(Producto producto)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    producto 
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ActualizarProductoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }
	}
}