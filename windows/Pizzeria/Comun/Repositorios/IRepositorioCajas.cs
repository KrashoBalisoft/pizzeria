﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
    public interface IRepositorioCajas
    {
        Task<List<Caja>> ObtenerListaMovimientos(int[] IdSucursal);

        Task<List<OperacionesCaja>> ObtenerListaOperaciones();

        Task<List<Denominacion>> ObtenerListaDenominaciones();

        Task<bool> GuardarCaja(Caja caja);

        Task<bool> CajaAbierta(int[] IdSucursal);

        Task<bool> CancelarMovimientoCaja(int IdMovimiento, string Motivo);
    }
}
