﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioCajasWS: IRepositorioCajas
    {
        private const string ObtenerListaOperacionesPorFechaURL = "?r=caja/buscarOperacionesCajaXFecha";
        private const string ObtenerListaOperacionesURL = "?r=operacionesCaja/buscar";
        private const string ObtenerListaDenominacionesURL = "?r=denominacion/buscar";
        private const string GuardarMovimientoCajaURL = "?r=caja/insertar";
        private const string VerificarEstadoCajaURL = "?r=caja/buscarCajaAbierta";
        private const string CancelarMovimientoCajaURL = "?r=caja/cancelar";

		private readonly string _usuario;
		private readonly string _password;

        public RepositorioCajasWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		} 

        public async Task<List<Caja>> ObtenerListaMovimientos(int[] IdSucursal)
        {
            var obj = new
            {
                datos = new
                {
                    caja = new
                    {
                        fecha = DateTime.Now.ToString("yyyy-MM-dd"),
                        id_sucursal = IdSucursal
                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaOperacionesPorFechaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var movimientos = respuesta.Datos["datos"].ToObject<List<Caja>>();

            return movimientos;
        }

        public async Task<List<OperacionesCaja>> ObtenerListaOperaciones()
        {
            var obj = new
            {
                datos = new
                { 
                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaOperacionesURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var listaOperaciones = respuesta.Datos["datos"].ToObject<List<OperacionesCaja>>();

            return listaOperaciones;
        }

        public async Task<List<Denominacion>> ObtenerListaDenominaciones()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaDenominacionesURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var listaDenominaciones = respuesta.Datos["datos"].ToObject<List<Denominacion>>();

            return listaDenominaciones;
        }

        public async Task<bool> GuardarCaja(Caja caja)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    caja = new 
                           {
                              id_operacion_caja = caja.IdOperacion, 
                              id_usuario = caja.IdUsuario, 
                              total = caja.Total, 
                              id_sucursal = caja.IdSucursal,
                              observaciones = caja.Observaciones,
                              detalle = caja.Detalle
                           }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(GuardarMovimientoCajaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }


        public async Task<bool> CajaAbierta(int[] IdSucursal)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    caja = new
                    {
                        id_sucursal = IdSucursal,
                        fecha = ""
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(VerificarEstadoCajaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CancelarMovimientoCaja(int IdMovimiento, string  Motivo)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    caja = new
                    {
                        id = IdMovimiento, 
                        motivo = Motivo
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(CancelarMovimientoCajaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        } 
    }
}
