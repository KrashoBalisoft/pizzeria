﻿using System.Collections.Generic;
using Comun.Entidades;
using System.Threading.Tasks;

namespace Comun.Repositorios
{
    public interface IRepositorioPedidos
    {
        Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos);

        Task<Pedido> BuscarPedido(int id);

		Task<IList<Pedido>> ObtenerTodos();

        Task<IList<Pedido>> ObtenerPedidosPorSucursal(int[] listaSucursales);
        Task<bool> EnviarPedidoAPreparacion(int id);
        Task<bool> TerminarPedido(int id);
        Task<bool> CancelarPedido(int id);
	    Task<bool> AgregarPedido(Pedido pedido); 
        Task<Pedido> AgregarPedidoSucursal(Pedido pedido);
        Task<bool> CobrarPedido(int id);
        Task<bool> EnviarPedidoALaCocina(int id);
        Task<bool> TransferirPedido(int IdPedido, int IdSucursal); 
        Task<bool> MandarPedidoUrgente(int IdPedido);
        Task<bool> AsignarPedido(int IdPedido, int IdEmpleado); 
        Task<bool> ConfirmarPedido(int IdPedido, int Entregado, string Observaciones); 
        Task<IList<Pedido>> ObtenerPedidosPorSucursalyFecha(int[] listaSucursales, string fechaInicial, string fechaFinal);


    }
}