﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
	public interface IRepositorioClientes
	{
		Task<Cliente> BuscarClientePorTelefono(string telefono);
		Task<Cliente> AgregarCliente(Cliente cliente);
		Task<bool> ModificarCliente(Cliente cliente);

        Task<List<Cliente>> ObtenerListaClientes();

        Task<string> Insertar(Cliente cliente);

        Task<string> Eliminar(int id);
	}
}