﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
	public interface IRepositorioProductos
	{

		Task<List<Producto>> ObtenerProductos();
        Task<List<Producto>> ObtenerIngredientes();

        Task<List<Producto>> ListadoProductos();
        Task<string> Eliminar(int Id);
        Task<bool> Agregar(Producto producto);
        Task<bool> Actualizar(Producto producto); 
	}
}