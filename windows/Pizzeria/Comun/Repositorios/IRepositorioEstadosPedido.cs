﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Comun.Repositorios
{
    public interface IRepositorioEstadosPedido
    {
        Task<List<EstadoPedido>> ObtenerListaEstadoPedido();
    }
}
