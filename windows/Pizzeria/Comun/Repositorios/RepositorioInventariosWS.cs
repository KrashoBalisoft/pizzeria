﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioInventariosWS: IRepositorioInventarios
    {
        private const string ObtenerListadoURL = "?r=inventario/buscarMovimientos";
        private const string InsertarURL = "?r=inventario/insertar";
        private const string ActualizarURL = "?r=inventario/actualizar";
        private const string EliminarURL = "?r=inventario/cancelar";

		private readonly string _usuario;
		private readonly string _password;

        public RepositorioInventariosWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

        public async Task<List<Inventario>> ObtenerListado()
        {
            var obj = new
            {
                datos = new
                {
                    inventario = new
                    {
                        incluirProductos = true
                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListadoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var inventarios = respuesta.Datos["datos"].ToObject<List<Inventario>>();
              
            return inventarios;
        }

        public async Task<Inventario> Insertar(Inventario _inventario)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                   inventario = _inventario 
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(InsertarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var movimiento = respuesta.Datos["datos"].ToObject<Inventario>();

            return movimiento;
        }

        public async Task<bool> Actualizar(Inventario inventario)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    inventario
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ActualizarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    inventario = new {id} 
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EliminarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

    }
}
