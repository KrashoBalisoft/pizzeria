﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
    public interface IRepositorioMensajes
    {
        Task<List<Mensaje>> ObtenerListaMensaje();

        Task<String> ActualizarMensaje(int id, string descripcion); 
    }
}
