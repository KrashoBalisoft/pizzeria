﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Servicios;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioPedidosWS : IRepositorioPedidos
    {
        private readonly string _usuario;
        private readonly string _password;

        private ServiciosColores _servicioColores  = new ServiciosColores(ConstantesWS.Usuario, ConstantesWS.Password);
        private List<ColorSemaforo> listaColores; 

        public RepositorioPedidosWS(string usuario, string password)
        {
            _usuario = usuario;
            _password = password;
        } 

        private const string BuscarPedidosPorSucursalURL = "?r=pedido/buscar";
        private const string BuscarPedidosParaCocinaURL = "?r=pedido/pendientesDeCocinar";
        private const string BuscarPedidosEnPreparacionCocinaURL = "?r=pedido/enPreparacion";
        private const string TerminarPedidoCocinaURL = "?r=pedido/terminar";
        private const string CancelarPedidoCocinaURL = "?r=pedido/cancelar";
        private const string EnviarPedidoAPreparacionURL = "?r=pedido/preparar";
        private const string TransferirPedidoASucursalURL = "?r=pedido/transferir";
        private const string EnviarPedidoACocinaURL = "?r=pedido/enviarACocina";
		private const string AgregarPedidoURL = "?r=pedido/insertar";
		private const string ObtenerTodosLosPedidosURL = "?r=pedido/todos";
        private const string PagarPedidoURL = "?r=pedido/pagar";
        private const string MandarPedidoUrgenteURL = "?r=pedido/urgente";
        private const string EntregarPedidoUrgenteURL = "?r=entrega/insertar";
        private const string RegresarPedidoURL = "?r=entrega/regreso";

        public async Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos)
        {
            //Cargamos el semáforo
            listaColores = await _servicioColores.ObtenerListaColores();

            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        sucursales = listaSucursales
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            string cadenaJson;

            if (estadoPedidos == 1) //Pendientes de preparar
            {
                cadenaJson = await  MetodosWS.ObtenerCadenaJson(BuscarPedidosParaCocinaURL, json);
            }
            else //En preparacion
            {
                cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPedidosEnPreparacionCocinaURL, json);
            }
             
            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var pedidos = respuesta.Datos["datos"].ToObject<IList<Pedido>>();

            return CalcularTiempo(pedidos);
        }

        public async Task<Pedido> BuscarPedido(int id)
        {
            //Cargamos el semáforo
            listaColores = await _servicioColores.ObtenerListaColores();

            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        pedidos = new []{id},
                        incluirProductos = true,
                        incluirIngredientes = true
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            string cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPedidosPorSucursalURL, json);
            
            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var _pedido =  respuesta.Datos["datos"].First.ToObject<Pedido>();

            return _pedido;
        }

		public async Task<IList<Pedido>> ObtenerTodos()
		{
			var obj = new
			{
				datos = new
				{
					usuario = _usuario,
					password = _password,
				}
			};

			var json = JsonConvert.SerializeObject(obj);
			string cadenaJson;

			cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerTodosLosPedidosURL, json);

			var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

			if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
			{
				return null;
			}

			if (respuesta.Datos["datos"].First == null)
			{
				return null;
			}

			var pedidos =  respuesta.Datos["datos"].ToObject<IList<Pedido>>();

			return pedidos;
		}

        public async Task<IList<Pedido>> ObtenerPedidosPorSucursalyFecha(int[] listaSucursales, string fechaInicial, string fechaFinal)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        sucursales = listaSucursales,
                        incluirProductos = true,
                        incluirIngredientes = true, 
                        fechaInicial, 
                        fechaFinal
                    } 
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            string cadenaJson;

            cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPedidosPorSucursalURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var pedidos = respuesta.Datos["datos"].ToObject<IList<Pedido>>();

            return pedidos;
        }

        public async Task<IList<Pedido>> ObtenerPedidosPorSucursal(int[] listaSucursales)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        sucursales = listaSucursales,
                        incluirProductos = true,
                        incluirIngredientes = true 
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            string cadenaJson;

            cadenaJson = await MetodosWS.ObtenerCadenaJson(BuscarPedidosPorSucursalURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var pedidos = respuesta.Datos["datos"].ToObject<IList<Pedido>>();

            return pedidos;
        } 

        private IList<Pedido> CalcularTiempo(IList<Pedido> listaPedidos)
        {
            int item = 0;
            TimeSpan tiempoTranscurrido;
            DateTime horaInicio;
            DateTime horaActual = DateTime.Now;
            string cadenaTiempo; 

    
            foreach (var pedido in listaPedidos)
            {
                //Comprobamos que tenga detalle en los datos de la cocina
                if (pedido.Cocina.Count == 0) continue;
               
                horaInicio = DateTime.Parse(pedido.Cocina[0].HoraInicio);

                listaPedidos[item].HoraCocina = pedido.Cocina[0].HoraInicio.Substring(11);

                tiempoTranscurrido = horaActual.Subtract(horaInicio);
                
                cadenaTiempo =
                    DateTime.Parse(string.Format("{0}:{1}:{2}", tiempoTranscurrido.Hours, tiempoTranscurrido.Minutes,
                        tiempoTranscurrido.Seconds)).ToString(CultureInfo.InvariantCulture).Substring(10, 9); 

                listaPedidos[item].TiempoTranscurrido = cadenaTiempo;

                //EL pedido si es urgente, lo imprime en rojo.... 
                if (pedido.Urgente == 1)
                {
                    listaPedidos[item].Color = "Red";
                    listaPedidos[item].CartelUrgente = "Urgente";

                    item++;

                    continue; 
                }

                //Aquí irá el cálculo de los colores, según el semáforo establecido
                if (listaColores.Any())
                {
                    int rangos = listaColores.Count - 1;  
    
                    for (int i = 0; i < rangos; i++)
                    {
                        if (tiempoTranscurrido.TotalMinutes >= listaColores[i].Tolerancia && tiempoTranscurrido.TotalMinutes < listaColores[i + 1].Tolerancia)
                        {
                            listaPedidos[item].Color = listaColores[i].Color;
                        }

                        //Si es igual o mayor al tiempo máximo
                        if (tiempoTranscurrido.TotalMinutes >= listaColores[rangos].Tolerancia)
                        {
                            listaPedidos[item].Color = listaColores[rangos].Color;

                            break;
                        } 
                    } 
                } 

                item++;
            } 

            return listaPedidos;
        } 

        public async Task<bool> EnviarPedidoAPreparacion(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EnviarPedidoAPreparacionURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> EnviarPedidoALaCocina(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EnviarPedidoACocinaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> TransferirPedido(int IdPedido, int IdSucursal)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id = IdPedido, 
                        id_sucursal = IdSucursal
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(TransferirPedidoASucursalURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> TerminarPedido(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(TerminarPedidoCocinaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

	    public async Task<bool> AgregarPedido(Pedido pedido)
	    {
			var obj = new
			{
				datos = new
				{
					usuario = _usuario,
					password = _password,
					pedido
				}
			};

			var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(AgregarPedidoURL, json);

			var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

			if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
			{
				return false;
			}

			return true;
	    }

        public async Task<Pedido> AgregarPedidoSucursal(Pedido pedido)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    pedido
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(AgregarPedidoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var _pedido = respuesta.Datos["datos"].First.ToObject<Pedido>();

            return _pedido; 
        } 

        public async Task<bool> CobrarPedido(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    pedido = new
                               {
                                  id
                               }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(PagarPedidoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CancelarPedido(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(CancelarPedidoCocinaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> MandarPedidoUrgente(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    pedido = new
                    {
                        id
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(MandarPedidoUrgenteURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> AsignarPedido(int id_pedido, int id_empleado)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    entrega = new
                    {
                        id_pedido, 
                        id_empleado, 
                        fecha_entrega_salida = DateTime.Now.Date.ToString("yyyy-MM-dd"),
                        hora_entrega_salida = DateTime.Now.TimeOfDay
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EntregarPedidoUrgenteURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ConfirmarPedido(int id_pedido, int entregado, string observaciones)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,
                    entrega = new
                    {
                        id_pedido,  
                        fecha_entrega_regreso = DateTime.Now.Date.ToString("yyyy-MM-dd"),
                        hora_entrega_regreso = DateTime.Now.TimeOfDay, 
                        entregado, 
                        observaciones
                    }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(RegresarPedidoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

    }
}