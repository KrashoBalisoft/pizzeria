﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioColoresWS: IRepositorioColores
    {
        private const string ObtenerListaColoresURL = "?r=colores/buscar";

        private const string InsertarURL = "?r=colores/insertar";
        private const string ActualizarURL = "?r=colores/actualizar";
        private const string EliminarURL = "?r=colores/eliminar";


		private readonly string _usuario;
		private readonly string _password;

        public RepositorioColoresWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

        public async Task<List<ColorSemaforo>> ObtenerListaColores()
        {
            var obj = new
            {
                datos = new
                {
                    colores = new
                    {
                         
                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaColoresURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var colores = respuesta.Datos["datos"].ToObject<List<ColorSemaforo>>(); 

            return colores;
        }

        public async Task<string> Insertar(string color, string codigo, string tolerancia)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    colores = new
                    {
                        color,
                        codigo,
                        tolerancia,
                    }
                }
            };


            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string color, string codigo, string tolerancia)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    colores = new
                    {
                        id,
                        color,
                        codigo,
                        tolerancia,
                    }
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    colores = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }

    }
}