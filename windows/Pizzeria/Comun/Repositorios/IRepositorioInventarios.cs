﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
    public interface IRepositorioInventarios
    {

        Task<List<Inventario>> ObtenerListado();
    
        Task<Inventario> Insertar(Inventario _inventario);

        Task<bool> Actualizar(Inventario _inventario);

        Task<bool> Eliminar(int id);
    }
}
