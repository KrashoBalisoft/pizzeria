﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioMensajesWS : IRepositorioMensajes
    {
        private const string ObtenerListaMensajeURL = "?r=mensaje/buscar";
        private const string ActualizarURL = "?r=mensaje/actualizar";


        private readonly string _usuario;
        private readonly string _password;

        public RepositorioMensajesWS(string usuario, string password)
        {
            _usuario = usuario;
            _password = password;
        }

        public async Task<List<Mensaje>> ObtenerListaMensaje()
        {
            var obj = new
            {
                datos = new
                {
                    colores = new
                    {

                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaMensajeURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var mensaje = respuesta.Datos["datos"].ToObject<List<Mensaje>>();

            return mensaje;
        }

        public async Task<String> ActualizarMensaje(int id, string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    mensaje = new
                    {
                        id,
                        descripcion
                    }
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }
    }
}
