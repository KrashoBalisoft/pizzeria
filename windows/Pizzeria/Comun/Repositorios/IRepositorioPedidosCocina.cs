﻿using System.Collections.Generic;
using Comun.Entidades;
using System.Threading.Tasks;

namespace Comun.Repositorios
{
    public interface IRepositorioPedidosCocina
    {
        Task<IList<Pedido>> BuscarPedidosCocina(int[] listaSucursales, int estadoPedidos);
        Task<bool> EnviarPedidoAPreparacion(int Id);
        Task<bool> TerminarPedido(int Id);
    }
}