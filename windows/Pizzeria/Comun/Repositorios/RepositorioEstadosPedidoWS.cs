﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios
{
    public class RepositorioEstadosPedidoWS: IRepositorioEstadosPedido
    {
        private const string ObtenerListaEstadosPedidoURL = "?r=estatus/buscar";

		private readonly string _usuario;
		private readonly string _password;

        public RepositorioEstadosPedidoWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

        public async Task<List<EstadoPedido>> ObtenerListaEstadoPedido()
        {
            var obj = new
            {
                datos = new
                {
                    colores = new
                    {
                         
                    },

                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaEstadosPedidoURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var estados = respuesta.Datos["datos"].ToObject<List<EstadoPedido>>();

            var todos = new EstadoPedido();
            todos.Id = 0;
            todos.Descripcion = "Todos";

            estados.Add(todos);

            estados = estados.OrderBy(x => x.Id).ToList();

            return estados;
        }
    }
}
