﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;

namespace Comun.Repositorios
{
    public interface IRepositorioSucursales
    {
        
        Task<Sucursal> ObtenerDatosSucursal(int id);
        Task<List<Sucursal>> ObtenerListaSucursales();

        Task<string> Insertar(string nombre, string direccion, string telefono, string horario);

        Task<string> Actualizar(int id, string nombre, string direccion, string telefono, string horario);

        Task<string> Eliminar(int id);
    }
}
