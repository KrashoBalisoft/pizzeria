﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioEmpleado
    {
        Task<List<Empleado>> ObtenerLista();
        Task<string> Insertar(string nombre, int IdTipoEmpleado);
        Task<string> Actualizar(int id, string nombre, int IdTipoEmpleado);
        Task<string> Eliminar(int id);
    }
}
