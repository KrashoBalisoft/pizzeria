﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public class RepositorioTipoMovimientoWS : IRepositorioTipoMovimiento
    {
        private const string ObtenerListaURL = "?r=tipoMovimientos/buscar"; 

        private readonly string _usuario;
        private readonly string _password;

        public RepositorioTipoMovimientoWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

        public async Task<List<TipoMovimiento>> ObtenerListaTipoMovimiento()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<TipoMovimiento>(obj, ObtenerListaURL);
            return lista;
        }
         

    }
}
