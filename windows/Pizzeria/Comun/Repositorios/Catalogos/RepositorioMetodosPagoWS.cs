﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;

namespace Comun.Repositorios.Catalogos
{
    public class RepositorioMetodosPagoWS: IRepositorioMetodosPago
    {
        private const string ObtenerListaURL = "?r=metodosPago/buscar";
        private const string InsertarURL = "?r=metodosPago/insertar";
        private const string EliminarURL = "?r=metodosPago/eliminar";
        private const string ActualizarURL = "?r=metodosPago/actualizar";

        private readonly string _usuario;
        private readonly string _password;

        public RepositorioMetodosPagoWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}

        public async Task<List<MetodoPago>> ObtenerLista()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ObtenerListaURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var lista = respuesta.Datos["datos"].ToObject<List<MetodoPago>>();

            return lista;
        }

        public async Task<bool> Insertar(string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodoPago = new { descripcion }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(InsertarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }
        public async Task<bool> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodoPago = new { id }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(EliminarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> Actualizar(int id, string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodoPago = new { id, descripcion }
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(ActualizarURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return false;
            }

            return true;
        }

        


    }
}
