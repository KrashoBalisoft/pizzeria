﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioTipoEmpleados
    {
        Task<List<TipoEmpleado>> ObtenerLista();
        Task<string> Insertar(string nombre);
        Task<string> Actualizar(int id, string nombre);
        Task<string> Eliminar(int id);
    }
}
