﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioTipoMovimiento
    {
        Task<List<TipoMovimiento>> ObtenerListaTipoMovimiento(); 
    }
}
