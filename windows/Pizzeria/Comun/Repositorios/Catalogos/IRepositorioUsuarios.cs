﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioUsuarios
    {
        Task<List<Usuario>> ObtenerLista();
        Task<string> Insertar(string username, string pass, int id_perfil, string estatus, int id_empleado);
        Task<string> Actualizar(int id, string username, string pass, int id_perfil, string estatus, int id_empleado);
        Task<string> Eliminar(int id);
        Task<Usuario> ValidarUsuario();
    }
}
