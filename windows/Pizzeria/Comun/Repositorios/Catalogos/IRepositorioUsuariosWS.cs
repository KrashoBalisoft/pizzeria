﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Newtonsoft.Json;

namespace Comun.Repositorios.Catalogos
{
    public class IRepositorioUsuarioWS: IRepositorioUsuarios
    {
        private const string ObtenerListaURL = "?r=usuario/buscar";
 
       /* private const string ObtenerPorIdURL = "?r=usuarios/buscarPorId";
        private const string InsertarTipoEntregaURL = "?r=usuarios/insertar";
        private const string EliminartipoEntregaURL = "?r=usuarios/eliminar";
        private const string ActualizartipoEntregaURL = "?r=usuarios/actualizar";*/
 
        private const string ObtenerPorIdURL = "?r=usuario/buscarPorId";
        private const string InsertarURL = "?r=usuario/insertar";
        private const string EliminarURL = "?r=usuario/eliminar";
        private const string ActualizarURL = "?r=usuario/actualizar";
 
        private const string LoginURL = "?r=usuario/login";

		private readonly string _usuario;
		private readonly string _password;

        public IRepositorioUsuarioWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}
        
        public async Task<List<Usuario>> ObtenerLista()
        {
            var obj = new
            {
                datos = new
                { 
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<Usuario>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<Usuario> ObtenerPorId(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password, 
                    id

                }
            };


            var registro = await MetodosWS.ObtenerRegistroPorId<Usuario>(obj, ObtenerListaURL);
            return registro;
        }

        public async Task<string> Insertar(string username, string pass, int id_perfil, string estatus, int id_empleado)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    usuarios = new { 
                        username,
                        pass,
                        id_perfil,
                        estatus,
                        id_empleado
                    }
                }
            };

            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    usuarios = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string username, string pass, int id_perfil, string estatus, int id_empleado)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    usuarios = new
                    {
                        id,
                        username,
                        pass,
                        id_perfil,
                        estatus,
                        id_empleado
                    }

                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<Usuario> ValidarUsuario()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password 
                }
            };

            var json = JsonConvert.SerializeObject(obj);
            var cadenaJson = await MetodosWS.ObtenerCadenaJson(LoginURL, json);

            var respuesta = JsonConvert.DeserializeObject<RespuestaJson>(cadenaJson);

            if (string.IsNullOrEmpty(respuesta.Error) || int.Parse(respuesta.Error) > 0)
            {
                return null;
            }

            if (respuesta.Datos["datos"].First == null)
            {
                return null;
            }

            var usuario = respuesta.Datos["datos"].First.ToObject<Usuario>(); 

            return usuario;
        }
    }
}
