﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public class IRepositorioEmpleadoWS : IRepositorioEmpleado
    {
        private const string ObtenerListaURL = "?r=empleados/buscar";
        private const string InsertarURL = "?r=empleados/insertar";
        private const string EliminarURL = "?r=empleados/eliminar";
        private const string ActualizarURL = "?r=empleados/actualizar";

        private readonly string _usuario;
        private readonly string _password;

        public IRepositorioEmpleadoWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}
        
        public async Task<List<Empleado>> ObtenerLista()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<Empleado>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<string>Insertar(string nombre, int id_tipo_empleado)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    empleado = new 
                    {
                        nombre,
                        id_tipo_empleado,
                    }
                }
            };


            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string nombre, int id_tipo_empleado)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    empleado = new 
                    { 
                        id, 
                        nombre,
                        id_tipo_empleado
                    }
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    empleado = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }


    }
}
