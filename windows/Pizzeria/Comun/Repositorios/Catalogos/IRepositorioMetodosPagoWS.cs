﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public class IRepositorioMetodosPagoWS : IRepositorioMetodosPago
    {
        private const string ObtenerListaURL = "?r=metodosPago/buscar";
        private const string InsertarURL = "?r=metodosPago/insertar";
        private const string EliminarURL = "?r=metodosPago/eliminar";
        private const string ActualizarURL = "?r=metodosPago/actualizar";

        private readonly string _usuario;
        private readonly string _password;

        public IRepositorioMetodosPagoWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}
        
        public async Task<List<MetodoPago>> ObtenerLista()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<MetodoPago>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<string>Insertar(string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodosPago = new { descripcion }
                }
            };


            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodosPago = new { id, descripcion}
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    metodosPago = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }


    }
}
