﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioTipoEntrega
    {
        Task<List<TipoEntrega>> ObtenerListaTipoEntrega();
        Task<bool> Insertar(string descripcion);
        Task<bool> Actualizar(int id, string descripcion);
        Task<bool> Eliminar(int id);
    }
}
