﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public interface IRepositorioMetodosPago
    {
         Task<List<MetodoPago>> ObtenerLista();
         Task<string> Insertar(string descripcion);
         Task<string> Actualizar(int id, string descripcion);
         Task<string> Eliminar(int id);
    }
}
