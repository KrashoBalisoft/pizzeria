﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comun.Entidades;
using Newtonsoft.Json;
using Comun.Entidades.Catalogos;

namespace Comun.Repositorios.Catalogos
{
    public class IRepositorioTipoEmpleadosWS : IRepositorioTipoEmpleados
    {
        private const string ObtenerListaURL = "?r=tipoEmpleados/buscar";
        private const string InsertarURL = "?r=tipoEmpleados/insertar";
        private const string EliminarURL = "?r=tipoEmpleados/eliminar";
        private const string ActualizarURL = "?r=tipoEmpleados/actualizar";

        private readonly string _usuario;
        private readonly string _password;

        public IRepositorioTipoEmpleadosWS(string usuario, string password)
		{
			_usuario = usuario;
			_password = password;
		}
        
        public async Task<List<TipoEmpleado>> ObtenerLista()
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password
                }
            };

            var lista = await MetodosWS.ObtenerLista<TipoEmpleado>(obj, ObtenerListaURL);
            return lista;
        }

        public async Task<string>Insertar(string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    tipoEmpleado = new { descripcion }
                }
            };


            var respuesta = await MetodosWS.Insertar(obj, InsertarURL);
            return respuesta;
        }

        public async Task<string> Actualizar(int id, string descripcion)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    tipoEmpleado = new 
                    { 
                        id, 
                        descripcion
                    }
                }
            };

            var respuesta = await MetodosWS.Actualizar(obj, ActualizarURL);
            return respuesta;
        }

        public async Task<string> Eliminar(int id)
        {
            var obj = new
            {
                datos = new
                {
                    usuario = _usuario,
                    password = _password,

                    tipoEmpleado = new { id }
                }
            };

            var respuesta = await MetodosWS.Eliminar(obj, id, EliminarURL);
            return respuesta;
        }


    }
}
