﻿using System.Linq;
using Comun;
using Comun.Servicios;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Pedidos
{
	/// <summary>
	/// Interaction logic for MainWindow.xam
	/// </summary>
	public partial class VentanaPrincipal : Window
	{
		#region Variables

		private ServiciosPedidos _serviciosPedidos;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();


		#endregion

		#region Constructor

		public VentanaPrincipal()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

			_serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);

            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 1, 0);
            dispatcherTimer.Start();
		}

		#endregion

		#region Eventos

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
				await CargarPedidos();
			}
			catch (Exception)
			{
				MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}

		private void CapturarPedidoClick(object sender, RoutedEventArgs e)
		{
			var ventanaCapturarPedido = new VentanaCapturarPedido()
			{
				Owner = this
			};

			ventanaCapturarPedido.Show();
		}

		private void SistemaSalirClick(object sender, RadRoutedEventArgs e)
		{
			Close();
		}

        private async void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            await CargarPedidos();
        } 

		#endregion

		#region Métodos

		public async Task CargarPedidos()
		{
			var pedidos = await _serviciosPedidos.ObtenerTodos();

		    if (pedidos != null)
		    {
                pedidos = pedidos.Where(x => x.IdOrigen == ConstantesSistemas.IdOrigenCallCenter).ToList();

                GrdPedidos.ItemsSource = pedidos;
		    }  
		}

		#endregion
	}
}
