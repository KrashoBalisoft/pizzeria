﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows; 
using System.Xaml;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Pedidos;
using MessageBox = System.Windows.MessageBox;

namespace Pedidos
{
    /// <summary>
    /// Interaction logic for FrmVentaDetalle.xaml
    /// </summary>
    public partial class FrmVentaDetalle : Window
    {
        private ServiciosProductos _serviciosProductos;
        private List<Producto> _listaProductos;
        public Producto ProductoEnVenta;
        private VentanaCapturarPedido ventanaCapturarPedido;
        public FrmVentaDetalle()
        {
            _serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void FrmProductoDetalle_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Obtenemos una copia de la ventana madre  
                if (this.Owner is VentanaCapturarPedido)
                {
                    ventanaCapturarPedido = (VentanaCapturarPedido)this.Owner;
                } 
                
                //Pinta el nombre del producto
                NombreProducto.Content = ProductoEnVenta.Nombre;

                //Solamente estarán activos para cuando sea pizza Mitad y Mitad
                PrimeraMitad.IsEnabled = false;
                SegundaMitad.IsEnabled = false;
                //solamente estará activo cuando sea personalizada ...
                TabIngredientes.IsEnabled = false;
                TabIngredientesExtras.IsSelected = true;

                if (ProductoEnVenta.Tamanios != null)
                {
                    Tamanios.ItemsSource = ProductoEnVenta.Tamanios;
                    Tamanios.DisplayMemberPath = "Tamanio";
                    Tamanios.SelectedValuePath = "Id";
                }

                if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas)
                {
                    if (ProductoEnVenta.CantidadIngredientes == 0) //Para las pizzas Mitad y Mitad
                    {
                        _listaProductos = await _serviciosProductos.ObtenerTodos();
                         
                        _listaProductos = _listaProductos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEspecialidades).OrderBy(x => x.Nombre).ToList();

                        PrimeraMitad.DisplayMemberPath = "Nombre";
                        PrimeraMitad.SelectedValuePath = "Id";
                        PrimeraMitad.ItemsSource = _listaProductos;
                        PrimeraMitad.IsEnabled = true;

                        SegundaMitad.DisplayMemberPath = "Nombre";
                        SegundaMitad.SelectedValuePath = "Id";
                        SegundaMitad.ItemsSource = _listaProductos;
                        SegundaMitad.IsEnabled = true;

						CenterWindowOnScreen();
                    }
                    else //Para las pizzas personalizadas según el número de ingredientes
                    {
                        TabIngredientes.IsEnabled = true;
                        TabIngredientes.IsSelected = true;
                    } 
                }

                var listaIngredientes = await _serviciosProductos.ObtenerIngredientes();

                //Creamos el directorio de las imágenes
                int index = 0;
                while (index < listaIngredientes.Count)
                {
                    listaIngredientes[index].NombreImagen = String.Format("{0}{1}", ConstantesSistemas.DirectorioImagenesCallCenter, listaIngredientes[index].NombreImagen);
                    index++;
                }

                listaIngredientes = listaIngredientes.OrderBy(x => x.Nombre).ToList();

                ListaIngredientes.ItemsSource = listaIngredientes;
                ListaIngredientesExtra.ItemsSource = listaIngredientes;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }  
        }

		private void CenterWindowOnScreen()
		{
			double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
			double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
			double windowWidth = this.Width;
			double windowHeight = this.Height;
			this.Left = (screenWidth / 2) - (windowWidth / 2);
			this.Top = (screenHeight / 2) - (windowHeight / 2);
		}

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (ValidarFormulario())
            {
                AgregarProducto();

                Close();
            }
        }

        private bool ValidarFormulario()
        {
            //Validamos que el tamaño esté seleccionado
            if (Tamanios.SelectedItem == null)
            {
                MessageBox.Show("Seleccione el tamaño de la pizza", "Error", MessageBoxButton.OK);

                return false;
            }
            
            //Validamos las personalizadas, según número de ingredientes
            if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas && ProductoEnVenta.CantidadIngredientes > 0)
            {
                if (ListaIngredientes.SelectedItems.Count == 0 ||  ListaIngredientes.SelectedItems.Count > ProductoEnVenta.CantidadIngredientes)
                {
                    MessageBox.Show(string.Format("Para este producto, solamente puede seleccionar {0}", ProductoEnVenta.CantidadIngredientes), "Error", MessageBoxButton.OK);

                    return false;
                }

            }

            //Validamos las personalizadas, mitad y mitad
            if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas && ProductoEnVenta.CantidadIngredientes == 0)
            {
                if (PrimeraMitad.SelectedItem == null || SegundaMitad.SelectedItem == null)
                {
                    MessageBox.Show("Para este producto, seleccione ambas mitades", "Error", MessageBoxButton.OK);

                    return false;
                } 
            }

            return true;
        }

        private void AgregarProducto()
        {
            try
            {
                for (double i = 0; i < NumeroPizzas.Value; i++)
                {

                    var primeraMitad = new Producto();
                    var segundaMitad = new Producto();

                    var productoVendido = new PedidoProducto
                    {
                        IdProducto = ProductoEnVenta.Id,
                        Producto = ProductoEnVenta.Nombre,
                        Observaciones = Observaciones.Text,
                        Cantidad = 1,
                        Ingredientes = new List<Ingrediente>()
                    };

                    //localizamos el precio de venta en el arreglo de tamaños
                    if (ProductoEnVenta.Tamanios.Count > 0)
                    {
                        var itemTamanio =
                            (ProductoTamanio)
                                ProductoEnVenta.Tamanios.Find(x => x.Id == Convert.ToInt32(Tamanios.SelectedValue));

                        productoVendido.Precio = itemTamanio.PrecioVenta;
                        productoVendido.Tamanio = itemTamanio.Tamanio;
                        productoVendido.IdTamanio = itemTamanio.IdTamanio;
                    }
                    else //O lo obtenemos directamente del 
                    {
                        productoVendido.Precio = ProductoEnVenta.PrecioVenta;
                        productoVendido.Tamanio = String.Empty;
                    }

                    //Detectamos si es una personalizada mitad y mitad, 
                    //cada mitad será almacenada como un ingrediente

                    if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas &&
                        ProductoEnVenta.CantidadIngredientes == 0)
                    {
                        primeraMitad = (Producto) PrimeraMitad.SelectedItem;
                        segundaMitad = (Producto) SegundaMitad.SelectedItem;

                        if (primeraMitad != null)
                        {
                            var ingredienteExtra = new Ingrediente
                            {
                                EsExtra = false,
                                IdProducto = primeraMitad.Id,
                                PrecioVenta = 0,
                                Descripcion = primeraMitad.Nombre
                            };

                            productoVendido.Ingredientes.Add(ingredienteExtra);

                            productoVendido.IdMitadUno = primeraMitad.Id;
                        }

                        if (segundaMitad != null)
                        {
                            var ingredienteExtra = new Ingrediente
                            {
                                EsExtra = false,
                                IdProducto = segundaMitad.Id,
                                PrecioVenta = 0,
                                Descripcion = segundaMitad.Nombre
                            };

                            productoVendido.Ingredientes.Add(ingredienteExtra);

                            productoVendido.IdMitadDos = segundaMitad.Id;
                        }
                    }

                    //Obtenemos la lista de los ingredientes en el caso de las pizzas personalizadas
                    if (ListaIngredientes.SelectedItems.Count > 0)
                    {
                        foreach (Producto extra in ListaIngredientes.SelectedItems)
                        {
                            var ingredienteExtra = new Ingrediente
                            {
                                EsExtra = false,
                                IdProducto = extra.Id,
                                PrecioVenta = 0,
                                Descripcion = extra.Nombre
                            };

                            productoVendido.Ingredientes.Add(ingredienteExtra);
                        }
                    }

                    //Verificamos que tenga ingredientes extra
                    if (ListaIngredientesExtra.SelectedItems.Count > 0)
                    {
                        foreach (Producto extra in ListaIngredientesExtra.SelectedItems)
                        {
                            var ingredienteExtra = new Ingrediente
                            {
                                EsExtra = true,
                                IdProducto = extra.Id,
                                PrecioVenta = extra.PrecioVenta ?? 0,
                                Descripcion = extra.Nombre

                            };

                            productoVendido.Ingredientes.Add(ingredienteExtra);
                        }
                    }

                    //Calculamos el total de la venta del producto + ingredientes adicionales si los tiene

                    productoVendido.TotalIngredientesExtras = 0;

                    if (productoVendido.Ingredientes.Count > 0)
                    {
                        productoVendido.TotalIngredientesExtras = productoVendido.Ingredientes.Sum(x => x.PrecioVenta);
                    }

                    //Validamos las personalizadas, mitad y mitad
                    if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas &&
                        ProductoEnVenta.CantidadIngredientes == 0)
                    {
                        decimal precioVenta = 0;
                        var TamanioSeleccionado = (ProductoTamanio) Tamanios.SelectedItem;
                        var itemTamanio1 =
                            primeraMitad.Tamanios.Find(x => x.IdTamanio == TamanioSeleccionado.IdTamanio);

                        var itemTamanio2 =
                           segundaMitad.Tamanios.Find(x => x.IdTamanio == TamanioSeleccionado.IdTamanio);

                        if (itemTamanio1.PrecioVenta >= itemTamanio2.PrecioVenta)
                        {
                            precioVenta = (decimal)itemTamanio1.PrecioVenta;
                        }
                        else
                        {
                            precioVenta = (decimal)itemTamanio2.PrecioVenta;
                        }

                        productoVendido.Total =  (precioVenta + productoVendido.Precio + productoVendido.TotalIngredientesExtras);
                    }
                    else
                    {
                        productoVendido.Total = (productoVendido.Precio + productoVendido.TotalIngredientesExtras);
                    }

                    if (this.Owner is VentanaCapturarPedido)
                    {
                        //Pintamos los datos en la ventana principal
                        ventanaCapturarPedido._productosEnPedido.Add(productoVendido);

                        ventanaCapturarPedido.GridPedido.ItemsSource = ventanaCapturarPedido._productosEnPedido;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                
                throw;
            }
        }
    }
}
