﻿using System; 
using System.Windows; 
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Documents.Utils;
using MessageBox = System.Windows.MessageBox;

namespace Pedidos
{
    /// <summary>
    /// Interaction logic for VentanaCancelacion.xaml
    /// </summary>
    public partial class VentanaLogin
    { 
        private ServiciosUsuario _serviciosUsuario; 
        public VentanaLogin()
        {   
            InitializeComponent();

			this.Loaded += VentanaLogin_Loaded;
        }

		void VentanaLogin_Loaded(object sender, RoutedEventArgs e)
		{
			txtUsuario.Focus();
		}

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (!ValidarFormulario()) return;

            try
            {
                _serviciosUsuario = new ServiciosUsuario(txtUsuario.Text, Crypto.MD5Hash(txtPassword.Password));

                var datosUsuario = await _serviciosUsuario.ValidarUsuario();

                if (datosUsuario == null)
                { 
                    MessageBox.Show("¡Usuario o contraseña incorrectos, inténtelo nuevamente!", "Atención",
                        MessageBoxButton.OK); 
                }
                else
                {
                    ConstantesWS.Usuario = datosUsuario.Username;
                    ConstantesWS.Password = datosUsuario.Password;
                    ConstantesSistemas.IdUsuario = datosUsuario.Id;

                    SplashScreen splashScreen = new SplashScreen("/Imagenes/SplashScreen.png");
                    splashScreen.Show(true);

                    Application.Current.MainWindow = new VentanaPrincipal();
                    Application.Current.MainWindow.Show(); 

                    Close(); 
                }
            }
            catch (Exception expt)
            { 
                MessageBox.Show("¡Error al conectarse con el servidor!", "¡Atención: error de conexión!", MessageBoxButton.OK,
                    MessageBoxImage.Error); 
            }
        }

        private void btnCancelar_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private bool ValidarFormulario()
        {
            if (String.IsNullOrEmpty(txtUsuario.Text))
            {
                MessageBox.Show("Introduzca el nombre del usuario", "Atención", MessageBoxButton.OK);
                txtUsuario.Focus();

                return false;
            }

            if (String.IsNullOrEmpty(txtPassword.Password))
            {
                MessageBox.Show("Introduzca la constraseña", "Atención", MessageBoxButton.OK);
                txtPassword.Focus();

                return false;
            }

            return true;
        }
    }
}
