﻿using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;

namespace Pedidos
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaDireccion : Window
    {
        public int Id; 

        private VentanaCapturarPedido ventanaPedidos;

        public Cliente cliente;

        public VentanaDireccion()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent(); 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ventanaPedidos = (VentanaCapturarPedido) this.Owner;
            
            //Si el objeto es para actualización
            if (cliente != null)
            {
                
                //Codigo.SelectedColor = (Color)ColorConverter.ConvertFromString(color.Codigo);
            }

        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {    
            Guardar();
        }

        private void Guardar()
        {
            try
            {
                var clienteDirecciones = new ClienteDirecciones
                {
                    Calle = Calle.Text,
                    Colonia = Colonia.Text,
                    NumeroExterior = Numero.Text,
                    Referencia = Referencia.Text, 
                    DomicilioConcatenado = string.Format("Calle {0} No.  {1} Colonia: {2}", Calle.Text, Numero.Text, Colonia.Text), 
                    IdCliente = ventanaPedidos._cliente.Id
                };

                //Agregamos la dirección en la siguiente posición del arreglo
                ventanaPedidos._cliente.Direcciones.Add(clienteDirecciones);

                ventanaPedidos.TxtDomicilio.DisplayMemberPath = "DomicilioConcatenado";
                ventanaPedidos.TxtDomicilio.SelectedValuePath = "Id";
                ventanaPedidos.TxtDomicilio.ItemsSource = ventanaPedidos._cliente.Direcciones.ToList();
		    

                Close(); 
            }
            catch (Exception x)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde." + x.ToString(), "¡Atención!", MessageBoxButton.OK);

                //throw;
            }             
        }
    }
}
