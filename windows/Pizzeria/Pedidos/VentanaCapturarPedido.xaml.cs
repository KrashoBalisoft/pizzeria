﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
﻿using System.Threading.Tasks;
using System.Windows;
﻿using System.Windows.Controls.Primitives;
﻿using System.Windows.Input;
﻿using System.Windows.Media.Imaging;
﻿using Comun;
using Comun.Entidades;
using Comun.Servicios; 
﻿using Telerik.Windows;
﻿using Telerik.Windows.Controls.Map;

namespace Pedidos
{
	/// <summary>
	/// Interaction logic for VentanaCapturarPedido.xaml
	/// </summary>
	public partial class VentanaCapturarPedido : Window
	{
		private ServiciosClientes _serviciosClientes;
	    private ServiciosSucursales _serviciosSucursales;
	    private ServiciosPedidos _serviciosPedidos;
		public Cliente _cliente;

		private ServiciosProductos _serviciosProductos;
		private List<Producto> _productos;

	    public ObservableCollection<PedidoProducto> _productosEnPedido;
        private Producto productoSeleccionado = new Producto();
         
		private BingSearchProvider _proveedorBusquedas;

	    private VentanaPrincipal frmVentanaPrincipal;

	    public VentanaCapturarPedido()
		{
			InitializeComponent();

			_serviciosClientes = new ServiciosClientes(ConstantesWS.Usuario, ConstantesWS.Password);
			_serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);

            _productosEnPedido = new ObservableCollection<PedidoProducto>();  
			
            GridPedido.ItemsSource = _productosEnPedido;

			TxtNumeroTelefono.Focus(); 

			mapUbicacion.Provider = new BingMapProvider(MapMode.Aerial, true, ConstantesMapas.BingClaveAplicacion);

			_proveedorBusquedas = new BingSearchProvider(ConstantesMapas.BingClaveAplicacion);
			_proveedorBusquedas.MapControl = mapUbicacion;
			_proveedorBusquedas.SearchCompleted += ProveedorBusquedasSearchCompleted;
		}

		#region Eventos
		private async void BtnBuscarClick(object sender, RoutedEventArgs e)
		{
		    if (String.IsNullOrEmpty(TxtNumeroTelefono.Text))
		        return;

			IndicadorOcupado.IsBusy = true;
			try
			{
				await BuscarClientePorTelefono();
			}
			catch (Exception ex)
			{
				MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico + ex);
			    throw;
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}

		private async void BtnGuardarClienteClick(object sender, RadRoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
				await GuardarCliente();
			}
			catch (Exception)
			{
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}

		private void TxtNumeroTelefonoKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				BtnBuscar.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
			}
		}

		private async void VentanaCapturaPedidoLoaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
			    frmVentanaPrincipal = (VentanaPrincipal) this.Owner;

                var listaSucursales = await _serviciosSucursales.ObtenerListaSucursales();

                TxtSucursal.DisplayMemberPath = "Nombre";
			    TxtSucursal.SelectedValuePath = "Id";
                TxtSucursal.ItemsSource = listaSucursales;

				await CargarProductos();
			}
			catch (Exception)
			{
				MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			    throw;
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}

		}  

        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if (productoSeleccionado.Id == 0)
                return;

            switch (productoSeleccionado.IdClasificacion)
            {
                case ConstantesSistemas.IdBebidas:
                case ConstantesSistemas.IdEntremeses:
                case ConstantesSistemas.IdPastas:
                    AgregarProducto(productoSeleccionado);
                    break;
                default:
                    MostrarVentanaDetalle(productoSeleccionado);
                    break;
            } 
        }

		private void BtnCancelarClick(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void BtnEliminarProductoPedido(object sender, RadRoutedEventArgs e)
		{
			var item = (PedidoProducto) GridPedido.SelectedItem;
			if (item != null)
			{
				_productosEnPedido.Remove(item);
			}
		}

		private  void BtnAceptarClick(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
                //Validamos que haya datos de productos dados de alta en el pedido
			    if (!_productosEnPedido.Any())
			    {
			        MessageBox.Show("No hay productos agregados al pedido.", "Pedido", MessageBoxButton.OK,
			            MessageBoxImage.Exclamation);
			        return;
			    }

			    if (_cliente == null)
			    {
                    MessageBox.Show("No hay un cliente seleccionado.", "Pedido", MessageBoxButton.OK,
                       MessageBoxImage.Exclamation);
                    return;
			    }

			    if (ChkEnviarOtraDireccion.IsChecked == true && String.IsNullOrEmpty(TxtOtraDireccion.Text))
			    {
                    MessageBox.Show("Escriba la dirección de envío ya que ha indicado que el envío será a una dirección diferente.", "Pedido", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

			        TxtOtraDireccion.Focus();

                    return;
			    }

			    if (TxtSucursal.SelectedItem == null)
			    {
                    MessageBox.Show("Seleccione la sucursal antes de guardar el pedido.", "Pedido", MessageBoxButton.OK, MessageBoxImage.Exclamation);

			        TxtSucursal.Focus();

                    return;
			    }

                if (TxtDomicilio.SelectedItem == null)
                {
                    MessageBox.Show("Seleccione o capture un domicilio antes de guardar el pedido.", "Pedido", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    TxtSucursal.Focus();

                    return;
                }

			    AgregarPedido();
			}
			catch (Exception)
			{
				MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}
         

        private void ListaEspecialidades_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListaEntremeses.SelectedIndex = -1;
            ListaPersonalizadas.SelectedIndex = -1;
            ListaBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListaEspecialidades.SelectedItem;
        }

        private void ListaEntremeses_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListaEspecialidades.SelectedIndex = -1;
            ListaPersonalizadas.SelectedIndex = -1;
            ListaBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListaEntremeses.SelectedItem;
        }

        private void ListaPersonalizadas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListaEspecialidades.SelectedIndex = -1;
            ListaEntremeses.SelectedIndex = -1;
            ListaBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListaPersonalizadas.SelectedItem;
        }

        private void ListaBebidas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListaEspecialidades.SelectedIndex = -1;
            ListaPersonalizadas.SelectedIndex = -1;
            ListaEntremeses.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListaBebidas.SelectedItem;
        }

		void ProveedorBusquedasSearchCompleted(object sender, SearchCompletedEventArgs e)
		{
			this.informationLayer.Items.Clear();
			SearchResultCollection results = e.Response.ResultSets.First().Results;
			SearchRegion region = e.Response.ResultSets.First().SearchRegion;
			if (results.Count > 0)
			{
				foreach (SearchResultBase result in results)
				{
					MapItem item = new MapItem();
					item.Caption = result.Name;
					item.Location = result.LocationData.Locations.First();
					item.BaseZoomLevel = 9;
					item.ZoomRange = new ZoomRange(5, 12);
					this.informationLayer.Items.Add(item);
				}
			}
			else
			{
				if (region != null)
				{
					this.mapUbicacion.SetView(region.GeocodeLocation.BestView);
					if (region.GeocodeLocation.Address != null && region.GeocodeLocation.Locations.Count > 0)
					{
						foreach (Location location in region.GeocodeLocation.Locations)
						{
							MapItem item = new MapItem();
							item.Caption = region.GeocodeLocation.Address.FormattedAddress;
							item.Location = location;
							this.informationLayer.Items.Add(item);
						}
					}
				}
			}

		}

		#endregion

		#region Métodos
		private async Task CargarProductos()
		{
		    _productos = await _serviciosProductos.ObtenerTodos();

		    if (_productos.Any())
		    {
		        int index = 0;
                while (index < _productos.Count)
		        {
                    _productos[index].NombreImagen = String.Format("{0}{1}", ConstantesSistemas.DirectorioImagenesCallCenter, _productos[index].NombreImagen);
                    index++;
		        }

		        ListaEspecialidades.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEspecialidades).OrderBy(x => x.Nombre);
		        ListaEntremeses.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEntremeses || x.IdClasificacion == ConstantesSistemas.IdPastas).OrderBy(x => x.Nombre);
		        ListaPersonalizadas.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdPersonalizadas).OrderBy(x => x.Nombre);
		        ListaBebidas.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdBebidas).OrderBy(x => x.Nombre);
		    }
		}

		private async Task BuscarClientePorTelefono()
		{
			_cliente = await _serviciosClientes.BuscarClientePorTelefono(TxtNumeroTelefono.Text);
			if (_cliente == null)
			{
				MessageBox.Show(
					"No se encontró ningún cliente con ese número, verifique el número o introduzca los datos para dar de alta un nuevo cliente.",
					"Número no encontrado", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				_cliente = new Cliente { Telefono = TxtNumeroTelefono.Text, IdTipoTelefono = 1, Direcciones = new List<ClienteDirecciones>() }; 
			}

		    if (_cliente.Direcciones != null)
		    {

                TxtDomicilio.DisplayMemberPath = "DomicilioConcatenado";
                TxtDomicilio.SelectedValuePath = "Id";
                TxtDomicilio.ItemsSource = _cliente.Direcciones.ToList();
		    }
		    else
		    {
		        
		        _cliente.Direcciones = new List<ClienteDirecciones>();
		    }

		    FrmCliente.DataContext = _cliente; 

		    //  SearchRequest busqueda = new SearchRequest();
		    //	busqueda.Query = string.Format("{0}, {1}, {2}", _cliente.Domicilio, _cliente.Colonia, ConstantesSistemas.CiudadSucursal );
		    //_proveedorBusquedas.SearchAsync(busqueda);
		}

		private async Task GuardarCliente()
		{
		    try
		    {

		        if (String.IsNullOrEmpty(TxtNombre.Text) || String.IsNullOrEmpty(TxtCorreo.Text))

		        {
                    MessageBox.Show("Todos los datos del formulario son obligatorios", "Nuevo cliente", MessageBoxButton.OK,
                          MessageBoxImage.Information);

                    return;
		        }

		        if (TxtSucursal.SelectedItem == null)
		        {
                     MessageBox.Show("Selecciones la sucursal más cercaba al Cliente", "Nuevo cliente", MessageBoxButton.OK,
                            MessageBoxImage.Information);

		            return;
		        }

		        if (_cliente.Id == 0)
		        {
		            _cliente.IdSucursal = Convert.ToInt32(TxtSucursal.SelectedValue);
                    _cliente = await _serviciosClientes.AgregarCliente(_cliente);
                    if (_cliente != null)
                    {
                        MessageBox.Show("El cliente fue dado de alta correctamente", "Nuevo cliente", MessageBoxButton.OK,
                            MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Ocurrió un error al dar de alta al cliente", "Nuevo cliente", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                }
                else
		        {
		            int i = 0;
                    foreach (var direccion in _cliente.Direcciones)
                    { 
                        _cliente.Direcciones[i].IdCliente = _cliente.Id;
                        i++;
                    }

                    bool resultado = await _serviciosClientes.ModificarCliente(_cliente);
                    if (resultado == true)
                    {
                        MessageBox.Show("El cliente fue modificado correctamente", "Modificar cliente", MessageBoxButton.OK,
                            MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Ocurrió un error al modificar los datos del cliente", "Modificar cliente", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                }
		    }
		    catch (Exception x)
		    {
                MessageBox.Show("Ocurrió un error al agregar/modificar los datos del cliente" + x, "Nuevo cliente", MessageBoxButton.OK,
                           MessageBoxImage.Error);
		        //throw;
		    }
		}

        private void AgregarProducto(Producto producto)
        {
            var productoVendido = new PedidoProducto();

            productoVendido.IdProducto = producto.Id;
            productoVendido.Producto = producto.Nombre;
            productoVendido.Precio = producto.PrecioVenta;
            productoVendido.Total = producto.PrecioVenta;
            productoVendido.Observaciones = string.Empty;
            productoVendido.Cantidad = 1;

            _productosEnPedido.Add(productoVendido);

            GridPedido.ItemsSource = _productosEnPedido;
        }

        private void MostrarVentanaDetalle(Producto item)
        {
            var frmVentanaDetalle = new FrmVentaDetalle
            {
                Owner = this,
                ProductoEnVenta = item
            };

            frmVentanaDetalle.ShowDialog();
        }

        public async void AgregarPedido()
        {
            try 
            {
                if (_cliente.Id == 0)
                {
                    MessageBox.Show("¡Guarde los datos del cliente antes de guardar el pedido!", "¡Éxito!",
                       MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    return;
                } 

                var pedido = new Pedido
                {
                    IdCliente = _cliente.Id,
                    IdSucursal = Convert.ToInt32(TxtSucursal.SelectedValue),
                    IdOrigen = ConstantesSistemas.IdOrigenCallCenter,
                    IdTipoEntrega = 1,
                    IdMetodoPago = 1,
                    FechaPedido = DateTime.Now,
                    Hora = string.Format("{0}:{1}:{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                    EnviarPedido = false,
                    Domicilio = ChkEnviarOtraDireccion.IsChecked == true ? TxtOtraDireccion.Text : _cliente.Domicilio,
                    Descuento = 0,
                    Productos = new List<PedidoProducto>(_productosEnPedido.ToList()),
                    Subtotal = _productosEnPedido.Sum(x => x.Total),
                    IdDomicilio = (int) TxtDomicilio.SelectedValue, 
                    EnviarCocina = (bool) ChkEnviarCocina.IsChecked ? 1 : 0
                };

                BtnAgregar.IsEnabled = false;
                BtnAceptar1.IsEnabled = false;

                if (await _serviciosPedidos.AgregarPedido(pedido))
                {
                    MessageBox.Show("El pedido se ha guardado y enviado a la sucursal correctamente", "¡Éxito!",
                        MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    _productosEnPedido = new ObservableCollection<PedidoProducto>();

                    GridPedido.ItemsSource = _productosEnPedido;
                    
                    _cliente = null;

                    FrmCliente.DataContext = _cliente;

                    TxtDomicilio.ItemsSource = null;

                    BtnAgregar.IsEnabled = true;
                    BtnAceptar1.IsEnabled = true;

                    await frmVentanaPrincipal.CargarPedidos();

                    return;
                }

                BtnAceptar1.IsEnabled = true;

                MessageBox.Show("Hubo problemas al guardar el pedido", "¡Atención!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); 

                BtnAceptar1.IsEnabled = true;
                BtnAgregar.IsEnabled = true;
                //throw;
            }
        }

		#endregion

		 
		private class MapItem
		{
			public MapItem()
			{
			}
			public MapItem(string caption, Location location, double baseZoomLevel, ZoomRange zoomRange)
			{
				this.Caption = caption;
				this.Location = location;
				this.BaseZoomLevel = baseZoomLevel;
				this.ZoomRange = zoomRange;
			}
			public string Caption
			{
				get;
				set;
			}
			public Location Location
			{
				get;
				set;
			}
			public double BaseZoomLevel
			{
				get;
				set;
			}
			public ZoomRange ZoomRange
			{
				get;
				set;
			}
		}

        private void TabControlPedidos_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
        {

        }

	    private void BtnNuevaDireccion_Click(object sender, RoutedEventArgs e)
	    {   if (_cliente == null) return;
	        var frmFormaDirecciones = new VentanaDireccion
	        {
	            Owner = this
	        };

	        frmFormaDirecciones.ShowDialog(); 
	    } 
	}
}
