﻿﻿using System;
using System.Collections.Generic;
﻿using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Telerik.Windows;
using Telerik.Windows.Controls;
﻿using TicketingSystem;

namespace Cocina
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class VentanaPrincipal : Window
    {
        private readonly ServiciosPedidos _serviciosPedidos;
        private readonly ServiciosSucursales _serviciosSucursales;

        private IList<Pedido> _listaPedidosCocina;
        private IList<Pedido> _listaPedidosPreparacion;

        private Sucursal _datosSucursal;


        readonly System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public VentanaPrincipal()
        {
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
                 
        }

        private async void FrmListarPedidosCocina_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _datosSucursal = await _serviciosSucursales.ObtenerDatosSucursal(ConstantesSistemas.IdSucursal[0]);

                lblDatosSucursal.Content = _datosSucursal.Nombre; 

                CargarPedidos();

                dispatcherTimer.Tick += dispatcherTimer_Tick;
                dispatcherTimer.Interval = ConstantesSistemas.Tiempo;
                dispatcherTimer.Start();
            }
            catch (Exception)
            {

                MessageBox.Show("¡Hubo un problema con la conexión al servidor!", "Error de conexión",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            } 
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            CargarPedidos();
        } 

        private async void btnEnviarPedido_Click(object sender, RoutedEventArgs e)
        {
            if (listaEnviadasCocina.SelectedItem == null)
            {
                MessageBox.Show("¡Seleccione un pedido de la lista!", "Error", MessageBoxButton.OK,
                   MessageBoxImage.Warning);
                return;
            }
            
            int id = _listaPedidosCocina[listaEnviadasCocina.SelectedIndex].Id;

            if (!await _serviciosPedidos.EnviarPedidoAPreparacion(id))
            {
                MessageBox.Show("Hubo un problema al intentar enviar el pedido.", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);

                return;
            }

            CargarPedidos(); 
        }

        private async void btnTerminarPedido_Click(object sender, RoutedEventArgs e)
        {
            if (listaEnPreparacion.SelectedItem == null)
            {
                MessageBox.Show("¡Seleccione un pedido de la lista!", "Error", MessageBoxButton.OK,
                   MessageBoxImage.Warning);

                return;
            }

            int id = _listaPedidosPreparacion[listaEnPreparacion.SelectedIndex].Id;

            if (!await _serviciosPedidos.TerminarPedido(id))
            {
                MessageBox.Show("Hubo un problema al intentar enviar el pedido.", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);

                return;
            }

            ImprimirTicket(id);

            CargarPedidos();
        }

        private async void CargarPedidos()
        {
            try
            {
                //Buscamos la lista de los pedidos pendintes 
                listaEnviadasCocina.ItemsSource = null;
                listaEnPreparacion.ItemsSource = null;

                _listaPedidosCocina = await _serviciosPedidos.BuscarPedidosCocina(ConstantesSistemas.IdSucursal, 1);
                _listaPedidosPreparacion = await _serviciosPedidos.BuscarPedidosCocina(ConstantesSistemas.IdSucursal, 2);

                lblEnviadoCocina.Content = "NO HAY PEDIDOS...";
                lblListaPreparacion.Content = "NO HAY PEDIDOS...";

                if (_listaPedidosCocina != null)
                {
                    listaEnviadasCocina.ItemsSource = _listaPedidosCocina;
                    lblEnviadoCocina.Content = string.Format("PEDIDOS EN ESPERA {0}", _listaPedidosCocina.Count);
                }

                if (_listaPedidosPreparacion != null)
                {
                    listaEnPreparacion.ItemsSource = _listaPedidosPreparacion;
                    lblListaPreparacion.Content = string.Format("EN PREPARACIÓN {0}", _listaPedidosPreparacion.Count);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("¡Hubo un problema al establecer la conexión con el servidor!"); 
            } 
        }

        private async void ImprimirTicket(int IdPedido)
        {
            try
            {
                var _pedido = await _serviciosPedidos.BuscarPedido(IdPedido);

                Ticket tkt = new Ticket(); 

                tkt._pedido = _pedido;
                tkt._sucursal = _datosSucursal; 
                tkt.print(ConstantesSistemas.NumeroCopias); 
            }
            catch (Exception e)
            {
                MessageBox.Show("¡Hubo un problema con la impresora, conéctela!", "¡Atención!", MessageBoxButton.OK); 
            } 
        }

        private void SistemaSalirClick(object sender, RadRoutedEventArgs e)
        {
            Close();
        }
    }
}
