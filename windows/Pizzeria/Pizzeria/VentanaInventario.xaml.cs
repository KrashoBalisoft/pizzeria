﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using Telerik.Windows;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria
{
	/// <summary>
	/// Interaction logic for VentanaInventario.xaml
	/// </summary>
	public partial class VentanaInventario : Window
	{
        private ServiciosSucursales _servicioSucursales;
	    private ServiciosProductos _serviciosProductos;
	    private ServiciosInventarios _serviciosInventarios;
	    private ServiciosUnidadMedida _serviciosUnidadMedida;
	    private ServiciosTipoMovimiento _serviciosTipoMovimiento;

        private ObservableCollection<InventarioProducto> _productosEnEnvio;

		#region Constructor
		public VentanaInventario()
		{
            Thread.Sleep(2000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

			InitializeComponent();

            _servicioSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosInventarios = new ServiciosInventarios(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosUnidadMedida = new ServiciosUnidadMedida(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosTipoMovimiento = new ServiciosTipoMovimiento(ConstantesWS.Usuario, ConstantesWS.Password); 
		}
		#endregion

		#region Eventos
        private async void VentanaInventario_Loaded(object sender, RoutedEventArgs e)
        {
            _productosEnEnvio = new ObservableCollection<InventarioProducto>();

            ComboSucursalOrigen.ItemsSource = await _servicioSucursales.ObtenerListaSucursales();
            ComboSucursalDestino.ItemsSource = await _servicioSucursales.ObtenerListaSucursales();
            ComboProductos.ItemsSource = await _serviciosProductos.ObtenerIngredientes();
            ComboUnidadMedida.ItemsSource = await _serviciosUnidadMedida.ObtenerLista();
            ComboTipo.ItemsSource = await _serviciosTipoMovimiento.ObtenerListaTipoMovimiento();
            FiltroSucursales.ItemsSource = await _servicioSucursales.ObtenerListaSucursales();

            //agregamos el evento changed al filtro combo..
            FiltroSucursales.SelectionChanged += FiltroSucursales_SelectionChanged;

            Fecha.SelectionChanged += FiltroSucursales_SelectionChanged;

            CargarDatos();
        }
		private void BtnGuardarClick(object sender, RadRoutedEventArgs e)
		{
		  
            if (ComboSucursalOrigen.SelectedItem == null)
            {
                MessageBox.Show("Seleccione la sucursal de origen.", "¡Atención!", MessageBoxButton.OK,
                       MessageBoxImage.Exclamation);
                ComboSucursalOrigen.Focus();
                return;
            }

            if (ComboSucursalDestino.SelectedItem == null)
            {
                MessageBox.Show("Seleccione la sucursal de destino.", "¡Atención!", MessageBoxButton.OK,
                       MessageBoxImage.Exclamation);
                ComboSucursalDestino.Focus();
                return;
            }

            if (ComboTipo.SelectedItem == null)
            {
                MessageBox.Show("Seleccione el tipo de movimiento.", "¡Atención!", MessageBoxButton.OK,
                       MessageBoxImage.Exclamation);
                ComboTipo.Focus();
                return;
            } 

		    GuardarPedido(); 
		}

	    private void RadButton_Click(object sender, RoutedEventArgs e)
	    {
	        AgregarProducto(); 
	    }
 
        private void RadMenuItem_Click(object sender, RadRoutedEventArgs e)
        {
            var respuesta = (DialogResult)MessageBox.Show("El movimiento será cancelado, ¿Continuar?", "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                _productosEnEnvio = new ObservableCollection<InventarioProducto>();

                GridMovimientos.ItemsSource = _productosEnEnvio;
            } 
        }

		#endregion

		#region Métodos
         

	    private async void CargarDatos()
	    {
	        var _listadoMovimientos = await _serviciosInventarios.ObtenerListado();

            //Cargamos los movimientos que haya en la base de datos
            if (_listadoMovimientos != null)
            {
                if (FiltroSucursales.SelectedItem != null)
                {
                    _listadoMovimientos =
                        _listadoMovimientos.Where(
                            x => x.IdSucursalDestino == Convert.ToInt32(FiltroSucursales.SelectedValue)).ToList();
                }

                if (Fecha.SelectedValue != null)
                {
                    _listadoMovimientos =
                        _listadoMovimientos.Where(
                            x => x.Fecha == Fecha.SelectedValue).ToList();
                }

	            GridListado.ItemsSource = _listadoMovimientos.ToList();
	        } 
	    } 

	    private void AgregarProducto()
	    {
	        if (ComboProductos.SelectedItem == null)
	        {
	            MessageBox.Show("Seleccione un producto de la lista", "¡Atención!", MessageBoxButton.OK,
	                MessageBoxImage.Exclamation);

	            ComboProductos.Focus();

	            return;
	        }

            if (ComboUnidadMedida.SelectedItem == null)
            {
                MessageBox.Show("Seleccione la unidad de medida.", "¡Atención!", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

                ComboUnidadMedida.Focus();

                return;
            }

            if (Cantidad.Value <= 0 || String.IsNullOrEmpty(Cantidad.ContentText))
            {
                MessageBox.Show("Introduzca la cantidad que va a enviar.", "¡Atención!", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

                Cantidad.Focus();

                return;
            }

	        var _productoAgregado = new InventarioProducto
	        {
	            IdProducto = Convert.ToInt32(ComboProductos.SelectedValue),
	            Producto = ComboProductos.Text,
	            UnidadMedida = ComboUnidadMedida.Text,
	            IdUnidadMedida = Convert.ToInt32(ComboUnidadMedida.SelectedValue),
	            Cantidad = Convert.ToDouble(Cantidad.Value), 
                Precio = 0
	        }; 

	        _productosEnEnvio.Add(_productoAgregado); 

	        if (_productosEnEnvio.Count > 0)
	        {
	            GridMovimientos.ItemsSource = _productosEnEnvio.ToList();
	        }
	    }

	    private async void GuardarPedido()
	    {
	        if (_productosEnEnvio.Count == 0)
	        {
	            MessageBox.Show("No hay productos agregados a la lista.", "¡Atención!", MessageBoxButton.OK, MessageBoxImage.Error);
	            return; 
            }

	        try
	        {
                var _movimientoInventario = new Inventario
                {
                    IdSucursalDestino = Convert.ToInt32(ComboSucursalDestino.SelectedValue),
                    IdSucursalOrigen = Convert.ToInt32(ComboSucursalOrigen.SelectedValue),
                   
                    IdTipoMovimiento =  Convert.ToInt32(ComboTipo.SelectedValue),
                    _productosEnLista = _productosEnEnvio
                };

	            var _movimiento = await _serviciosInventarios.Insertar(_movimientoInventario);

	            if (_movimiento != null)
	            {
	                Folio.Text = _movimiento.Folio;

                    MessageBox.Show("El movimiento ha sido guardado correctamente.", "¡Éxito!", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

	            }
	        }
	        catch (Exception expt)
	        {
	            MessageBox.Show("Hubo un error al insertar los movimientos del inventario", "Error", MessageBoxButton.OK,
	                MessageBoxImage.Error);

	            throw;
	        } 
	    }

	    #endregion  

        private void RadMenuItem_Click_1(object sender, RadRoutedEventArgs e)
        {
            TabMovimiento.IsSelected = true;
        }

        private async void RadMenuItem_Click_2(object sender, RadRoutedEventArgs e)
        {
            if (GridListado.SelectedItem == null)
            {
                MessageBox.Show("Seleccione un elemento de la lista", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);

                return;
            }
           
            var respuesta = (DialogResult)MessageBox.Show("El movimiento será cancelado, ¿Continuar?", "Atención", MessageBoxButton.YesNo);

            if (respuesta != System.Windows.Forms.DialogResult.Yes) return; 

            try
            {
                var item = (Inventario)GridListado.SelectedItem;

                if (await _serviciosInventarios.Eliminar(item.Id))
                {
                    MessageBox.Show("Movimiento eliminado correctamente.", "Éxito.", MessageBoxButton.OK,
                    MessageBoxImage.Information); 
                }

                CargarDatos();
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error al cancelar el movimiento.", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                throw;
            }
             

        }

        private void FiltroSucursales_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CargarDatos();
        }
	}
}
