﻿using System;
using System.Collections.Generic;
using System.Windows;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Pizzeria.CapturaPedidos;
using Pizzeria.Catalogos.Mensajes;
using Pizzeria.Catalogos.TiposEntrega; 
using Telerik.Windows;
using Pizzeria.Catalogos.MetodosPago;
using Pizzeria.Catalogos.Sucursale;
using Pizzeria.Catalogos.Clientes;
using Pizzeria.Catalogos.Empleados;
using Pizzeria.Catalogos.TipoEmpleados;
using Pizzeria.Catalogos.Denominaciones;
using Pizzeria.Catalogos.Colores;
using Pizzeria.Catalogos.Usuarios;
using Pizzeria.Catalogos.Perfiles;
using Pizzeria.Catalogos.UnidadesMedida;
using Pizzeria.Catalogos.Productos;
namespace Pizzeria
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class VentanaPrincipal : Window
	{
	    private Sucursal DatosSucursal;
	   
	    private ServiciosSucursales _serviciosSucursales;
	    
        #region Constructor
		public VentanaPrincipal()
		{
			InitializeComponent();
		    
            _serviciosSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
        }
		#endregion

		#region Eventos
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DatosSucursal = await _serviciosSucursales.ObtenerDatosSucursal(ConstantesSistemas.IdSucursal[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); 
            } 
        }

		private void InventarioMovimientosClick(object sender, RadRoutedEventArgs e)
		{
			var ventana = new VentanaInventario {Owner = this};
			ventana.ShowDialog();
		}

        private void MetodosPagoClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoMetodosPago { Owner = this };
            ventana.ShowDialog();
        }

        private void SucursalesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoSucursal { Owner = this };
            ventana.ShowDialog();
        }
         

		private void VentasCapturarClick(object sender, RadRoutedEventArgs e)
		{ 
			var ventana = new ListaPedidosSucursal() {Owner = this, DatosSucursal = DatosSucursal};
			ventana.ShowDialog(); 
		}

        private void AbrirCajaClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoMovimientosCaja() { Owner = this };

            ventana.ShowDialog();
        }

	    private void TiposEntregaClick(object sender, RadRoutedEventArgs e)
	    {
            var ventana = new ListadoTipoEntrega(){ Owner = this   };

            ventana.ShowDialog();
	    }

        private void ClientesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoClientes() { Owner = this };

            ventana.ShowDialog();              
        }
        
        #endregion

        private void EmpleadosClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoEmpleado() { Owner = this };

            ventana.ShowDialog();              

        }

        private void TipoEmpleadosClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoTipoEmpleados() { Owner = this };

            ventana.ShowDialog();              
        }

        private void DenominacionesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoDenominaciones() { Owner = this };

            ventana.ShowDialog();              

        }

        private void ColoresClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoColores() { Owner = this };

            ventana.ShowDialog();              

        }

        private void UsuariosClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoUsuarios() { Owner = this };

            ventana.ShowDialog();              

        }

        private void PerfilesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoPerfiles() { Owner = this };

            ventana.ShowDialog();              

        }

        private void UnidadesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoUnidades() { Owner = this };

            ventana.ShowDialog();              

        }

        private void ProductosClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoProductos() { Owner = this };

            ventana.ShowDialog();              

        }

        private void SalirClick(object sender, RadRoutedEventArgs e)
        {
            Application.Current.Shutdown();   
        }

        private void MensajesClick(object sender, RadRoutedEventArgs e)
        {
            var ventana = new ListadoMensajes() { Owner = this };

            ventana.ShowDialog();

        }
    }
}
