﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Comun;
using Comun.Servicios;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria
{
    /// <summary>
    /// Interaction logic for VentanaListadoVentas.xaml
    /// </summary>
    public partial class VentanaListadoVentas : Window
    {
        private ServiciosPedidos _serviciosPedidos;
        public VentanaListadoVentas()
        {
            Thread.Sleep(2000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);
        }

        private  void FrmListadoVentas_Loaded(object sender, RoutedEventArgs e)
        {
            Fecha.Culture = new System.Globalization.CultureInfo("es-MX");
            Fecha.Culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            Fecha.DateTimeText = DateTime.Now.ToString("yyyy-MM-dd");


            CargarDatos();
        }

        private  void btnBuscar_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
           CargarDatos();
        }

        private async void CargarDatos()
        {
            var lista = await _serviciosPedidos.ObtenerPedidosPorSucursalyFecha(ConstantesSistemas.IdSucursal, Fecha.DateTimeText, Fecha.DateTimeText);

            GridPedidos.ItemsSource = null;
            if (lista != null)
            {
                lista = lista.Where(x => x.Pagado).ToList();

                GridPedidos.ItemsSource = lista;
            }
            else
            {
                MessageBox.Show("¡No hay pedidos cobrados para la fecha seleccionada!", "Atención", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        } 
         
    }
}
