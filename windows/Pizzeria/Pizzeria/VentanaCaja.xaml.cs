﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Telerik.Windows.Controls;

namespace Pizzeria
{
    /// <summary>
    /// Interaction logic for VentanaCaja.xaml
    /// </summary>
    public partial class VentanaCaja : Window
    {
        #region Variables

        private ServiciosCajas _serviciosCaja;
        ObservableCollection<Operacion> listaOperaciones = new ObservableCollection<Operacion>();
        private ListadoMovimientosCaja frmListadoCajas;
        public bool EstaAbiertaCaja;
      
        #endregion
       
        #region Constructor

        public VentanaCaja()
        {
            Thread.Sleep(2000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosCaja = new ServiciosCajas(ConstantesWS.Usuario, ConstantesWS.Password);
        }

        #endregion

        #region Eventos
        private async void FrmCaja_Loaded(object sender, RoutedEventArgs e)
        {
            frmListadoCajas = (ListadoMovimientosCaja)this.Owner;

            await CargarOperaciones();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (ValidarFormulario())
            {
                GuardarMovimientos();  
            }
        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();  
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            AgregarMovimientos();
        }

        #endregion

        #region Métodos

        private async Task CargarOperaciones()
        {
            var operaciones = await _serviciosCaja.ObtenerListaOperaciones();
            var denominaciones = await _serviciosCaja.ObtenerListaDenominaciones();

            ComboOperacion.ItemsSource = operaciones;
            ComboDenominacion.ItemsSource = denominaciones;
        }

        private void AgregarMovimientos()
        {
            if (String.IsNullOrEmpty(Cantidad.Value.ToString()) || Cantidad.Value == 0 || ComboDenominacion.SelectedItem == null)
                return;

            try
            {
                double SumaTotal = Double.Parse(ComboDenominacion.Text, CultureInfo.InvariantCulture) * Double.Parse(Cantidad.ContentText, CultureInfo.InvariantCulture);

                var operacion = new Operacion()
                {
                    IdDenominacion = Convert.ToInt32(ComboDenominacion.SelectedValue),
                    Denominacion = Double.Parse(ComboDenominacion.Text, CultureInfo.InvariantCulture),
                    Cantidad = Convert.ToInt32(Cantidad.Value),
                    Total = SumaTotal
                };

                listaOperaciones.Add(operacion);

                Total.Text = string.Format("Total: $ {0}", listaOperaciones.Sum(x => x.Total));

                GridMovimientos.ItemsSource = listaOperaciones;
            }
            catch (Exception e)
            {
                MessageBox.Show("Hubo un error al realizar la operación, pruebe en un momento", "Atención", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            } 
        }

        private bool ValidarFormulario()
        {
            if ((ComboOperacion.SelectedItem  == null))
            return false;

            if (ComboDenominacion.SelectedItem == null)
            {
                return false;
            }

            if (Convert.ToInt32(ComboOperacion.SelectedValue) == ConstantesSistemas.IdEstadoCajaAbierta && EstaAbiertaCaja)
            {
                MessageBox.Show("La caja ya ha sido abierta anteriormente .", "¡Atención!", MessageBoxButton.OK, MessageBoxImage.Warning);

                return false;
            }

            if (listaOperaciones.Count == 0)
            {
                MessageBox.Show("No ha agregado ningún valor monetario.", "¡Atención!", MessageBoxButton.OK, MessageBoxImage.Warning);
          
                return false;
            }

            if (Convert.ToInt32(ComboOperacion.SelectedValue) == ConstantesSistemas.IdRetiroEfectivo && String.IsNullOrEmpty(Observaciones.Text))
            {
                MessageBox.Show("Cuando se trate de un retiro de efectivo, deberá justificar el movimiento.");
                return false;
            }

            return true;
        } 

        private async void GuardarMovimientos()
        {
            try
            {
                var caja = new Caja
                {
                    IdOperacion = Convert.ToInt32(ComboOperacion.SelectedValue),
                    IdSucursal = ConstantesSistemas.IdSucursal[0],
                    IdUsuario = ConstantesSistemas.IdUsuario,
                    Observaciones = Observaciones.Text,
                    Total = Convert.ToDecimal(listaOperaciones.Sum(x => x.Total)),
                    Fecha = DateTime.Now,
                    Detalle = new List<CajaDetalle>()
                };

                foreach (var detalle in listaOperaciones)
                {
                    var cajita = new CajaDetalle { IdDenominacion = detalle.IdDenominacion, Cantidad = detalle.Cantidad };

                    caja.Detalle.Add(cajita);
                }
                
                await _serviciosCaja.GuardarCaja(caja);

                await frmListadoCajas.CargarPedidos();

                listaOperaciones = new ObservableCollection<Operacion>();

                GridMovimientos.ItemsSource = listaOperaciones;

                Total.Text = string.Empty;

                Observaciones.Text = string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        } 

        #endregion 

        private class Operacion
        {
            public int IdDenominacion { get; set; }
            public double Denominacion { get; set; }
            public Int32 Cantidad { get; set; }
            public double Total { get; set; } 
        } 
    } 
}
