﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.Formularios
{
    /// <summary>
    /// Interaction logic for FrmVentaDetalle.xaml
    /// </summary>
    public partial class FrmVentaDetalle : Window
    {
        private ServiciosProductos _serviciosProductos;
        private List<Producto> _listaProductos;
        public Producto ProductoEnVenta;
        private VentanaVentas frmVentanaVentas;

        public FrmVentaDetalle()
        {
            _serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void FrmProductoDetalle_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Obtenemos una copia de la ventana madre
                frmVentanaVentas = (VentanaVentas)this.Owner;

                //Pinta el nombre del producto
                NombreProducto.Content = ProductoEnVenta.Nombre;

                //Solamente estarán activos para cuando sea pizza Mitad y Mitad
                PrimeraMitad.IsEnabled = false;
                SegundaMitad.IsEnabled = false;
                //solamente estará activo cuando sea personalizada ...
                TabIngredientes.IsEnabled = false;
                TabIngredientesExtras.IsSelected = true;

                if (ProductoEnVenta.Tamanios != null)
                {
                    Tamanios.ItemsSource = ProductoEnVenta.Tamanios;
                    Tamanios.DisplayMemberPath = "Tamanio";
                    Tamanios.SelectedValuePath = "Id";
                }

                if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas)
                {
                    if (ProductoEnVenta.CantidadIngredientes == 0) //Para las pizzas Mitad y Mitad
                    {
                        _listaProductos = await _serviciosProductos.ObtenerTodos();

                        _listaProductos = _listaProductos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEspecialidades).OrderBy(x => x.Nombre).ToList();

                        PrimeraMitad.DisplayMemberPath = "Nombre";
                        PrimeraMitad.SelectedValuePath = "Id";
                        PrimeraMitad.ItemsSource = _listaProductos;
                        PrimeraMitad.IsEnabled = true;

                        SegundaMitad.DisplayMemberPath = "Nombre";
                        SegundaMitad.SelectedValuePath = "Id";
                        SegundaMitad.ItemsSource = _listaProductos;
                        SegundaMitad.IsEnabled = true;
                    }
                    else //Para las pizzas personalizadas según el número de ingredientes
                    {
                        TabIngredientes.IsEnabled = true;
                        TabIngredientes.IsSelected = true;
                    } 
                }

                var listaIngredientes = await _serviciosProductos.ObtenerIngredientes();

                listaIngredientes = listaIngredientes.OrderBy(x => x.Nombre).ToList();

                ListaIngredientes.ItemsSource = listaIngredientes;
                ListaIngredientesExtra.ItemsSource = listaIngredientes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }  
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (ValidarFormulario())
            {
                AgregarProducto();
            }
        }

        private bool ValidarFormulario()
        {
            //Validamos que el tamaño esté seleccionado
            if (Tamanios.SelectedItem == null)
            {
                MessageBox.Show("Seleccione el tamaño de la pizza", "Error", MessageBoxButton.OK);

                return false;
            }
            
            //Validamos las personalizadas, según número de ingredientes
            if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas && ProductoEnVenta.CantidadIngredientes > 0)
            {
                if (ListaIngredientes.SelectedItems.Count == 0 ||  ListaIngredientes.SelectedItems.Count > ProductoEnVenta.CantidadIngredientes)
                {
                    MessageBox.Show(string.Format("Para este producto, solamente puede seleccionar {0}", ProductoEnVenta.CantidadIngredientes), "Error", MessageBoxButton.OK);

                    return false;
                }

            }

            //Validamos las personalizadas, mitad y mitad
            if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas && ProductoEnVenta.CantidadIngredientes == 0)
            {
                if (PrimeraMitad.SelectedItem == null || SegundaMitad.SelectedItem == null)
                {
                    MessageBox.Show("Para este producto, seleccione ambas mitades", "Error", MessageBoxButton.OK);

                    return false;
                } 
            }

            return true;
        }

        private void AgregarProducto()
        {
            try
            {
                var productoVendido = new PedidoProducto
                {
                    IdProducto = ProductoEnVenta.Id,
                    Producto = ProductoEnVenta.Nombre,
                    Observaciones = Observaciones.Text,
                    Cantidad = Convert.ToInt32(NumeroPizzas.Value), 
                    Ingredientes = new List<Ingrediente>()
                };

                //localizamos el precio de venta en el areglo de tamaños
                if (ProductoEnVenta.Tamanios.Count > 0)
                { 
                    var itemTamanio = (ProductoTamanio)ProductoEnVenta.Tamanios.Find(x => x.Id == Convert.ToInt32(Tamanios.SelectedValue));

                    productoVendido.Precio = itemTamanio.PrecioVenta;
                    productoVendido.Tamanio = itemTamanio.Tamanio;
                    productoVendido.IdTamanio = itemTamanio.IdTamanio;
                }
                else //O lo obtenemos directamente del 
                {
                    productoVendido.Precio = ProductoEnVenta.PrecioVenta;
                    productoVendido.Tamanio = String.Empty;
                } 

                //Detectamos si es una personalizada mitad y mitad, 
                //cada mitad será almacenada como un ingrediente

                if (ProductoEnVenta.IdClasificacion == ConstantesSistemas.IdPersonalizadas &&  ProductoEnVenta.CantidadIngredientes == 0)
                {
                    var primeraMitad = (Producto)PrimeraMitad.SelectedItem;
                    var segundaMitad = (Producto)SegundaMitad.SelectedItem;

                    if (primeraMitad != null)
                    {
                        var ingredienteExtra = new Ingrediente
                        {
                            EsExtra = false,
                            IdProducto = primeraMitad.Id,
                            PrecioVenta = 0, Descripcion = primeraMitad.Nombre
                        };

                        productoVendido.Ingredientes.Add(ingredienteExtra);
                    }

                    if (segundaMitad != null)
                    {
                        var ingredienteExtra = new Ingrediente
                        {
                            EsExtra = false,
                            IdProducto = segundaMitad.Id,
                            PrecioVenta = 0, Descripcion = segundaMitad.Nombre
                        };

                        productoVendido.Ingredientes.Add(ingredienteExtra);
                    }
                }

                //Obtenemos la lista de los ingredientes en el caso de las pizzas personalizadas
                if (ListaIngredientes.SelectedItems.Count > 0)
                {
                    foreach (Producto extra in ListaIngredientes.SelectedItems)
                    {
                        var ingredienteExtra = new Ingrediente
                        {
                            EsExtra = false,
                            IdProducto = extra.Id,
                            PrecioVenta = 0, Descripcion = extra.Nombre
                        }; 

                        productoVendido.Ingredientes.Add(ingredienteExtra);
                    }
                }
                 
                //Verificamos que tenga ingredientes extra
                if (ListaIngredientesExtra.SelectedItems.Count > 0)
                {
                    foreach (Producto extra in ListaIngredientesExtra.SelectedItems)
                    {
                        var ingredienteExtra = new Ingrediente
                        {
                            EsExtra = true,
                            IdProducto = extra.Id,
                            PrecioVenta = extra.PrecioVenta ?? 0,
                            Descripcion = extra.Nombre

                        }; 

                        productoVendido.Ingredientes.Add(ingredienteExtra);
                    }
                }

                //Calculamos el total de la venta del producto + ingredientes adicionales si los tiene

                productoVendido.TotalIngredientesExtras = 0;
               
                if (productoVendido.Ingredientes.Count > 0)
                {
                    productoVendido.TotalIngredientesExtras = productoVendido.Ingredientes.Sum(x => x.PrecioVenta);
                }

                productoVendido.Total = productoVendido.Precio + productoVendido.TotalIngredientesExtras;

                //Pintamos los datos en la ventana principal
                frmVentanaVentas._productosEnPedido.Add(productoVendido);

                frmVentanaVentas.GridPedido.ItemsSource = frmVentanaVentas._productosEnPedido; 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                
                throw;
            }
        }
    }
}
