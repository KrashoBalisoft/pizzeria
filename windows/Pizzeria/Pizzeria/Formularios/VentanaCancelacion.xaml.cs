﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Comun;
using Comun.Servicios;
using Telerik.Windows.Controls;

namespace Pizzeria.Formularios
{
    /// <summary>
    /// Interaction logic for VentanaCancelacion.xaml
    /// </summary>
    public partial class VentanaCancelacion : Window
    {
        public int IdMovimiento;
        private ServiciosCajas _serviciosCaja;
        private ListadoMovimientosCaja frmListadoCajas;
       
        public VentanaCancelacion()
        {
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosCaja = new ServiciosCajas(ConstantesWS.Usuario, ConstantesWS.Password);
        }
        private void VentanaCancelacion_Loaded(object sender, RoutedEventArgs e)
        {
            frmListadoCajas = (ListadoMovimientosCaja)this.Owner;
        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void RadButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (await _serviciosCaja.CancelarMovimientoCaja(IdMovimiento, Motivo.Text))
                {
                    MessageBox.Show("La cancelación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);
                    await frmListadoCajas.CargarPedidos();

                    Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            } 
        }  
    }
}
