﻿using System.Linq;
using System.Windows.Controls;
using System.Windows.Forms;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media; 
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MessageBox = System.Windows.MessageBox;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Pizzeria.CapturaPedidos
{
	/// <summary>
	/// Interaction logic for MainWindow.xam
	/// </summary>
	public partial class ListaPedidosSucursal : Window
	{
		#region Variables

		private ServiciosPedidos _serviciosPedidos;
	    private ServiciosEstadosPedido _serviciosEstadosPedido;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
	   
        public Sucursal DatosSucursal;
	    

		#endregion

		#region Constructor

		public ListaPedidosSucursal()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

			_serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosEstadosPedido = new ServiciosEstadosPedido(ConstantesWS.Usuario, ConstantesWS.Password);

            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 1, 0);
            dispatcherTimer.Start();
		}

		#endregion

		#region Eventos

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
                //Los datos de la sucursal
			    NombreSucursal.Content = DatosSucursal.Nombre;

                //Cargamos el listado de los estados del pedido
			    var listaEstados = await _serviciosEstadosPedido.ObtenerListaEstadoPedido();

			    Filtro.DisplayMemberPath = "Descripcion";
			    Filtro.SelectedValuePath = "Id";
			   

                Filtro.ItemsSource =  listaEstados;
			    Filtro.SelectedIndex = 1; 

			    //Cargar la lista de pedidos
                await CargarPedidos(); 
			}
			catch (Exception)
			{
				//MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}  

        private async void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            await CargarPedidos();
        } 
       
        private void Agregar_Click(object sender, RoutedEventArgs e)
        {
            var ventanaCapturarPedido = new VentanaVentas()
            {
                Owner = this, DatosSucursal = this.DatosSucursal
            };

            ventanaCapturarPedido.Show();
        }

        private async void Enviar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null)
            {

                MessageBox.Show("¡Seleccione una operación para enviar a la cocina!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;

            if (item.IdEstatus != ConstantesSistemas.IdEstadoInicialPedido)
            {
                MessageBox.Show("El pedido ya ha sido enviado a la cocina o ha sido cancelado", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            } 

            try
            { 
                if (await _serviciosPedidos.EnviarPedidoALaCocina(item.Id))
                {
                    MessageBox.Show("El pedido ha sido enviado a la cocina correctamente.", "Éxito", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                }

                await CargarPedidos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error." + ex.ToString());
            }
        }

        private async void Cancelar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null)
            {
               MessageBox.Show("¡Seleccione una operación para cancelar!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
               return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;
            
            if (item.Pagado)
            {
                MessageBox.Show("El pedido ya ha sido cobrado, ya no puede ser cancelado.", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

            try
            {
                var respuesta = (DialogResult)MessageBox.Show("El pedido será cancelado, ¿Continuar?", "Atención", MessageBoxButton.YesNo);

                if (respuesta == System.Windows.Forms.DialogResult.Yes)
                {
                     
                    if (await _serviciosPedidos.CancelarPedido(item.Id))
                    {
                        MessageBox.Show("El pedido ha sido cancelado correctamente.", "Éxito", MessageBoxButton.OK,
                            MessageBoxImage.Exclamation);
                    }

                    await CargarPedidos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error." + ex.ToString());
            }
        }

	    private void Transferir_Click(object sender, RoutedEventArgs e)
	    {
            if (ListadoPedidos.SelectedItem == null)
            { 
                MessageBox.Show("¡Seleccione un pedido para transferir!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;

            if (item.Pagado)
            {
                MessageBox.Show("El pedido ya ha sido cobrado, no puede transferirlo.", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

            if (item.IdEstatus != ConstantesSistemas.IdEstadoInicialPedido)
            {
                MessageBox.Show("El pedido ya ha sido enviado a la cocina o ha sido cancelado", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

	        var VentanaTransferir = new FrmTransferirPedido() {Owner = this, pedido = item};

	        VentanaTransferir.ShowDialog();
	    }

        private void Repartir_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItems.Count == 0)
            {
                MessageBox.Show("¡Seleccione al menos un pedido!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;

            if (item.IdEstatus == ConstantesSistemas.IdEnReparto)
            {
                MessageBox.Show("¡El pedido ya fue asignado a un repartidor!", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            } 

            if (item.IdEstatus != ConstantesSistemas.IdEstadoTerminado)
            {
                MessageBox.Show("¡El pedido no está terminado aún, no puede ser asignado!", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

            var pedidosSeleccionados = new int[ListadoPedidos.Items.Count];
            int id = 0;
            foreach (Pedido pedido in ListadoPedidos.SelectedItems)
            {
                pedidosSeleccionados[id] = pedido.Id;
                id ++;
            }

            var ventanaRepartir = new FrmAsignarPedidos
            {
                listaPedidosRepartir = pedidosSeleccionados, ventanaListaPedidos = this
            };

            ventanaRepartir.ShowDialog();
        }
         
        private async void Cambiar_Urgente_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null)
            {
                MessageBox.Show("¡Seleccione un pedido!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;  

            try
            {
                var respuesta = (DialogResult)MessageBox.Show("El pedido será marcado como urgente, ¿Continuar?", "Atención", MessageBoxButton.YesNo);
                
                if (respuesta == System.Windows.Forms.DialogResult.Yes)
                {

                    if (await _serviciosPedidos.MandarPedidoUrgente(item.Id))
                    {
                        MessageBox.Show("El pedido ha sido modificado correctamente.", "Éxito", MessageBoxButton.OK,
                            MessageBoxImage.Exclamation);
                    }

                    await CargarPedidos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error." + ex.ToString());
            }
        }

        private async void Cobrar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null)
            {
                MessageBox.Show("¡Seleccione una operación para cobrar!", "¡Atención!", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                return;
            }

            var item = (Pedido)ListadoPedidos.SelectedItem;

            if (item.Pagado)
            {
                MessageBox.Show("El pedido ya ha sido cobrado anteriormente.", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

            var _ventanaCobrar = new FrmCobro() { Owner = this, _pedido = item };

            _ventanaCobrar.ShowDialog();

           await CargarPedidos();
        }
	 
	 
		#endregion

		#region Métodos

		public async Task CargarPedidos()
		{
		    try
		    {
                ListadoPedidos.SelectionMode = SelectionMode.Single;
  
                var pedidos = await _serviciosPedidos.ObtenerPedidosPorSucursal(ConstantesSistemas.IdSucursal);

		        if (pedidos != null)
		        {
                    if (Convert.ToInt32(Filtro.SelectedValue) != 0)
                    {
                        pedidos = pedidos.Where(x => x.IdEstatus == Convert.ToInt32(Filtro.SelectedValue)).ToList();
                    }

                    ListadoPedidos.ItemsSource = pedidos; 
                } 
		    }
		    catch (Exception e)
		    {
		        MessageBox.Show(e.ToString());
		    }
		}

		#endregion 

        private async void Filtro_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            await CargarPedidos();
        }
    }
}
