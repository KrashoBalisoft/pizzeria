﻿using System;
using System.Collections.Generic;
using System.Windows;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.CapturaPedidos
{
    /// <summary>
    /// Interaction logic for FrmTransferirPedido.xaml
    /// </summary>
    public partial class FrmTransferirPedido : Window
    {
        public Pedido pedido = new Pedido();
        private ServiciosSucursales _serviciosSucursales;
        private ServiciosPedidos _serviciosPedidos;
        private List<Sucursal> _listaSucursales;
        private ListaPedidosSucursal frmListadoPedidos;

        public FrmTransferirPedido()
        {
            _serviciosSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private void FrmTransferir_Loaded(object sender, RoutedEventArgs e)
        {
            frmListadoPedidos = (ListaPedidosSucursal) this.Owner;

            CargarSucursales();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (ListaSucursales.SelectedItem == null)
            {
                MessageBox.Show("Seleccione una sucursal", "Atención", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }

            try
            { 
                var item = (Sucursal)ListaSucursales.SelectedItem;

                if (await _serviciosPedidos.TransferirPedido(pedido.Id, item.Id))
                {
                    MessageBox.Show("Pedido transferido con éxito", "¡Éxito!", MessageBoxButton.OK);

                    await frmListadoPedidos.CargarPedidos();

                    Close(); 
                }  
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un problema al realizar la transferencia");
                throw;
            } 
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void CargarSucursales()
        {
            try
            {
                _listaSucursales = await _serviciosSucursales.ObtenerListaSucursales();

                ListaSucursales.ItemsSource = _listaSucursales;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error al cargar los datos de las sucursales", "Atención", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation); 
            }
        }
    }


 
}
