﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Comun;
using Comun.Entidades.Catalogos;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;

namespace Pizzeria.CapturaPedidos
{
    /// <summary>
    /// Interaction logic for FrmAsignarPedidos.xaml
    /// </summary>
    public partial class FrmAsignarPedidos : Window
    {
        public int[] listaPedidosRepartir;
        private ServiciosEmpleado _serviciosEmpleados;
        private ServiciosPedidos _serviciosPedidos;
        public ListaPedidosSucursal ventanaListaPedidos = new ListaPedidosSucursal();

        public FrmAsignarPedidos()
        {
            Thread.Sleep(2000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosEmpleados = new ServiciosEmpleado(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var lista = await _serviciosEmpleados.ObtenerLista();

            if (lista.Any())
            {
                List<List<Empleado>> lsts = new List<List<Empleado>>();
                var filas = Math.Ceiling((double)(lista.Count() / 5)); //5 Registros por línea
                var numeroDeElementos = lista.Count();
                int index = 0;

                for (int i = 0; i < filas + 1; i++)
                {
                    lsts.Add(new List<Empleado>());

                    for (int j = 0; j < 5; j++)
                    {
                        if (index < numeroDeElementos)
                        {
                            lsts[i].Add(lista[index]);
                        }
                        
                        index++;
                    }
                }

                InitializeComponent();

                employeesList.ItemsSource = lsts;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = (int)(sender as Button).Tag; 

                if (listaPedidosRepartir.Any())
                {
                     
                    foreach (int idPedido in listaPedidosRepartir)
                    {
                        await _serviciosPedidos.AsignarPedido(idPedido, id);
                    }

                    MessageBox.Show("¡Pedidos asignados correctamente!", "¡Éxito!", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);

                    await ventanaListaPedidos.CargarPedidos();

                    Close();
                }
            }
            catch (Exception x)
            {
                MessageBox.Show("!Hubo un problema con la asignación!" + x); 
            }
        } 
    } 
}

