﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Telerik.Windows.Controls;
using TicketingSystem;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.CapturaPedidos
{
	/// <summary>
	/// Interaction logic for VentanaVentas.xaml
	/// </summary>
	public partial class VentanaVentas : Window
	{
        private ServiciosProductos _serviciosProductos;
	    private ServiciosPedidos _serviciosPedidos;
        private List<Producto> _productos;
        public ObservableCollection<PedidoProducto> _productosEnPedido;
        private Producto productoSeleccionado = new Producto();
	    private ListaPedidosSucursal frmListadoSucursal;

	    public Sucursal DatosSucursal;

	   
		public VentanaVentas()
		{
			InitializeComponent();

            StyleManager.ApplicationTheme = new Windows8TouchTheme();
           
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            _serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password); 
		} 

        private async void VentanaVentas_OnLoaded(object sender, RoutedEventArgs e)
        {
            frmListadoSucursal = (ListaPedidosSucursal)this.Owner;
            await CargarProductos();  
            _productosEnPedido = new ObservableCollection<PedidoProducto>();  
        }
         
        private async Task CargarProductos()
        {
            try
            {
                _productos = await _serviciosProductos.ObtenerTodos();

                if (_productos.Any())
                {
                    int index = 0;
                    while (index < _productos.Count)
                    {
                        _productos[index].NombreImagen = String.Format("{0}{1}", ConstantesSistemas.DirectorioImagenesCallCenter, _productos[index].NombreImagen);
                        index++;
                    }

                    ListEspecialidades.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEspecialidades).OrderBy(x => x.Nombre);
                    ListEntremesesPastas.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdEntremeses || x.IdClasificacion == ConstantesSistemas.IdPastas).OrderBy(x => x.Nombre);
                    ListPersonalizadas.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdPersonalizadas).OrderBy(x => x.Nombre);
                    ListBebidas.ItemsSource = _productos.Where(x => x.IdClasificacion == ConstantesSistemas.IdBebidas).OrderBy(x => x.Nombre);
            
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);   
                throw;
            } 
        } 

        private void btnEspecialidad_Click(object sender, RoutedEventArgs e)
        {
            if (productoSeleccionado == null)
              return;

            switch (productoSeleccionado.IdClasificacion)
            {
                case ConstantesSistemas.IdBebidas:
                case ConstantesSistemas.IdEntremeses:
                case ConstantesSistemas.IdPastas:
                    AgregarProducto(productoSeleccionado);
                    break;
                default:
                    MostrarVentanaDetalle(productoSeleccionado); 
                    break; 
            } 
        }

        private void btnQuitar_Click(object sender, RoutedEventArgs e)
        {
            if (GridPedido.SelectedItem == null)
                return;

            int index = GridPedido.Items.IndexOf(GridPedido.SelectedItem);

            //Eliminamos el producto de la lista de pedidos
            _productosEnPedido.RemoveAt(index);

            GridPedido.ItemsSource = _productosEnPedido;
        }

        private void Enviar_Click(object sender, RoutedEventArgs e)
        {
            //Verificamos que haya al menos un producto agregado al pedido...
            if (_productosEnPedido.Count == 0) return;

            var ventanaCobro = new FrmCobro
            {
                Owner = this
            };

            ventanaCobro.ShowDialog(); 
        } 

	    private void AgregarProducto(Producto producto)
	    {
	        var productoVendido = new PedidoProducto();

	        productoVendido.IdProducto = producto.Id;
	        productoVendido.Producto = producto.Nombre;
	        productoVendido.Precio = producto.PrecioVenta;
            productoVendido.Total = producto.PrecioVenta;
	        productoVendido.Observaciones = string.Empty;
	        productoVendido.Cantidad = 1;
           
            _productosEnPedido.Add(productoVendido);

            GridPedido.ItemsSource = _productosEnPedido; 
        } 

	    private void MostrarVentanaDetalle(Producto item)
	    {
	        var frmVentanaDetalle = new FrmVentaDetalle
	        {
                Owner = this, 
	            ProductoEnVenta = item
	        };

	        frmVentanaDetalle.ShowDialog();
	    }

        private void Cancelar_Click(object sender, RoutedEventArgs e)
        {
            if (_productosEnPedido.Count == 0) return;

            CancelarPedido();
        }

        private void ListEspecialidades_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListEntremesesPastas.SelectedIndex = -1;
            ListPersonalizadas.SelectedIndex = -1;
            ListBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListEspecialidades.SelectedItem;
        }

        private void ListEntremesesPastas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListEspecialidades.SelectedIndex = -1;
            ListPersonalizadas.SelectedIndex = -1;
            ListBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListEntremesesPastas.SelectedItem;
        }

        private void ListPersonalizadas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListEspecialidades.SelectedIndex = -1;
            ListEntremesesPastas.SelectedIndex = -1;
            ListBebidas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListPersonalizadas.SelectedItem; 
        }

        private void ListBebidas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListEspecialidades.SelectedIndex = -1;
            ListPersonalizadas.SelectedIndex = -1;
            ListEntremesesPastas.SelectedIndex = -1;

            productoSeleccionado = (Producto)ListBebidas.SelectedItem;
        } 

	    public async void GuardarPedido(int IdTipoEntrega, int IdMetodoPago, int esUrgente)
	    {
            try
            {  
                var pedido = new Pedido
                    {
                        IdCliente = DatosSucursal.IdCliente,
                        IdSucursal = DatosSucursal.Id,
                        IdEstatus = ConstantesSistemas.IdEstadoEnviadoCocina,
                        IdTipoEntrega = IdTipoEntrega,
                        IdMetodoPago = IdMetodoPago,
                        IdOrigen = ConstantesSistemas.IdOrigen,
                        FechaPedido =  DateTime.Now, 
                        Hora = string.Format("{0}:{1}:{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                        
                        EnviarPedido = false, 
                        Domicilio = String.Empty,  
                        Descuento = 0,
                        Productos = new List<PedidoProducto>(_productosEnPedido.ToList()),
                        Subtotal = _productosEnPedido.Sum(x => x.Total),
                        Pagado = true, 
                        Urgente = esUrgente
                    };
               
                var _pedido = await _serviciosPedidos.AgregarPedidoSucursal(pedido);

                if (_pedido != null)
                {
                    /**********
                     * Imprimimos el ticket
                     **********/
                    Ticket tkt = new Ticket();  
                    tkt._pedido = _pedido;
                    tkt._sucursal = DatosSucursal;  
                    tkt.print(ConstantesSistemas.NumeroCopias); //Parámetro que indica el número de veces que será impreso el recibo
                   
                    MessageBox.Show("El pedido se ha guardado y enviado a la cocina correctamente", "Éxito",
                        MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    _productosEnPedido = new ObservableCollection<PedidoProducto>();

                    GridPedido.ItemsSource = _productosEnPedido;

                    await frmListadoSucursal.CargarPedidos();

                    return;
                }

                MessageBox.Show("Hubo problemas al guardar el pedido", "¡Atención!",  MessageBoxButton.OK, MessageBoxImage.Exclamation);
             }
            catch (Exception ex)
            {
                MessageBox.Show("¡Hubo un problema con la impresora, conéctela!", "¡Atención!", MessageBoxButton.OK); 
            }
	    } 

	    public void CancelarPedido()
	    {
            var respuesta = (DialogResult)MessageBox.Show("El pedido será eliminado, ¿Continuar?", "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                _productosEnPedido = new ObservableCollection<PedidoProducto>();

                GridPedido.ItemsSource = _productosEnPedido;
            }  
	    }

	} 
}
