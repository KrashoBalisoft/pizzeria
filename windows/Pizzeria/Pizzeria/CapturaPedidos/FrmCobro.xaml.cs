﻿using System;
using System.Linq;
using System.Windows;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.CapturaPedidos
{
    /// <summary>
    /// Interaction logic for FrmCobro.xaml
    /// </summary>
    public partial class FrmCobro : Window
    {
        private VentanaVentas ventanaPedidos = new VentanaVentas();
        private ServiciosTipoEntrega _serviciosTipoEntrega;
        private ServiciosMetodosPago _serviciosMetodosPago;
        private ServiciosPedidos _serviciosPedidos;
        public Pedido _pedido;
        public FrmCobro()
        {
            _serviciosTipoEntrega = new ServiciosTipoEntrega(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosMetodosPago = new ServiciosMetodosPago(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private async void FrmCobroPedido_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.Owner is VentanaVentas)
            { 
                ventanaPedidos = (VentanaVentas)this.Owner;
            } 

            TipoEntrega.SelectedValuePath = "Id";
            TipoEntrega.DisplayMemberPath = "Descripcion";
            TipoEntrega.ItemsSource = await _serviciosTipoEntrega.ObtenerListaTipoEntrega();
            
            TipoPago.SelectedValuePath = "Id";
            TipoPago.DisplayMemberPath = "Descripcion";
            TipoPago.ItemsSource = await _serviciosMetodosPago.ObtenerLista();

            CargarDatosPedido();

            Efectivo.Focus();
        }

        private void CargarDatosPedido()
        {
            try
            {
                //Si el llamado viene desde la ventana de los productos
                if (this.Owner is VentanaVentas)
                {
                    GridDetalleVentas.ItemsSource = ventanaPedidos._productosEnPedido.ToList();
                    Total.Value = Convert.ToDouble(ventanaPedidos._productosEnPedido.Sum(x => x.Total));
                }
                else if (_pedido != null)
                     {
                         GridDetalleVentas.ItemsSource = _pedido.Productos.ToList();
                         Total.Value = Convert.ToDouble(_pedido.Subtotal);

                         TipoEntrega.SelectedValue = _pedido.IdTipoEntrega;
                         TipoPago.SelectedValue = _pedido.IdMetodoPago;

                         if (!String.IsNullOrEmpty(_pedido.Entregado))
                         {
                             Entregado.Visibility = Visibility.Visible;
                             TxtMotivo.Visibility = Visibility.Visible;  
                         } 
                     }
            }
            catch (Exception)              
            {
                MessageBox.Show("Hubo un error al cargar los datos.");
                throw;
            }
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            //ventanaPedidos.GuardarPedido();
            if (String.IsNullOrEmpty(Efectivo.Value.ToString()) || Efectivo.Value < Total.Value)
            {
                MessageBox.Show("¡Indica correctamente el monto del efectivo!", "Atención", MessageBoxButton.OK);
                
                return;
            }

            if (TipoEntrega.SelectedItem == null)
            {
                MessageBox.Show("¡Indica el tipo de entrega!", "Atención", MessageBoxButton.OK);

                return;
            }

            if (Entregado.Visibility == Visibility.Visible && !(bool)Entregado.IsChecked && String.IsNullOrEmpty(TxtMotivo.Text))
            {
                MessageBox.Show("¡Indica la razón por la que no fue entregada la pizza!", "Atención", MessageBoxButton.OK);

                return; 
            }

            btnAceptar.IsEnabled = false;

            Cambio.Value = (Efectivo.Value - Total.Value);
            int esUrgente = 0;
            if (isUrgent.IsChecked != null && (bool) isUrgent.IsChecked) esUrgente = 1;

            if (this.Owner is VentanaVentas)
            {
                ventanaPedidos.GuardarPedido(Convert.ToInt32(TipoEntrega.SelectedValue), Convert.ToInt32(TipoPago.SelectedValue), esUrgente);
            }
            else
            {
                //Indicamos que el pedido ya está pagado
                _pedido.Pagado = true;

                if (Entregado.Visibility == Visibility.Visible)
                { 
                    int entregado = 1;
                    if (Entregado.IsChecked != null && (bool) !Entregado.IsChecked)
                    {
                        entregado = 0;
                        _pedido.Pagado = false;
                    } 
                    
                    //Confirma o no la entrega del pedido
                    await _serviciosPedidos.ConfirmarPedido(_pedido.Id, entregado, TxtMotivo.Text);
                    //Cancelamos el pedido
                    await _serviciosPedidos.CancelarPedido(_pedido.Id);
                }

                if (_pedido.Pagado)
                {
                    if (await _serviciosPedidos.CobrarPedido(_pedido.Id))
                    {
                        MessageBox.Show("El pedido ha sido cobrado correctamente.", "Éxito", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    } 
                }
                else
                {
                    MessageBox.Show("El pedido ha sido cancelado correctamente.", "Éxito", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }

            btnAceptar.IsEnabled = true;

            Close();
        }

        private void btcCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }  
         
        private void Efectivo_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {  
            Cambio.Value = Efectivo.Value - Total.Value; 
        } 
    }
}
