﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Pizzeria.CapturaPedidos;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class ListadoMovimientosCaja : Window
	{
		#region Variables

		private ServiciosCajas _serviciosCaja;

	    private bool estaAbiertaCaja = false;
	    private bool estaCerradaCaja = false;

	    #endregion

		#region Constructor

        public ListadoMovimientosCaja()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

            _serviciosCaja = new ServiciosCajas(ConstantesWS.Usuario, ConstantesWS.Password);
		}

		#endregion

		#region Eventos

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
				await CargarPedidos();
			  //  estaAbiertaCaja = await _serviciosCaja.CajaAbierta(ConstantesSistemas.IdSucursal);
			}
			catch (Exception)
			{
				//MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		}
         
        //Agregar movimientos 
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if (estaCerradaCaja)
            {
                MessageBox.Show("La caja ya ha sido cerrada ¡No puedes agregar más movimientos!", "¡Atención!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var ventanaMovimientoCaja = new VentanaCaja()
            {
                Owner = this,
                EstaAbiertaCaja = estaAbiertaCaja
            };

            ventanaMovimientoCaja.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CancelarMovimiento(); 
        }
          
		#endregion

		#region Métodos

		public async Task CargarPedidos()
		{
		    estaAbiertaCaja = false;
            estaCerradaCaja = false;
			
            var movimientos = await _serviciosCaja.ObtenerListaMovimientos(ConstantesSistemas.IdSucursal);

		    if (movimientos.Count > 0)
		    {
		        GridMovimientosCaja.ItemsSource = movimientos;

                //Verificamos que la caja no haya sido abierta
		        if (movimientos.Count(x => x.IdOperacion == ConstantesSistemas.IdEstadoCajaAbierta) > 0)
		        {
		            estaAbiertaCaja = true;
		        }

                //Verificamos que la caja aún no tenga un movimiento de caja cerrada
                if (movimientos.Count(x => x.IdOperacion == ConstantesSistemas.IdCierreCaja) > 0)
                {
                    estaCerradaCaja = true;
                }
		    }
         
		}

	    private void CancelarMovimiento()
	    {
	        if (GridMovimientosCaja.SelectedItem == null)
	        {
	            MessageBox.Show("Seleccione un movimiento", "Atención", MessageBoxButton.OK, MessageBoxImage.Exclamation);
	          
	            return;
            }

            //Obtenemos el elemento seleccionado del grid
            Caja caja = (Caja)GridMovimientosCaja.SelectedItem; 

            var respuesta = (DialogResult)MessageBox.Show(string.Format("El movimiento con monto {0} será cancelado, ¿Continuar?", caja.Total), "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                var ventanaMotivo = new VentanaCancelacion {IdMovimiento = caja.Id, Owner = this};

                ventanaMotivo.Show(); 

            } 

	        return;
	    }

	    #endregion  

        private void btnVentas_Click(object sender, RoutedEventArgs e)
        {
            var ventanaListado = new VentanaListadoVentas();

            ventanaListado.ShowDialog();
        } 
	}
}
