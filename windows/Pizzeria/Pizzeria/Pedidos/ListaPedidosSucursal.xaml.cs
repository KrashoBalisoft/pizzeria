﻿using System.Linq;
using System.Windows.Forms;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Comun.Servicios.Catalogos;
using Telerik.Windows;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.Pedidos
{
	/// <summary>
	/// Interaction logic for MainWindow.xam
	/// </summary>
	public partial class ListaPedidosSucursal : Window
	{
		#region Variables

		private ServiciosPedidos _serviciosPedidos;
	    private ServiciosEstadosPedido _serviciosEstadosPedido;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
	   
        public Sucursal DatosSucursal;
	    

		#endregion

		#region Constructor

		public ListaPedidosSucursal()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

			_serviciosPedidos = new ServiciosPedidos(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosEstadosPedido = new ServiciosEstadosPedido(ConstantesWS.Usuario, ConstantesWS.Password);

            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 1, 0);
            dispatcherTimer.Start();
		}

		#endregion

		#region Eventos

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
			try
			{
                //Los datos de la sucursal
			    NombreSucursal.Content = DatosSucursal.Nombre;

                //Cargamos el listado de los estados del pedido
			    var listaEstados = _serviciosEstadosPedido.ObtenerListaEstadoPedido();

			    Filtro.DisplayMemberPath = "Descripcion";
			    Filtro.SelectedValuePath = "Id";
			    Filtro.SelectedValue = 1;

                Filtro.ItemsSource = await listaEstados;
			     
                //Cargar la lista de pedidos
                await CargarPedidos(); 
			}
			catch (Exception)
			{
				//MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
			}
			finally
			{
				IndicadorOcupado.IsBusy = false;
			}
		} 

		private void SistemaSalirClick(object sender, RadRoutedEventArgs e)
		{
			Close();
		}

        private async void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            await CargarPedidos();
        } 
       
        private void Agregar_Click(object sender, RoutedEventArgs e)
        {
            var ventanaCapturarPedido = new VentanaVentas()
            {
                Owner = this, DatosSucursal = this.DatosSucursal
            };

            ventanaCapturarPedido.Show();
        }

        private async void Enviar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null) return;

            try
            {
                var item = (Pedido)ListadoPedidos.SelectedItem;

                if (item.IdEstatus != ConstantesSistemas.IdEstadoInicialPedido)
                {
                    MessageBox.Show("El pedido ya ha sido enviado a la cocina o ha sido cancelado", "Atención", MessageBoxButton.OK,
                          MessageBoxImage.Exclamation);

                    return;
                }

                if (await _serviciosPedidos.EnviarPedidoALaCocina(item.Id))
                {
                    MessageBox.Show("El pedido ha sido enviado a la cocina", "Éxito", MessageBoxButton.OK,
                           MessageBoxImage.Exclamation);
                }

                await CargarPedidos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error." + ex.ToString());
            }
        }

        private async void Cancelar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoPedidos.SelectedItem == null) return;

            try
            {
                var respuesta = (DialogResult)MessageBox.Show("El pedido será cancelado, ¿Continuar?", "Atención", MessageBoxButton.YesNo);

                if (respuesta == System.Windows.Forms.DialogResult.Yes)
                {
                    var item = (Pedido)ListadoPedidos.SelectedItem;

                    if (await _serviciosPedidos.CancelarPedido(item.Id))
                    {
                        MessageBox.Show("El pedido ha sido cancelado", "Éxito", MessageBoxButton.OK,
                            MessageBoxImage.Exclamation);
                    }

                    await CargarPedidos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error." + ex.ToString());
            }
        }

	    private void Transferir_Click(object sender, RoutedEventArgs e)
	    {
            if (ListadoPedidos.SelectedItem == null) return;

            var item = (Pedido)ListadoPedidos.SelectedItem;

            if (item.IdEstatus != ConstantesSistemas.IdEstadoInicialPedido)
            {
                MessageBox.Show("El pedido ya ha sido enviado a la cocina o ha sido cancelado", "Atención", MessageBoxButton.OK,
                      MessageBoxImage.Exclamation);

                return;
            }

	        var VentanaTransferir = new FrmTransferirPedido() {Owner = this, pedido = item};

	        VentanaTransferir.ShowDialog();
	    }
	 
		#endregion

		#region Métodos

		public async Task CargarPedidos()
		{
		    try
		    {
                var pedidos = await _serviciosPedidos.ObtenerPedidosPorSucursal(ConstantesSistemas.IdSucursal);

		        if (Convert.ToInt32(Filtro.SelectedValue) != 0)
		        {
		            pedidos = pedidos.Where(x => x.IdEstatus == Convert.ToInt32(Filtro.SelectedValue)).ToList();
		        }

		        ListadoPedidos.ItemsSource = pedidos; 

		    }
		    catch (Exception e)
		    {
		        MessageBox.Show(e.ToString());
		    }
		}

		#endregion 

        private async void Filtro_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            await CargarPedidos();
        }
    }
}
