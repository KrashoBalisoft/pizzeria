﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Comun;
using Comun.Entidades;
using Comun.Servicios.Catalogos;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.Pedidos
{
    /// <summary>
    /// Interaction logic for FrmCobro.xaml
    /// </summary>
    public partial class FrmCobro : Window
    {
        private VentanaVentas ventanaPedidos = new VentanaVentas();
        private ServiciosTipoEntrega _serviciosTipoEntrega;
        private ServiciosMetodosPago _serviciosMetodosPago;
        public FrmCobro()
        {
            _serviciosTipoEntrega = new ServiciosTipoEntrega(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosMetodosPago = new ServiciosMetodosPago(ConstantesWS.Usuario, ConstantesWS.Password);

            InitializeComponent();
        }

        private async void FrmCobroPedido_Loaded(object sender, RoutedEventArgs e)
        {
            ventanaPedidos = (VentanaVentas)this.Owner;

            TipoEntrega.SelectedValuePath = "Id";
            TipoEntrega.DisplayMemberPath = "Descripcion";
            TipoEntrega.ItemsSource = await _serviciosTipoEntrega.ObtenerListaTipoEntrega();
            
            TipoPago.SelectedValuePath = "Id";
            TipoPago.DisplayMemberPath = "Descripcion";
            TipoPago.ItemsSource = await _serviciosMetodosPago.ObtenerLista();

            CargarDatosPedido();
        }

        private void CargarDatosPedido()
        {
            try
            {  
                GridDetalleVentas.ItemsSource = ventanaPedidos._productosEnPedido.ToList();
                Total.Text = ventanaPedidos._productosEnPedido.Sum(x => x.Total).ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error al cargar los datos.");
                throw;
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            //ventanaPedidos.GuardarPedido();
            if (String.IsNullOrEmpty(Efectivo.Text) || Convert.ToDecimal(Efectivo.Text) < Convert.ToDecimal(Total.Text))
            {
                MessageBox.Show("Indica correctamente el monto del efectivo", "Atención", MessageBoxButton.OK);
                
                return;
            }

            if (TipoEntrega.SelectedItem == null)
            {
                MessageBox.Show("Indica el tipo de entrega", "Atención", MessageBoxButton.OK);

                return;
            }

            Cambio.Text = (Convert.ToDecimal(Efectivo.Text) - Convert.ToDecimal(Total.Text)).ToString(CultureInfo.CurrentCulture);

            ventanaPedidos.GuardarPedido(Convert.ToInt32(TipoEntrega.SelectedValue), Convert.ToInt32(TipoPago.SelectedValue));

            Close();
        }

        private void btcCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Efectivo_GotFocus(object sender, RoutedEventArgs e)
        {
            Efectivo.Text = String.Empty;
        }

        private void Efectivo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Efectivo.Text) || Convert.ToDecimal(Efectivo.Text) < Convert.ToDecimal(Total.Text))
            {
                MessageBox.Show("Indica correctamente el monto del efectivo", "Atención", MessageBoxButton.OK);

                return;
            }

            Cambio.Text = (Convert.ToDecimal(Efectivo.Text) - Convert.ToDecimal(Total.Text)).ToString(CultureInfo.CurrentCulture);

        } 
    }
}
