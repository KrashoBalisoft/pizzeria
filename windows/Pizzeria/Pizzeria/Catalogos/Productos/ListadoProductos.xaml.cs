﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;
using Comun.Servicios;

namespace Pizzeria.Catalogos.Productos
{
    /// <summary>
    /// Interaction logic for ListadoMetodosPago.xaml
    /// </summary>
    public partial class ListadoProductos : Window
    {
        private ServiciosProductos _servicioProductos;

		#region Constructor

        public ListadoProductos()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

            _servicioProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);
		}

		#endregion
         

        public async Task CargarDatos()
        {
            var lista = await _servicioProductos.ListadoProductos();

            Listado.AutoGenerateColumns = false;
            Listado.ItemsSource = lista;
        }

        private async void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            IndicadorOcupado.IsBusy = true;
            try
            {
                await CargarDatos();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            finally
            {
                IndicadorOcupado.IsBusy = false;
            }

        }
           
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var ventana = new VentanaProductos()
            {
                Owner = this
            };

            ventana.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           if (Listado.SelectedItem == null) return;

           var ventana = new VentanaProductos()
           {
               Owner = this,
               producto = (Producto)Listado.SelectedItem
           };

           ventana.ShowDialog();
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var producto = (Producto)Listado.SelectedItem;

            if (producto == null)
            {
                MessageBox.Show("¡Seleccione un producto de la lista!", "Atención", MessageBoxButton.OK);

                return; 
            }

            var respuesta = (DialogResult)MessageBox.Show(string.Format("{0} será eliminado, ¿Continuar?", producto.Nombre), "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                var resultado = await _servicioProductos.Eliminar(producto.Id);
                if (resultado == "true")
                {
                    MessageBox.Show("El registro ha sido eliminado correctamente.", "Éxito", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);

                    await CargarDatos();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                } 
            }
        }

     }
}
