﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Entidades.Catalogos;
using Telerik.Windows.Controls;
using Comun.Entidades;
using Comun.Servicios;

namespace Pizzeria.Catalogos.Productos
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaProductos : Window
    {
        public int Id;
        private ServiciosProductos _serviciosProductos;
        private ListadoProductos frmListado;
        public Producto producto = new Producto();

        public VentanaProductos()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosProductos = new ServiciosProductos(ConstantesWS.Usuario, ConstantesWS.Password);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoProductos) this.Owner;

            //Llenado de los combos
            TipoProductoCombo.ItemsSource = TipoProducto.ObtenerLista();
            TipoProductoCombo.SelectedValuePath = "Id";
            TipoProductoCombo.DisplayMemberPath = "Descripcion";

            //Llenado de los combos
            Clasificacion.ItemsSource = ClasificacionProducto.ObtenerLista();
            Clasificacion.SelectedValuePath = "Id";
            Clasificacion.DisplayMemberPath = "Descripcion";
            
            //Si el objeto es para actualización
            if (producto.Id != 0)
            {
                Descripcion.Text = producto.Nombre;
                txtImagen.Text = producto.NombreImagen;
                PrecioVenta.Text = producto.PrecioVenta.ToString();
                Cantidad.Text = producto.Cantidad.ToString();
                CantidadMinima.Text = producto.CantidadMinima.ToString(); 
                EnviarCocina.IsChecked = producto.EnviarCocina; 
                Seguir.IsChecked = producto.Seguir == 1;
                Clasificacion.SelectedValue = Convert.ToInt32(producto.IdClasificacion);
                TipoProductoCombo.SelectedValue = Convert.ToChar(producto.TipoProducto); 
            }

        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnTamanios_Click(object sender, RoutedEventArgs e)
        {
            var frmTamanios = new FrmProductoTamanio
            {
                Owner = this,
                _producto = producto
            };

            frmTamanios.ShowDialog();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Descripcion.Text))
            {
                MessageBox.Show("¡El nombre del producto es obligatorio!", "¡Atención!", MessageBoxButton.OK);

                return;
            }
            
            if (String.IsNullOrEmpty(PrecioVenta.Text) || String.IsNullOrEmpty(Cantidad.Text) || String.IsNullOrEmpty(CantidadMinima.Text))
            {
                MessageBox.Show("¡Precio, cantidad y cantidad mínima son oblgatorios!", "¡Atención!", MessageBoxButton.OK);

                return;
            }

            if (producto.Id == 0)
            {
                Guardar();
            }
            else
            {
                Actualizar();
            } 
        }

        private async void Guardar()
        {
            try
            {
               
                producto.Nombre = Descripcion.Text;
                producto.NombreImagen = txtImagen.Text;
                producto.PrecioVenta = Convert.ToDecimal(PrecioVenta.Text);
                producto.Cantidad = Convert.ToInt32(Cantidad.Text);
                producto.CantidadMinima = Convert.ToInt32(CantidadMinima.Text);
                producto.CantidadIngredientes = 0;
                producto.EnviarCocina = (bool)EnviarCocina.IsChecked;
                producto.Seguir = (bool)Seguir.IsChecked ? 1 : 0;
                producto.IdClasificacion = Convert.ToInt32(Clasificacion.SelectedValue);
                producto.TipoProducto = Convert.ToChar(TipoProductoCombo.SelectedValue);
                                     


                var resultado = await _serviciosProductos.Agregar(producto);
                if (resultado)
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
                  
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK); 
            }             
        }
         
        private async void Actualizar()
        {
            try
            {
                producto.Nombre = Descripcion.Text;
                 
                producto.NombreImagen = txtImagen.Text;
                producto.PrecioVenta = Convert.ToDecimal(PrecioVenta.Text);
                producto.Cantidad = Convert.ToInt32(Cantidad.Text);
                producto.CantidadMinima = Convert.ToInt32(CantidadMinima.Text);
                producto.CantidadIngredientes = 0;
                producto.EnviarCocina = (bool) EnviarCocina.IsChecked;
                producto.Seguir = Seguir.IsChecked != null && (bool) Seguir.IsChecked ? 1 : 0;
                producto.IdClasificacion = Convert.ToInt32(Clasificacion.SelectedValue);
                producto.TipoProducto = Convert.ToChar(TipoProductoCombo.SelectedValue);
                                     
                var resultado = await _serviciosProductos.Actualizar(producto);
                if (resultado)
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }  
            }
            catch (Exception exp)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde" + exp, "¡Atención!", MessageBoxButton.OK);
            }
        }

    }
}
