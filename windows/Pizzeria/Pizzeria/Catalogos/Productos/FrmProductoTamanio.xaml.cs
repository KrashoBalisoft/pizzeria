﻿using System.Collections.Generic;
using System.Windows;
using Comun;
using Comun.Entidades;

namespace Pizzeria.Catalogos.Productos
{
    /// <summary>
    /// Interaction logic for FrmProductoTamanio.xaml
    /// </summary>
    public partial class FrmProductoTamanio : Window
    {
        public Producto _producto = new Producto();
        private VentanaProductos _ventanaProductos;
        public FrmProductoTamanio()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            GuardarDatos(); 
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FrmProductoTamanio_Loaded(object sender, RoutedEventArgs e)
        {
            CargarDatos();
        }

        private void GuardarDatos()
        {
            if (ValidarDatos())
            {
                _producto.Tamanios = new List<ProductoTamanio>();
  

                if (Pequeno.IsChecked != null && ((bool) Pequeno.IsChecked))
                {
                    ProductoTamanio detalle = new ProductoTamanio();
                    detalle.PrecioVenta = (decimal?)PrecioPequeno.Value;
                    detalle.IdTamanio = ConstantesSistemas.IdPequeno; 
                    detalle.IdProducto = _producto.Id;
                    detalle.Estatus = "A";
                 
                    _producto.Tamanios.Add(detalle); 
 
                }

                if (Mediano.IsChecked != null && ((bool) Mediano.IsChecked))
                {
                    ProductoTamanio detalle = new ProductoTamanio();

                     detalle.PrecioVenta = (decimal?)PrecioMediano.Value;
                     detalle.IdTamanio = ConstantesSistemas.IdMediano;
                     detalle.IdProducto = _producto.Id;
                     detalle.Estatus = "A";

                    _producto.Tamanios.Add(detalle); 
                }

                if (Grande.IsChecked != null && ((bool)Grande.IsChecked))
                {
                    ProductoTamanio detalle = new ProductoTamanio();

                    detalle.PrecioVenta = (decimal?)PrecioGrande.Value;
                    detalle.IdTamanio = ConstantesSistemas.IdGrande;
                    detalle.IdProducto = _producto.Id;
                    detalle.Estatus = "A";

                    _producto.Tamanios.Add(detalle); 
                }

                _ventanaProductos.producto.Tamanios = _producto.Tamanios;

                Close();
            }
        }
          
        private bool ValidarDatos()
        {
            bool bandera = false;
            
            if (Pequeno.IsChecked != null && ((bool)Pequeno.IsChecked && PrecioPequeno.Value < 0))
            {
                bandera = true;
            }

            if (Mediano.IsChecked != null && ((bool)Mediano.IsChecked && PrecioMediano.Value < 0))
            {
                bandera = true; 
            }

            if (Grande.IsChecked != null && ((bool)Grande.IsChecked && PrecioGrande.Value < 0))
            {
                bandera = true; 
            }

            if (bandera)
            {
                MessageBox.Show("Para los tamaños seleccionados, escriba el precio", "Atención", MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        private void CargarDatos()
        {
            _ventanaProductos = (VentanaProductos) this.Owner;
            if (_producto.Id != 0)
            {
                foreach (var _tamanio in _producto.Tamanios)
                {
                    if (_tamanio != null)
                        switch (_tamanio.IdTamanio)
                        {
                            case ConstantesSistemas.IdPequeno:
                                Pequeno.IsChecked = true;
                                PrecioPequeno.Value = (double?) _tamanio.PrecioVenta;
                                break;
                            case ConstantesSistemas.IdMediano:
                                Mediano.IsChecked = true;
                                PrecioMediano.Value = (double?) _tamanio.PrecioVenta;
                                break;
                            case ConstantesSistemas.IdGrande:
                                Grande.IsChecked = true;
                                PrecioGrande.Value = (double?) _tamanio.PrecioVenta;
                            break; 
                        }
                }
            }
        }

        private void PrecioPequeno_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {

        }
    }
}
