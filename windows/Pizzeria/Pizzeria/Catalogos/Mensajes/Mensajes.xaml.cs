﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;
using Comun.Entidades.Catalogos;

namespace Pizzeria.Catalogos.Mensajes
{
    /// <summary>
    /// Interaction logic for ListadoMetodosPago.xaml
    /// </summary>
    public partial class ListadoMensajes : Window
    {
        private ServiciosMensajes _servicio;

		#region Constructor

        public ListadoMensajes()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

            _servicio = new ServiciosMensajes(ConstantesWS.Usuario, ConstantesWS.Password);
		}

		#endregion 

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IndicadorOcupado.IsBusy = true;
            try
            {
                await CargarDatos();
            }
            catch (Exception)
            {
                //MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
            }
            finally
            {
                IndicadorOcupado.IsBusy = false;
            }

        }

        public async Task CargarDatos()
        {
            var lista = await _servicio.ObtenerListaMensajes();
            Listado.ItemsSource = lista;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (Listado.SelectedItem == null) return;

            var ventana = new VentanaMensajes()
            {
                Owner = this,
                registro = (Mensaje)Listado.SelectedItem
            };

            ventana.ShowDialog();
        } 
    }
}
