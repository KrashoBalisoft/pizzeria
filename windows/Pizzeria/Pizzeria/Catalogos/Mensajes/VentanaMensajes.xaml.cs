﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Comun.Entidades.Catalogos;

namespace Pizzeria.Catalogos.Mensajes
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaMensajes : Window
    {
        public int Id;
        private ServiciosMensajes _servicios;
        private ListadoMensajes frmListado;
        public Mensaje registro;

        public VentanaMensajes()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _servicios = new ServiciosMensajes(ConstantesWS.Usuario, ConstantesWS.Password);

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoMensajes) this.Owner;

            //Si el objeto es para actualización
            if (registro != null)
            {
                Descripcion.Text = registro.Descripcion;
            }

        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (registro != null)
            {
                 Actualizar();
            }
            
        }
 
        private async void Actualizar()
        {
            try
            {
                var resultado = await _servicios.ActualizarMensaje(registro.Id, Descripcion.Text);
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            } 

        }

    }
}
