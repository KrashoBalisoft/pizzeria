﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Pizzeria.Catalogos.Sucursale;
using Comun.Entidades;
using Comun.Servicios;

namespace Pizzeria.Catalogos.Sucursale
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaSucursal : Window
    {
        public int Id;
        private ServiciosSucursales _servicios;
        private ListadoSucursal frmListado;
        public Sucursal sucursal;

        public VentanaSucursal()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _servicios = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);

        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoSucursal) this.Owner;

            //Si el objeto es para actualización
            if (sucursal != null)
            {
                Nombre.Text = sucursal.Nombre;
                Direccion.Text = sucursal.Direccion;
                Telefono.Text = sucursal.Telefono;
                Horario.Text = sucursal.Horario;
            }

        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (sucursal == null)
            {
                Guardar();
            }
            else
            {
                Actualizar();
            }
            
        }

        private async void Guardar()
        {
            try
            {
                var resultado = await _servicios.Insertar(Nombre.Text, Direccion.Text, Telefono.Text, Horario.Text);
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                throw;
            }
           
        }

        private async void Actualizar()
        {
            try
            {
                var resultado = await _servicios.Actualizar(sucursal.Id, Nombre.Text, Direccion.Text, Telefono.Text, Horario.Text);
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            }
        }

    }
}
