﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;
using Comun.Entidades.Catalogos;
using Comun.Servicios;

namespace Pizzeria.Catalogos.Colores
{
    /// <summary>
    /// Interaction logic for ListadoMetodosPago.xaml
    /// </summary>
    public partial class ListadoColores : Window
    {
        private ServiciosColores _servicio;

		#region Constructor

        public ListadoColores()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

            _servicio = new ServiciosColores(ConstantesWS.Usuario, ConstantesWS.Password);
		}

		#endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var ventana = new VentanaColores()
            {
                Owner = this
            };

            ventana.ShowDialog();
            
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IndicadorOcupado.IsBusy = true;
            try
            {
                await CargarDatos();
            }
            catch (Exception)
            {
                //MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
            }
            finally
            {
                IndicadorOcupado.IsBusy = false;
            }

        }

        public async Task CargarDatos()
        {
            var lista = await _servicio.ObtenerListaColores();
            Listado.ItemsSource = lista;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (Listado.SelectedItem == null) return;

            var ventana = new VentanaColores()
            {
                Owner = this,
                color = (Comun.Entidades.ColorSemaforo)Listado.SelectedItem
            };

            ventana.ShowDialog();
 
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var entrega = (ColorSemaforo)Listado.SelectedItem;

            var respuesta = (DialogResult)MessageBox.Show(string.Format("{0} será eliminado, ¿Continuar?", entrega.Color), "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                var resultado = await _servicio.Eliminar(entrega.Id);
                if (resultado == "true")
                {
                    MessageBox.Show("El registro ha sido eliminado correctamente.", "Éxito", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);

                    await CargarDatos();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }

            }              
        }

    }
}
