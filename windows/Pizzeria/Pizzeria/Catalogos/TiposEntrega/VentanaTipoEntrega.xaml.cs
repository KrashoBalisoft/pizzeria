﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Comun.Entidades.Catalogos;

namespace Pizzeria.Catalogos.TiposEntrega
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaTipoEntrega : Window
    {
        public int Id;
        private ServiciosTipoEntrega _serviciosTipoEntrega;
        private ListadoTipoEntrega frmListadoTiposEntrega;
        public TipoEntrega tipoEntrega = new TipoEntrega();
 
        public VentanaTipoEntrega()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _serviciosTipoEntrega = new ServiciosTipoEntrega(ConstantesWS.Usuario, ConstantesWS.Password);

        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListadoTiposEntrega = (ListadoTipoEntrega) this.Owner;

            //Si el objeto es para actualización
            if (tipoEntrega != null)
            {
                Descripcion.Text = tipoEntrega.Descripcion;
            }
        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (tipoEntrega == null)
            {
                Guardar(); 
            }
            else
            {
                Actualizar();
            }
        }

        private async void Guardar()
        {
            try
            {
                if (await _serviciosTipoEntrega.Insertar(Descripcion.Text))
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListadoTiposEntrega.CargarDatos();

                    Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                throw;
            } 
        }

        private async void Actualizar()
        {
            try
            {
                if (await _serviciosTipoEntrega.Actualizar(tipoEntrega.Id, Descripcion.Text))
                {
                    MessageBox.Show("La actualización ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListadoTiposEntrega.CargarDatos();

                    Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                throw;
            }

            
        }
    }
}
