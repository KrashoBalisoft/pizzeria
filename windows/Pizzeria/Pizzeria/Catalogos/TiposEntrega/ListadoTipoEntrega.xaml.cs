﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Comun;
using Comun.Entidades;
using Comun.Entidades.Catalogos;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria.Catalogos.TiposEntrega
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class ListadoTipoEntrega : Window
	{
		#region Variables

		private ServiciosTipoEntrega _serviciosTipoEntrega;
	  	#endregion

		#region Constructor

        public ListadoTipoEntrega()
		{
			Thread.Sleep(2000);
			StyleManager.ApplicationTheme = new Windows8TouchTheme();

			Windows8TouchPalette.Palette.AccentColor = Colors.Red;
			Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
			Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
			Windows8TouchPalette.Palette.MediumColor = Colors.Green;
			Windows8TouchPalette.Palette.HighColor = Colors.Red;
			
			InitializeComponent();

            _serviciosTipoEntrega = new ServiciosTipoEntrega(ConstantesWS.Usuario, ConstantesWS.Password);
		}

		#endregion

		#region Eventos

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			IndicadorOcupado.IsBusy = true;
		    try
		    {
		        await CargarDatos();
		    }
		    catch (Exception)
		    {
		        //MessageBox.Show(Properties.Resources.ErrorWebServiceGenerico);
		    }
		    finally
		    {
		        IndicadorOcupado.IsBusy = false;
		    }
		}
         
        //Agregar movimientos 
        private void Agregar_Click(object sender, RoutedEventArgs e)
        {
            var ventanaTipoEntrega = new VentanaTipoEntrega()
            {
                Owner = this, 

                tipoEntrega = null
            };

            ventanaTipoEntrega.ShowDialog();
        }

        private void Actualizar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoTiposEntrega.SelectedItem == null) return;
            
            var ventanaTipoEntrega = new VentanaTipoEntrega()
            {
                Owner = this,
                tipoEntrega = (Comun.Entidades.Catalogos.TipoEntrega)ListadoTiposEntrega.SelectedItem
            };

            ventanaTipoEntrega.ShowDialog();

        }

        private void Eliminar_Click(object sender, RoutedEventArgs e)
        {
            if (ListadoTiposEntrega.SelectedItem == null) return;

            EliminarMovimiento(); 
        }
          
		#endregion

		#region Métodos

		public async Task CargarDatos()
		{
			var lista = await _serviciosTipoEntrega.ObtenerListaTipoEntrega();  
			ListadoTiposEntrega.ItemsSource = lista; 
		}

	    private async void EliminarMovimiento()
	    { 
            var entrega = (TipoEntrega)ListadoTiposEntrega.SelectedItem;

            var respuesta = (DialogResult)MessageBox.Show(string.Format("{0} será eliminado, ¿Continuar?", entrega.Descripcion), "Atención", MessageBoxButton.YesNo);

            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                if (await _serviciosTipoEntrega.Eliminar(entrega.Id))
                {
                    MessageBox.Show("El registro ha sido eliminado correctamente.", "Éxito", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);

                    await CargarDatos(); 
                } 
            }  
	    }
    } 
	  
   #endregion  
}
