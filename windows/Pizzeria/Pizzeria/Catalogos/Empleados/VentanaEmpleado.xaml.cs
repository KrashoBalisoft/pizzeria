﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Pizzeria.Catalogos.Empleados;
using Comun.Entidades;
using Comun.Entidades.Catalogos;

namespace Pizzeria.Catalogos.Empleados
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaEmpleado : Window
    {
        public int Id;
        private ServiciosEmpleado _servicios;
        private ServiciosTipoEmpleados _serviciosTipoEmpleado;
        private ListadoEmpleado frmListado;
        public Empleado empleado;

        public VentanaEmpleado()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _servicios = new ServiciosEmpleado(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosTipoEmpleado = new ServiciosTipoEmpleados(ConstantesWS.Usuario, ConstantesWS.Password);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoEmpleado) this.Owner;

			TipoEmpleadoCombo.SelectedValuePath = "Id";
			TipoEmpleadoCombo.DisplayMemberPath = "Descripcion";
			var tiposEmpleados = await _serviciosTipoEmpleado.ObtenerLista();
			TipoEmpleadoCombo.ItemsSource = tiposEmpleados;

            //Si el objeto es para actualización
            if (empleado != null)
            {
                Nombre.Text = empleado.Nombre;
                TipoEmpleadoCombo.SelectedValue = empleado.IdTipoEmpleado;
				/*foreach(TipoEmpleado tipo in tiposEmpleados)
				{
					if (tipo.Descripcion == empleado.TipoEmpleado)
					{
						TipoEmpleadoCombo.SelectedValue = tipo.Id;

						break;
					}
				}
                 */ 
            }


        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (empleado == null)
            {
                Guardar();
            }
            else
            {
                Actualizar();
            }             
        }

        private async void Guardar()
        {
            try
            {
                var resultado = await _servicios.Insertar(Nombre.Text,Convert.ToInt32(TipoEmpleadoCombo.SelectedValue));
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                throw;
            }
           
        }
        
        private async void Actualizar()
        {
            try
            {
                var resultado = await _servicios.Actualizar(empleado.Id, Nombre.Text, Convert.ToInt32(TipoEmpleadoCombo.SelectedValue));
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            }
        }
    }
}
