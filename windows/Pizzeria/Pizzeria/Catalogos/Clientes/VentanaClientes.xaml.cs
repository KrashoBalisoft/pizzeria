﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Comun.Entidades;
using Comun.Servicios;

namespace Pizzeria.Catalogos.Clientes
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaClientes : Window
    {
        public int Id;
        private ServiciosClientes _servicios;
        private ServiciosSucursales _serviciosSucursales;
        private ServiciosTipoTelefono _serviciosTipoTelefono;
        private ListadoClientes frmListado;
        public Cliente cliente;

        public VentanaClientes()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _servicios = new ServiciosClientes(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosSucursales = new ServiciosSucursales(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosTipoTelefono = new ServiciosTipoTelefono(ConstantesWS.Usuario, ConstantesWS.Password);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoClientes) this.Owner;

            SucursalCombo.SelectedValuePath = "Id";
            SucursalCombo.DisplayMemberPath = "Nombre";
            var sucursales = await _serviciosSucursales.ObtenerListaSucursales();
            SucursalCombo.ItemsSource = sucursales;

            TipoTelefonoCombo.SelectedValuePath = "Id";
            TipoTelefonoCombo.DisplayMemberPath = "Descripcion";
            var tipoTelefono = await _serviciosTipoTelefono.ObtenerLista();
            TipoTelefonoCombo.ItemsSource = tipoTelefono;
            
            //Si el objeto es para actualización
            if (cliente != null)
            {
                Nombre.Text = cliente.Nombre;
                Telefono.Text = cliente.Telefono;
                CodigoPostal.Text = cliente.CodigoPostal;
                Colonia.Text = cliente.Colonia;
                Domicilio.Text = cliente.Domicilio;
                Correo.Text = cliente.Correo;
                RFC.Text = cliente.RFC;

                SucursalCombo.SelectedValue = cliente.IdSucursal;
                TipoTelefonoCombo.SelectedValue = cliente.IdTipoTelefono;
                //Codigo.SelectedColor = (Color)ColorConverter.ConvertFromString(color.Codigo);
            }

        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (cliente == null)
            {
                Guardar();
            }
            else
            {
                Actualizar();
            }
            
        }

        private async void Guardar()
        {
            try
            {
                Cliente clienteNuevo = new Cliente();
                clienteNuevo = llenarDatos(clienteNuevo);
                var resultado = await _servicios.Insertar(clienteNuevo);
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
                 
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                //throw;
            }             
        }

        private Cliente llenarDatos(Cliente cliente)
        {
            cliente.IdSucursal = Convert.ToInt32(SucursalCombo.SelectedValue);
            cliente.IdTipoTelefono = Convert.ToInt32(TipoTelefonoCombo.SelectedValue);
            cliente.Nombre = Nombre.Text;
            cliente.Telefono = Telefono.Text;
            cliente.CodigoPostal = CodigoPostal.Text;
            cliente.Colonia = Colonia.Text;
            cliente.Domicilio = Domicilio.Text;
            cliente.Correo = Correo.Text;
            cliente.RFC = RFC.Text;

            return cliente;
        }
        private async void Actualizar()
        {
            try
            {
                cliente = llenarDatos(cliente);
                var resultado = await _servicios.ModificarCliente(cliente);
                if (resultado == true)
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }                 
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            }
        }

    }
}
