﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Comun;
using Comun.Servicios.Catalogos;
using Telerik.Windows.Controls;
using Comun.Entidades.Catalogos;
using System.Security.Cryptography;

namespace Pizzeria.Catalogos.Usuarios
{
    /// <summary>
    /// Interaction logic for VentanaTipoEntrega.xaml
    /// </summary>
    public partial class VentanaUsuarios : Window
    {
        public int Id;
        private ServiciosUsuario _servicios;
        private ServiciosPerfil _serviciosPerfil;
        private ServiciosEmpleado _serviciosEmpleado; 
        private ListadoUsuarios frmListado;
        public Usuario registro;

        public VentanaUsuarios()
        {
            Thread.Sleep(1000);
            StyleManager.ApplicationTheme = new Windows8TouchTheme();
 
            Windows8TouchPalette.Palette.AccentColor = Colors.Red;
            Windows8TouchPalette.Palette.EffectHighColor = Colors.White;
            Windows8TouchPalette.Palette.EffectLowColor = Colors.LightGreen;
            Windows8TouchPalette.Palette.MediumColor = Colors.Green;
            Windows8TouchPalette.Palette.HighColor = Colors.Red;

            InitializeComponent();

            _servicios = new ServiciosUsuario(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosPerfil = new ServiciosPerfil(ConstantesWS.Usuario, ConstantesWS.Password);
            _serviciosEmpleado = new ServiciosEmpleado(ConstantesWS.Usuario, ConstantesWS.Password);

        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frmListado = (ListadoUsuarios) this.Owner;
			

            cboPerfil.SelectedValuePath = "Id";
            cboPerfil.DisplayMemberPath = "Descripcion";
            cboPerfil.ItemsSource = await _serviciosPerfil.ObtenerLista();

            cboEmpleado.SelectedValuePath = "Id";
            cboEmpleado.DisplayMemberPath = "Nombre";
            cboEmpleado.ItemsSource = await _serviciosEmpleado.ObtenerLista();
			
	        cboEstatus.SelectedValuePath = "Id";
	        cboEstatus.DisplayMemberPath = "Descripcion";
	        cboEstatus.ItemsSource = Comun.Entidades.Catalogos.Estatus.ObtenerLista();
            

            //Si el objeto es para actualización
            if (registro != null)
            {
                Username.Text = registro.Username;
                cboPerfil.SelectedValue = registro.IdPerfil;
                cboEstatus.SelectedValue = registro.Estatus;
                cboEmpleado.SelectedValue = registro.IdEmpleado;
            }



        } 

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (registro == null)
            {
                Guardar();
            }
            else
            {
                Actualizar();
            }
            
        }

        private async void Guardar()
        {
            try
            {
                var passConvertido = "";

                if(Pass.Text.Length > 0){
                    passConvertido = Crypto.MD5Hash(Pass.Text);
                }

                var resultado = await _servicios.Insertar(Username.Text,passConvertido,Convert.ToInt32(cboPerfil.SelectedValue),cboEstatus.SelectedValue.ToString(),Convert.ToInt32(cboEmpleado.SelectedValue));
                //string username, string pass, int id_perfil, string estatus, int id_empleado
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La inserción ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);

                throw;
            } 
        }

        private async void Actualizar()
        {
            try
            {
                var resultado = await _servicios.Actualizar(registro.Id, Username.Text, "", Convert.ToInt32(cboPerfil.SelectedValue), cboEstatus.SelectedValue.ToString(), Convert.ToInt32(cboEmpleado.SelectedValue));
                if (resultado.ToString() == "true")
                {
                    MessageBox.Show("La modificación ha sido correcta", "¡Éxito!", MessageBoxButton.OK);

                    await frmListado.CargarDatos();

                    Close();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error en el guardado", "¡Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hubo un error, inténtelo más tarde", "¡Atención!", MessageBoxButton.OK);
            } 
           
        }

    }
}
