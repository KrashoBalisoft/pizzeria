﻿using System;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace Pizzeria
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
	    
        private void Application_Startup(object sender, StartupEventArgs e)
        {
               Thread.CurrentThread.CurrentCulture = new CultureInfo("es");
               Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");

               Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-MX");
             
        }
	}
}
